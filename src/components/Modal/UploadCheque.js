import React, {useState} from "react";
import { Row, Col, Modal, Button } from "react-bootstrap";
import { Title } from "../Texonomy";
import swal from "sweetalert";

export const UploadCheque = (props) => {
  const [qrcode, setQrCode] = useState(false);
  const [prchmage, setPrChImage] = useState(null);
  let PrChData = new FormData();
  PrChData.append("priented_imag", prchmage);

  const Upload = () => {
    swal("Verify Cheque No", "Please Verify your Cheque Number", "warning");
    const id = props.ids;
    props.api
      .put(`/addprintedimage/${id}/`, PrChData, {
        headers: {
          Authorization: ` Bearer ${props.token}`,
          "content-type": `multipart/form-data`,
        },
      })
      .then((res) => {
        console.log(res);
        props.setCheqnoData(res.data.new_chqno);
        props.handleClose()
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <Modal show={props.show} onHide={props.handleClose} centered>
      <Modal.Header className="border-0" closeButton>
        <div className="font-size-18px font-weight-600 text-primary">
          Upload Bank Printed Cheque
        </div>
      </Modal.Header>
      <Modal.Body>
        <div className="rowCol">
          <Row>
            <Col>
              <div>
                <input
                  accept="image/jpg"
                  type="file"
                  id="image"
                  onChange={(e) => setPrChImage(e.target.files[0])}
                />
              </div>
              <h6>OR</h6>
              <a href="#" onClick={(e) => setQrCode(true)}>
                <h5>Click Picture</h5>
              </a>
              <div className="text-center">
                {qrcode && (
                  <img
                    style={{width: '60%'}}
                    alt="qrcode"
                    src="https://qrcode.tec-it.com/API/QRCode?data=QR+Code+Generator+by+TEC-IT"
                  ></img>
                )}
              </div>
            </Col>
          </Row>
          <div className="text-right">
              <Button onClick={Upload} variant="primary">Save</Button>
          </div>


          {/* <Row>
            <Col
              md={12}
              className="d-flex flex-wrap align-items-center justify-content-md-end"
            >
              <Button className="ok-btn" onClick={props.handleClose}>
                Ok
              </Button>
              <button className="delete-btn ml-md-2">Delete</button>
            </Col>
          </Row> */}
        </div>
      </Modal.Body>
    </Modal>
  );
};
