import React from "react"
import { Row, Col, Modal } from 'react-bootstrap';

export const NewGroupCreation = (props) => {
    return (<Modal show={props.show} onHide={props.handleClose} centered>
        <Modal.Header className="border-0 " closeButton>New Group Creation</Modal.Header>
        <Modal.Body className="pt-0 pb-4">
        <div className="rowCol">
            <div className="">                
                <Row>                   
                        <Col md={12}>
                        <div className="form-group">
                            <label className="font-size-14px pl-2">New Group Name</label>
                            <input name="Transaction" className="form-control" type="text" placeholder="New Group Name" />

                        </div>
                        </Col>
                            <Col md={12}>
                                <div className="form-group">
                                    <label className="font-size-12px pl-2">Group Name<span className="pl-2 text-muted font-size-12px">Under which new group will be created</span></label>
                                    <select className="form-control">
                                        <option value="none" selected disabled>
                                            Select a Group
                                        </option>
                                        <option>Group 1</option>
                                        <option>Group 2</option>
                                        <option>Group 3</option>
                                    </select>
                                    
                                </div>
                            </Col>
                            
                    <Col md={12} className="d-flex flex-wrap align-items-center justify-content-md-end">
                        <button className="ok-btn " onClick={props.handleClose}>Ok</button>
                        <button className="delete-btn ml-md-2">Delete</button>
                    </Col>
                </Row>
            </div>
        </div>
        </Modal.Body>
    </Modal>)
}