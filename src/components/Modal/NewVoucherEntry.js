import React, { useEffect, useState } from "react";
import { Row, Col, Modal } from "react-bootstrap";
import DatePicker from "react-datepicker";
import { useSelector } from "react-redux";
import axios from "axios";

export const NewVoucherEntry = (props) => {
  const { sellerData, buyerData, otherData, productData } = useSelector(
    (state) => state.InvoiceCreateReducer
  );
  const { data, sellerCompanyList, TotalAmount } = props;
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [addShow, setAddShow] = useState(false);
  const [voucherData, setVoucherData] = useState({});

  useEffect(() => {
    setVoucherData({
      Narration: "Invoice " + otherData.Invoice_no ? otherData.Invoice_no : null,
    });
  }, [otherData]);

  const [voucherValue, setVoucherValue] = useState({
    dr: "",
    cr: "",
    type: "",
  });
  const [ovoucherValue, setOVoucherValue] = useState({
    dr: TotalAmount,
    cr: TotalAmount,
    type: "",
  });
  const [thirdRow, setThirdRow] = useState({
    Amount: 0,
    type: "",
  });

  useEffect(() => {
    setVoucherValue({
      dr: TotalAmount,
      cr: TotalAmount,
      type: "",
    });
    setOVoucherValue({
      dr: TotalAmount,
      cr: TotalAmount,
      type: "",
    });
  }, [TotalAmount]);

  const VoucherList = [
    "SELECT VOUCHER",
    "PURCHASE VOUCHER",
    "SALES VOUCHER",
    "DEBIT NOTE VOUCHER",
    "PAYMENT VOUCHER",
    "RECEIPT VOUCHER",
    "JOURNAL VOUCHER",
    "CREDIT NOTE VOUCHER",
    "CONTRA VOUCHER",
  ];

  const voucherTypeList = ["Sale Voucher", "Purchase Voucher"];

  const SubmitVoucher = async () => {
    const body = {
      Voucher_date: otherData.Invoice_date,
      legdername: sellerData.seller_company,
      Voucher_amount: TotalAmount,
      GSTlegderdata: "GST",
      GSTlegderamount: thirdRow.Amount,
      EditLegder: "Sale Account",
      ...voucherData,
    };

    await axios({
      method: "post",
      url: "http://127.0.0.1:8000/invoice/voucherinvoice/",
      data: body,
    })
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  const onChangeValue = (name, value, type) => {
    setVoucherValue({ ...voucherValue, [name]: value, type: type });
  };

  const OnEnterPress = (name, value, type) => {
    if (type == "dr") {
      const Amount = (ovoucherValue.dr - value).toFixed(2);
      setThirdRow({ Amount, type: name });
    }

    if (type == "cr") {
      const Amount = (ovoucherValue.cr - value).toFixed(2);
      setThirdRow({ Amount, type: name });
    }
  };

  const deleteRow = (rowType) => {
    if (rowType == "thirdRow") {
      setThirdRow({
        Amount: 0,
        type: "",
      });
      setVoucherValue({ ...ovoucherValue });
    }
    if (rowType == "addShow") {
      setVoucherValue({ ...ovoucherValue });
      setAddShow(false);
    }
  };
  return (
    <Modal show={props.show} onHide={props.handleClose} centered size="lg">
      <Modal.Header className="border-0" closeButton>
        New Voucher Entry
      </Modal.Header>
      <Modal.Body className="pt-0 pb-4">
        <div className="rowCol">
          <div className="borderBox">
            <Row>
              <Col md={3}>
                <div className="form-group">
                  <DatePicker
                    dateFormat="dd/MM/yyyy"
                    selected={
                      otherData.Invoice_date != undefined
                        ? new Date(otherData.Invoice_date)
                        : new Date()
                    }
                    selectsStart
                    className="form-control"
                    placeholderText="From Date"
                    disabled
                  />
                </div>
              </Col>
              <Col md={6}>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Particulars"
                    value={sellerData.seller_company}
                  />
                </div>
              </Col>
              <Col md={3}>
                <div className="form-group position-relative">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Amount"
                    value={voucherValue.dr}
                    name="dr"
                    onChange={(e) =>
                      onChangeValue(e.target.name, e.target.value, "dr")
                    }
                    onKeyDown={(e) => {
                      if (e.keyCode == "13") {
                        OnEnterPress(e.target.name, e.target.value, "dr");
                      }
                    }}
                    disabled={
                      voucherValue.type == "cr" || addShow ? true : false
                    }
                  />
                  <span className="status dr" value="dr">Dr</span>
                  {/* <select
                    className="form-control status dr"
                    disabled={
                      voucherValue.type == "cr" || addShow ? true : false
                    }
                  >
                    <option value="dr">DR</option>
                    <option value="cr">CR</option>
                  </select> */}
                </div>
              </Col>
            </Row>
            <Row>
              <Col md={3}>
                <div className="form-group">
                  <DatePicker
                    dateFormat="dd/MM/yyyy"
                    selected={selectedDate}
                    onChange={(date) => setSelectedDate(date)}
                    selected={
                      otherData.Invoice_date != undefined
                        ? new Date(otherData.Invoice_date)
                        : new Date()
                    }
                    className="form-control"
                    placeholderText="From Date"
                    disabled
                  />
                </div>
              </Col>
              <Col md={6}>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Particulars"
                    value="Purchase Account"
                    disabled={true}
                  />

                  {/* <select
                    className="form-control"
                    name="Voucher_type"
                    onChange={(e) => setVoucherData({...voucherData, [e.target.name] : e.target.value})}
                  >
                    {voucherTypeList.map((item, index) => {
                      return (
                        <option value={item} key={index}>
                          {item}
                        </option>
                      );
                    })}
                  </select> */}
                </div>
              </Col>
              <Col md={3}>
                <div className="form-group position-relative">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Amount"
                    value={voucherValue.cr}
                    name="cr"
                    onChange={(e) =>
                      onChangeValue(e.target.name, e.target.value, "cr")
                    }
                    onKeyDown={(e) => {
                      if (e.keyCode == "13") {
                        OnEnterPress(e.target.name, e.target.value, "cr");
                      }
                    }}
                    disabled={
                      voucherValue.type == "dr" || addShow ? true : false
                    }
                  />
                  <span className="status cr" value="cr">Cr</span>
                  {/* <select
                    className="form-control status cr"
                    disabled={
                      voucherValue.type == "dr" || addShow ? true : false
                    }
                  >
                    <option value="cr">CR</option>
                    <option value="dr">DR</option>
                  </select> */}
                </div>
              </Col>
            </Row>
            {thirdRow.Amount != "" && (
              <Row>
                <Col md={3}>
                  <div className="form-group">
                    <DatePicker
                      dateFormat="dd/MM/yyyy"
                      selected={
                        otherData.Invoice_date != undefined
                          ? new Date(otherData.Invoice_date)
                          : new Date()
                      }
                      selectsStart
                      className="form-control"
                      placeholderText="From Date"
                      disabled
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Particulars"
                      value="GST"
                      disabled
                    />
                  </div>
                </Col>
                <Col md={3}>
                  <div className="form-group position-relative">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Amount"
                      value={thirdRow.Amount}
                      name={thirdRow.type}
                      // onChange={e => onChangeValue(e.target.name, e.target.value,"dr")}
                      // onKeyDown={e => {
                      //   if(e.keyCode == '13'){
                      //     OnEnterPress(e.target.name, e.target.value,"dr")

                      //   }
                      // }}
                      // disabled={voucherValue.type == "cr" || addShow ? true : false}
                      disabled
                    />
                    <span className={`status ${thirdRow.type}`} value={thirdRow.type}>{thirdRow.type}</span>
                    <div
                      className="close-icon"
                      style={{ right: "-14px", top: "5px" }}
                    >
                      <span
                        onClick={() => deleteRow("thirdRow")}
                        style={{ right: "-14px", top: "5px" }}
                      >
                        x
                      </span>
                    </div>
                  </div>
                </Col>
              </Row>
            )}
            {thirdRow.type == "" && addShow && (
              <>
                <Row>
                  <Col md={3}>
                    <div className="form-group">
                      <DatePicker
                        dateFormat="dd/MM/yyyy"
                        selected={
                          otherData.Invoice_date != undefined
                            ? new Date(otherData.Invoice_date)
                            : new Date()
                        }
                        selectsStart
                        className="form-control"
                        placeholderText="From Date"
                        disabled
                      />
                    </div>
                  </Col>
                  <Col md={6}>
                    {/* <select
                    className="form-control"
                    name="Voucher_type"
                    onChange={(e) => setVoucherData({...voucherData, [e.target.name] : e.target.value})}
                  >
                    {voucherTypeList.map((item, index) => {
                      return (
                        <option value={item} key={index}>
                          {item}
                        </option>
                      );
                    })}
                  </select> */}
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Particulars"
                        value="Purchase Account"
                        disabled
                      />
                    </div>
                  </Col>
                  <Col md={3}>
                    <div className="form-group position-relative">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Amount"
                        value={TotalAmount != undefined ? TotalAmount : ""}
                        disabled
                      />
                      {/* {select className add dynemic cr or dr} */}
                      <span className="status dr" value="dr">Dr</span>
                      {/* <select className="form-control status dr" disabled>
                        <option value="dr">DR</option>
                        <option value="cr">CR</option>
                      </select> */}
                    </div>
                    <div
                      className="close-icon"
                      style={{ right: "-14px", top: "5px" }}
                    >
                      <span onClick={() => deleteRow("addShow")}>x</span>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col md={3}>
                    <div className="form-group">
                      <DatePicker
                        dateFormat="dd/MM/yyyy"
                        selected={selectedDate}
                        onChange={(date) => setSelectedDate(date)}
                        selected={
                          otherData.Invoice_date != undefined
                            ? new Date(otherData.Invoice_date)
                            : new Date()
                        }
                        className="form-control"
                        placeholderText="From Date"
                        disabled
                      />
                    </div>
                  </Col>
                  <Col md={6}>
                    <div className="form-group">
                      {/* <input
                    type="text"
                    className="form-control"
                    placeholder="Particulars"
                  /> */}
                      <select
                        className="form-control"
                        name="EditLegder2"
                        onChange={(e) =>
                          setVoucherData({
                            ...voucherData,
                            [e.target.name]: e.target.value,
                          })
                        }
                      >
                        {sellerCompanyList.map((item) => {
                          return (
                            <option value={item.comp_name} key={item.id}>
                              {item.comp_name}
                            </option>
                          );
                        })}
                      </select>
                    </div>
                  </Col>
                  <Col md={3}>
                    <div className="form-group position-relative">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Amount"
                        value={TotalAmount != undefined ? TotalAmount : ""}
                        disabled
                      />
                      {/* {select className add dynemic cr or dr} */}
                      <span className="status cr" value="cr">cr</span>
                      {/* <select className="form-control status cr" disabled>
                        <option value="cr">CR</option>
                        <option value="dr">DR</option>
                      </select> */}
                    </div>
                    <div
                      className="close-icon"
                      style={{ right: "-14px", top: "5px" }}
                    >
                      <span onClick={() => deleteRow("addShow")}>x</span>
                    </div>
                  </Col>
                </Row>
              </>
            )}
            <Row>
              <Col md={4}>
                <div className="form-group">
                  <label className="font-size-14px pl-2">Narration</label>
                  <input
                    name="Transaction"
                    className="form-control"
                    type="text"
                    placeholder="Narration"
                    name="Narration"
                    onChange={(e) =>
                      setVoucherData({
                        ...voucherData,
                        [e.target.name]: e.target.value,
                      })
                    }
                    value={voucherData.Narration}
                  />
                </div>
              </Col>
              <Col md={4}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">Voucher Type:</label>
                  <select
                    className="form-control"
                    name="Vouchetype"
                    onChange={(e) =>
                      setVoucherData({
                        ...voucherData,
                        [e.target.name]: e.target.value,
                      })
                    }
                  >
                    {VoucherList.map((item, index) => {
                      return (
                        <option value={item} key={index}>
                          {item}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </Col>
              <Col md={4}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">Voucher No:</label>
                  <input
                    className="form-control"
                    type="text"
                    placeholder="Voucher No"
                    name="Vouchenumber"
                    onChange={(e) =>
                      setVoucherData({
                        ...voucherData,
                        [e.target.name]: e.target.value,
                      })
                    }
                  />
                </div>
              </Col>
              <Col
                md={12}
                className="d-flex flex-wrap align-items-center justify-content-md-end"
              >
                <button className="ok-btn " onClick={SubmitVoucher}>
                  Ok
                </button>
                <button
                  className="add-btn ml-md-2"
                  onClick={() => setAddShow(true)}
                >
                  Add
                </button>
                <button
                  className="delete-btn ml-md-2"
                  onClick={props.handleClose}
                >
                  Delete
                </button>
              </Col>
            </Row>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
};
