import React from "react"
import { Button, Modal } from "react-bootstrap";
import { WarningIcon } from "../Icons/Icons";

export const Modal2 = (props) => {
    return (<Modal show={props.show} onHide={props.handleClose} centered>
        <Modal.Header className="border-0 pb-0" closeButton></Modal.Header>
        <Modal.Body className="text-center pt-0 pb-4">
            <WarningIcon />
            <div className="font-size-lg-28px font-size-20px font-weight-700 text-primary py-2">{props.title}</div>
            <div className="font-size-md-16px font-size-14px pb-3">{props.text}</div>
            <div className="d-flex justify-content-center">
                <button className="ok-btn w-auto h-auto mr-3 px-4 py-2" onClick={props.handleClose}>
                    Secondary CTA
                </button>
                <Button variant="primary" className="px-4 py-2" onClick={props.handleClose}>
                    Primary CTA
                </Button>
            </div>
        </Modal.Body>
    </Modal>)
}