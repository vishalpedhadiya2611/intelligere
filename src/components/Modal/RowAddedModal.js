import React from "react"
import { Button, Modal } from "react-bootstrap";

export const RowAddedModal = (props) => {
    return (<Modal show={props.show} onHide={props.handleClose} centered>
        <Modal.Header className="border-0 pb-0" closeButton></Modal.Header>
        <Modal.Body className="text-center pt-0 pb-4">
            <div className="font-size-lg-28px font-size-20px font-weight-700 text-primary pb-2">{props.title}</div>
            <div className="font-size-md-16px font-size-14px pb-3">{props.text}</div>
            <div className="d-flex justify-content-center">
                <Button variant="primary" className="px-4 py-2" onClick={props.handleClose}>
                    OKAY
                </Button>
            </div>
        </Modal.Body>
    </Modal>)
}