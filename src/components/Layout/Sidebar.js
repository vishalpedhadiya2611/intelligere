import React, { useEffect } from "react";
import { Navigation } from "react-minimal-side-navigation";
import { useHistory, useLocation } from "react-router-dom";
import PerfectScrollbar from "react-perfect-scrollbar";
import logo from "./../../assets/logo.svg";
import { BankStatementIcon, DashboardIcon, InvoiceIcon } from "../Icons/Icons";
import Icon from "awesome-react-icons";
import { CompanyListAPI } from "../../container/BankStatements/Api";
import { companyList } from "../../redux/action/BankStatementAction";
import { useDispatch } from "react-redux";

const perfectScrollbarOptions = {
    wheelSpeed: 2,
    wheelPropagation: false,
};

const Sidebar = () => {
    const dispatch=useDispatch();
    const history = useHistory();
    const location = useLocation();

    const handleSidebar = () => {
        document.body.classList.toggle('TM_sidebarOpen');
    }

    return (
        <React.Fragment>
            <div className="TM_sidebar">
                <div className="TM_logo d-flex align-items-center">
                    <button
                        className="btn btn-link p-0 mr-3 text-dark TM_sidebarBTN"
                        type="button"
                        onClick={handleSidebar}
                    >
                        <Icon name="burger" />
                    </button><img src={logo} alt="Logo" width="100px" />
                </div>


                <PerfectScrollbar
                    options={perfectScrollbarOptions}
                    style={{ maxHeight: "calc(100vh - 70px)" }}
                >

                    <Navigation
                        activeItemId={location.pathname}
                        onSelect={({ itemId }) => {
                            history.push(itemId);
                            document.body.classList.toggle('TM_sidebarOpen')
                        }}
                        items={[
                            {
                                title: "Dashboard",
                                itemId: "/dashboard",
                                elemBefore: () => <DashboardIcon />
                            },
                            {
                                title: "Bank Statement",
                                itemId: "/bank-statement-emport",
                                elemBefore: () => <BankStatementIcon />,
                                subNav: [
                                    {
                                        title: "Import",
                                        itemId: "/bank-statement-emport"
                                    },
                                    {
                                        title: "Reports",
                                        itemId: "/bank-statement-report"
                                    },
                                ]
                            },
                            {
                                title: "Invoice",
                                itemId: "/invoice-create",
                                elemBefore: () => <InvoiceIcon />,
                                subNav: [
                                    {
                                        title: "Create",
                                        itemId: "/invoice-create"
                                    },
                                    {
                                        title: "Import",
                                        itemId: "/invoice-import"
                                    },
                                    {
                                        title: "Report",
                                        itemId: "/invoice-report"
                                    },
                                ]
                            },
                            {
                                title: 'Bank Cheque',
                                itemId: '/bankcheque/dashboard',
                                elemBefore: () => <InvoiceIcon />,
                                subNav: [
                                    {
                                        title: "Dashboard",
                                        itemId: '/bankcheque/dashboard'
                                    },
                                    {
                                        title: "Payment",
                                        itemId: "/payment"
                                    },
                                    {
                                        title: "Organization",
                                        itemId: "/organization"
                                    },
                                    {
                                        title: "Bank",
                                        itemId: "/bank"
                                    }
                                ]
                            },
                            {
                                title: "Extra",
                                itemId: "/extra",
                                elemBefore: () => <DashboardIcon />
                            },
                        ]}
                    />
                </PerfectScrollbar>
            </div>
        </React.Fragment >
    );
};
export default Sidebar;