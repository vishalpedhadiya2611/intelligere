import React from "react";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";

import logoIcon from "./../../assets/logoIcon.jpg"

const Footer = () => {
    return (<>
        <footer>
            <Container fluid>
                <div className="text-dark order-2 order-md-1">
                    <span className="text-muted fw-bold me-2"><img src={logoIcon} alt="logoIcon" width="20px"/> 2021 ©</span>
                    <Link to="/dashboard" className="pl-2 gray-800 text-hover-primary">Digital Documentation Systems</Link>
                </div>
            </Container>
        </footer>
    </>)

}
export default Footer;