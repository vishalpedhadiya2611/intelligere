import React, { useState } from "react";
import { Navbar, Dropdown, Container, Nav, Button } from "react-bootstrap";
import { addBankStatementData } from "../../redux/action/BankStatementAction";
import { useDispatch, useSelector } from "react-redux";
import Icon from "awesome-react-icons";
import user from "./../../assets/avatar.png";
import logo from "./../../assets/logo.svg";
import {
  HelpIcon,
  NotificationIcon,
  PlusIcon,
  SearchIcon,
} from "../Icons/Icons";
import Help from "./Help";
import { NewVoucherEntry } from "../Modal/NewVoucherEntry";
import { NewLedgerCreation } from "../Modal/NewLedgerCreation";
import { NewGroupCreation } from "../Modal/NewGroupCreation";
import {toast } from 'react-toastify';
import { Error } from "../Message/Message";

const Header = () => {
  const dispatch = useDispatch();
  const handleSidebar = () => {
    document.body.classList.toggle("TM_sidebarOpen");
  };

  const updatedBankData = useSelector(
    (state) => state.bankStatement.updatedBankStmtList
  );
  const [searchShow, setSearchShow] = useState(false);
  const handleSearchShow = () => setSearchShow(!searchShow);
  const handleSearchHide = () => setSearchShow(false);
  const [HelpShow, setHelpShow] = useState(false);
  const handleShowHelp = () => setHelpShow(!HelpShow);
  //handleShowVoucherEntry
  const [ShowVoucherEntry, setShowVoucherEntry] = useState(false);
  const handleShowVoucherEntry = () => setShowVoucherEntry(true);
  const handleHideVoucherEntry = () => setShowVoucherEntry(false);
  //handleShowLedgerCreation
  const [ShowLedgerCreation, setShowLedgerCreation] = useState(false);
  const handleShowLedgerCreation = () => setShowLedgerCreation(true);
  const handleHideLedgerCreation = () => setShowLedgerCreation(false);
  //handleShowGroupCreation
  const [ShowGroupCreation, setShowGroupCreation] = useState(false);
  const handleShowGroupCreation = () => setShowGroupCreation(true);
  const handleHideGroupCreation = () => setShowGroupCreation(false);

  const onClickNewVoucherEntryButton = () => {
    if (updatedBankData.length > 0) {
      dispatch(addBankStatementData());
    }else{
      toast.error(<Error text="Please Import PDF First" />)
    }
  };

  return (
    <>
      <header>
        <Navbar expand="lg" className="p-0">
          <Container
            fluid
            className="d-flex align-items-center justify-content-between"
          >
            <div className="col-lg-4 pl-lg-0 d-flex align-items-center justify-content-between">
              <div className="d-flex align-items-center">
                <button
                  className="btn btn-link p-0 mr-3 text-dark TM_sidebarBTN"
                  type="button"
                  onClick={handleSidebar}
                >
                  <Icon name="burger" />
                </button>
                <div className="TM_logo ">
                  <img src={logo} alt="Logo" />
                </div>
              </div>
              <div className="d-flex align-items-center">
                <div className="d-block d-lg-none position-relative pr-3">
                  <Button
                    variant="link"
                    className="p-0"
                    onClick={handleSearchShow}
                  >
                    <SearchIcon />
                  </Button>
                  {searchShow && (
                    <div className="searchHdr">
                      <input type="text" placeholder="Search here" />
                      <Button
                        variant="link"
                        className="p-0"
                        onClick={handleSearchHide}
                      >
                        <SearchIcon />
                      </Button>
                    </div>
                  )}
                </div>
                <Navbar.Toggle aria-controls="TM_navbar" />
              </div>
            </div>
            <div className="searchHdr d-none d-lg-block col-lg-4">
              <Button variant="link" className="p-0">
                <SearchIcon />
              </Button>
              <input type="text" placeholder="Search here" />
            </div>
            <Navbar.Collapse id="TM_navbar" className="justify-content-end">
              <Nav>
                <Dropdown id="TM_new">
                  <Dropdown.Toggle variant="primary">
                    <PlusIcon />
                    <span className="pl-2">New</span>
                  </Dropdown.Toggle>
                  <Dropdown.Menu alignRight className="TM_headerDropDown">
                    <Dropdown.Item
                      className="font-size-12px"
                      onClick={onClickNewVoucherEntryButton}
                    >
                      New voucher entry
                    </Dropdown.Item>
                    <Dropdown.Item
                      className="font-size-12px"
                      onClick={handleShowLedgerCreation}
                    >
                      New ledger creation
                    </Dropdown.Item>
                    <Dropdown.Item
                      className="font-size-12px"
                      onClick={handleShowGroupCreation}
                    >
                      New group creation
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown>
                  <Dropdown.Toggle variant="link" id="TM_dropdownNotification">
                    <NotificationIcon />
                  </Dropdown.Toggle>
                  <Dropdown.Menu alignRight className="TM_headerDropDown">
                    <Dropdown.Item href="#/action-1">
                      Lorem ipsum dolor sit amet,{" "}
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-2">
                      Lorem ipsum dolor sit amet,{" "}
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-3">
                      Lorem ipsum dolor sit amet,{" "}
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-4">
                      Lorem ipsum dolor sit amet,{" "}
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-5">
                      Lorem ipsum dolor sit amet,{" "}
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-6" className="text-primary">
                      View All
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown>
                  <Dropdown.Toggle
                    variant="link"
                    id="TM_dropdownUser"
                    style={{ backgroundImage: `url(${user})` }}
                  ></Dropdown.Toggle>
                  <Dropdown.Menu alignRight className="TM_headerDropDown">
                    <Dropdown.Item href="#/action-1">Profile</Dropdown.Item>
                    <Dropdown.Item href="#/action-3">Logout</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </header>
      <div className="helpMain" style={{ right: HelpShow ? "0" : "" }}>
      <Button variant="primary" className="btnHelp" onClick={handleShowHelp}>
                {/* <HelpIcon /> */}
                H<br/>e<br/>l<br/>p<br/>
            </Button>
            <Help />
      </div>
      <NewVoucherEntry
        show={ShowVoucherEntry}
        handleClose={handleHideVoucherEntry}
      />
      <NewLedgerCreation
        show={ShowLedgerCreation}
        handleClose={handleHideLedgerCreation}
      />
      <NewGroupCreation
        show={ShowGroupCreation}
        handleClose={handleHideGroupCreation}
      />
    </>
  );
};
export default Header;
