export const Title = (props) => {
  return (
    <div className="font-size-lg-28px font-size-20px font-weight-700 text-primary pb-md-4 pb-3">
      {props.title}
    </div>
  );
};
