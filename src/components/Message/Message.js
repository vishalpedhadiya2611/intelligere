import React from "react"
import { ErrorIcon, SuccessIcon, WIcon } from "../Icons/Icons"

export const Success = (props) => {
    return (
        <div className="d-flex align-items-center"><SuccessIcon /><div className="pl-2 text-success font-size-14px">{props.text}</div></div>
    )
}
export const Error = (props) => {
    return (
        <div className="d-flex align-items-center"><ErrorIcon /><div className="pl-2 text-danger font-size-14px">{props.text}</div></div>
    )
}
export const Warning = (props) => {
    return (
        <div className="d-flex align-items-center"><WIcon /><div className="pl-2 text-warning font-size-14px">{props.text}</div></div>
    )
}