export const BankStatementIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
        <g id="vuesax_bold_receipt-text" data-name="vuesax/bold/receipt-text" transform="translate(-108 -444)">
            <g id="receipt-text" transform="translate(108 444)">
                <path id="Vector" d="M0,0H20V20H0Z" fill="none" opacity="0" />
                <path id="Vector-2" className="iconColor" data-name="Vector" d="M4.167,0H3.333C.833,0,0,1.492,0,3.333v12.5a.831.831,0,0,0,1.333.667l1.425-1.067a.84.84,0,0,1,1.1.083l1.383,1.392a.84.84,0,0,0,1.183,0l1.4-1.4a.826.826,0,0,1,1.083-.075L10.333,16.5a.835.835,0,0,0,1.333-.667V1.667A1.672,1.672,0,0,1,13.333,0ZM7.708,9.792H3.958a.625.625,0,1,1,0-1.25h3.75a.625.625,0,0,1,0,1.25Zm.625-3.333h-5a.625.625,0,0,1,0-1.25h5a.625.625,0,0,1,0,1.25Z" transform="translate(1.667 1.667)" />
                <path id="Vector-3" className="iconColor" data-name="Vector" d="M1.675,0V1.25a2.1,2.1,0,0,1,1.458.6A2.109,2.109,0,0,1,3.75,3.333V5.35c0,.617-.275.9-.9.9H1.25V1.675a.427.427,0,0,1,.425-.425Zm0,0A1.675,1.675,0,0,0,0,1.675V7.5H2.85A2,2,0,0,0,5,5.35V3.333A3.329,3.329,0,0,0,1.675,0Z" transform="translate(13.333 1.667)" />
            </g>
        </g>
    </svg>
    )
}

export const DashboardIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
        <g id="vuesax_bold_folder-2" data-name="vuesax/bold/folder-2" transform="translate(-428 -188)">
            <g id="folder-2" transform="translate(428 188)">
                <path id="Vector" d="M6.183,2.208a2.841,2.841,0,0,0-.55-.183A4.474,4.474,0,0,0,4.367,1.85H1.408L.325.408A4.983,4.983,0,0,0,0,0H2.817A3.661,3.661,0,0,1,6.183,2.208Z" transform="translate(10.283 1.667)" className="iconColor" />
                <path id="Vector-2" data-name="Vector" d="M15.117,3.783a3.483,3.483,0,0,0-1.208-.558,3.44,3.44,0,0,0-.925-.125h-3.1c-.483,0-.517-.042-.775-.383L7.942,1.167A2.389,2.389,0,0,0,5.617,0H3.683A3.685,3.685,0,0,0,0,3.683v9.3a3.685,3.685,0,0,0,3.683,3.683h9.3a3.685,3.685,0,0,0,3.683-3.683v-6.2A3.659,3.659,0,0,0,15.117,3.783ZM10.325,11.95H6.333a.583.583,0,0,1,0-1.167h3.992a.583.583,0,0,1,0,1.167Z" transform="translate(1.667 1.667)" className="iconColor" />
                <path id="Vector-3" data-name="Vector" d="M0,0H20V20H0Z" transform="translate(20 20) rotate(180)" fill="none" opacity="0" />
            </g>
        </g>
    </svg>

    )
}
export const InvoiceIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
        <g id="vuesax_bold_receipt-2" data-name="vuesax/bold/receipt-2" transform="translate(-492 -380)" opacity="0.7">
            <g id="receipt-2" transform="translate(492 380)">
                <path id="Vector" d="M0,0H20V20H0Z" fill="none" opacity="0" />
                <path id="Vector-2" data-name="Vector" d="M13.75,1.392c0-.008,0-.017-.017-.025a.876.876,0,0,0-.708-.358,2.335,2.335,0,0,0-1.55.908,1.473,1.473,0,0,1-2.333-.125L8.3.675A1.525,1.525,0,0,0,7.083,0,1.525,1.525,0,0,0,5.867.675L5.017,1.8A1.466,1.466,0,0,1,2.7,1.925l-.008-.008C1.75.908.908.758.433,1.367c-.017.008-.017.017-.017.025A6.833,6.833,0,0,0,0,4.2v8.267a6.833,6.833,0,0,0,.417,2.808.063.063,0,0,0,.017.033c.483.6,1.317.45,2.258-.558l.008-.008a1.466,1.466,0,0,1,2.317.125l.85,1.125a1.525,1.525,0,0,0,1.217.675A1.525,1.525,0,0,0,8.3,15.992l.842-1.117a1.473,1.473,0,0,1,2.333-.125,2.335,2.335,0,0,0,1.55.908.868.868,0,0,0,.708-.35.063.063,0,0,0,.017-.033,6.833,6.833,0,0,0,.417-2.808V4.2A6.833,6.833,0,0,0,13.75,1.392ZM9.167,10.417h-5a.625.625,0,0,1,0-1.25h5a.625.625,0,0,1,0,1.25ZM10.833,7.5H4.167a.625.625,0,0,1,0-1.25h6.667a.625.625,0,0,1,0,1.25Z" transform="translate(2.5 1.667)" className="iconColor" />
            </g>
        </g>
    </svg>
    )
}
export const NotificationIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
        <g id="Group_64" data-name="Group 64" transform="translate(-1748 -16)">
            <g id="NoPath_-_Copy_37_" data-name="NoPath - Copy (37)" transform="translate(1748 16)" fill="#fff" className="iconStrokeColor" strokeWidth="2" opacity="0.1">
                <rect width="48" height="48" rx="10" stroke="none" />
                <rect x="1" y="1" width="46" height="46" rx="9" fill="none" />
            </g>
            <g id="vuesax_bold_notification" data-name="vuesax/bold/notification" transform="translate(1590 -158)" opacity="0.5">
                <g id="notification" transform="translate(172 188)">
                    <path id="Vector" d="M12.524,10.408l-.833-1.383a3.1,3.1,0,0,1-.333-1.233V5.683A4.924,4.924,0,0,0,8.549,1.242,2.438,2.438,0,0,0,6.4,0,2.467,2.467,0,0,0,4.241,1.267a4.94,4.94,0,0,0-2.75,4.417V7.792a3.022,3.022,0,0,1-.333,1.225L.316,10.408A1.948,1.948,0,0,0,1.408,13.35a15.585,15.585,0,0,0,5.017.817,15.808,15.808,0,0,0,5.017-.808,2.01,2.01,0,0,0,1.083-2.95Z" transform="translate(3.592 1.667)" className="iconColor" />
                    <path id="Vector-2" data-name="Vector" d="M4.708.008A2.512,2.512,0,0,1,2.35,1.667,2.471,2.471,0,0,1,.583.925,2.3,2.3,0,0,1,0,0C.108.017.217.025.333.042c.192.025.392.05.592.067C1.4.15,1.883.175,2.367.175S3.317.15,3.783.108c.175-.017.35-.025.517-.05Z" transform="translate(7.65 16.667)" className="iconColor" />
                    <path id="Vector-3" data-name="Vector" d="M0,0H20V20H0Z" transform="translate(20 20) rotate(180)" fill="none" opacity="0" />
                </g>
            </g>
        </g>
    </svg>

    )
}
export const PlusIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="16.658" height="16.667" viewBox="0 0 16.658 16.667">
        <path id="Vector" d="M11.825,0H4.842A4.447,4.447,0,0,0,0,4.842v6.975a4.45,4.45,0,0,0,4.842,4.85h6.975a4.447,4.447,0,0,0,4.842-4.842V4.842A4.437,4.437,0,0,0,11.825,0Zm1.508,8.958H8.958v4.375a.625.625,0,0,1-1.25,0V8.958H3.333a.625.625,0,0,1,0-1.25H7.708V3.333a.625.625,0,0,1,1.25,0V7.708h4.375a.625.625,0,1,1,0,1.25Z" fill="#fff" />
    </svg>
    )
}
export const SearchIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="16.955" height="16.955" viewBox="0 0 16.955 16.955">
        <g id="vuesax_linear_search-normal" data-name="vuesax/linear/search-normal" transform="translate(-427.833 -187.833)" opacity="0.3">
            <g id="search-normal" transform="translate(428 188)">
                <path id="Vector" d="M12.667,6.333A6.333,6.333,0,1,1,6.333,0,6.333,6.333,0,0,1,12.667,6.333Z" transform="translate(1.333 1.333)" fill="none" className="iconStrokeColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" />
                <path id="Vector-2" data-name="Vector" d="M1.333,1.333,0,0" transform="translate(13.333 13.333)" fill="none" className="iconStrokeColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" />
                <path id="Vector-3" data-name="Vector" d="M0,0H16V16H0Z" fill="none" opacity="0" />
            </g>
        </g>
    </svg>

    )
}
export const BackIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
        <g id="Group_65" data-name="Group 65" transform="translate(-320 -97)">
            <path id="Rectangle_1950" data-name="Rectangle 1950" d="M0,0H20A20,20,0,0,1,40,20v0A20,20,0,0,1,20,40H0a0,0,0,0,1,0,0V0A0,0,0,0,1,0,0Z" transform="translate(320 97)" className="iconColor" />
            <g id="arrow_upward_black_24dp" transform="translate(330 127) rotate(-90)">
                <path id="Path_1157" data-name="Path 1157" d="M0,0H20V20H0Z" fill="none" />
                <path id="Path_1158" data-name="Path 1158" d="M11.568,16.57V7.261l4.067,4.067a.84.84,0,0,0,1.183,0,.83.83,0,0,0,0-1.175L11.326,4.661a.83.83,0,0,0-1.175,0l-5.5,5.483A.831.831,0,0,0,5.826,11.32L9.9,7.261V16.57a.833.833,0,0,0,1.667,0Z" transform="translate(-0.735 -0.736)" fill="#fff" />
            </g>
        </g>
    </svg>
    )
}
export const HelpIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="93" height="40" viewBox="0 0 93 40">
        <g id="Group_66" data-name="Group 66" transform="translate(-1827 -116)">
            <path id="Rectangle_1951" data-name="Rectangle 1951" d="M20,0H93a0,0,0,0,1,0,0V40a0,0,0,0,1,0,0H20A20,20,0,0,1,0,20v0A20,20,0,0,1,20,0Z" transform="translate(1827 116)" className="iconColor" />
            <text id="Help" transform="translate(1869 141)" fill="#fff" fontSize="18" fontFamily="JosefinSans-Regular, Josefin Sans" letterSpacing="-0.02em"><tspan x="0" y="0">Help</tspan></text>
            <g id="vuesax_bold_message-question" data-name="vuesax/bold/message-question" transform="translate(1219 -126)">
                <g id="message-question" transform="translate(620 252)">
                    <path id="Vector" d="M0,0H20V20H0Z" fill="none" opacity="0" />
                    <path id="Vector-2" data-name="Vector" d="M12.5,0H4.167A3.937,3.937,0,0,0,0,4.167v5a3.937,3.937,0,0,0,4.167,4.167v1.775a.828.828,0,0,0,1.292.692l3.708-2.467H12.5a3.937,3.937,0,0,0,4.167-4.167v-5A3.937,3.937,0,0,0,12.5,0ZM8.333,10.142a.625.625,0,1,1,.625-.625A.624.624,0,0,1,8.333,10.142Zm1.05-3.458c-.325.217-.425.358-.425.592V7.45a.625.625,0,1,1-1.25,0V7.275A1.939,1.939,0,0,1,8.683,5.65c.308-.208.408-.35.408-.567a.758.758,0,1,0-1.517,0,.625.625,0,0,1-1.25,0,2.008,2.008,0,0,1,4.017,0A1.912,1.912,0,0,1,9.383,6.683Z" transform="translate(1.667 2.025)" fill="#fff" />
                </g>
            </g>
        </g>
    </svg>
    )
}
export const LogoIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="70" height="70" viewBox="0 0 70 70">
        <g id="Group_2" data-name="Group 2" transform="translate(-925 -160)">
            <rect id="Rectangle_1916" data-name="Rectangle 1916" width="70" height="70" rx="15" transform="translate(925 160)" fill="#13669a" />
            <g id="vuesax_bold_security-card" data-name="vuesax/bold/security-card" transform="translate(640 -269)">
                <g id="security-card" transform="translate(300 444)">
                    <path id="Vector" d="M0,0H40V40H0Z" fill="none" opacity="0" />
                    <path id="Vector-2" data-name="Vector" d="M5.05,0H.9a.9.9,0,0,0,0,1.8H5.05a.9.9,0,1,0,0-1.8Z" transform="translate(17.433 22.3)" fill="#fff" />
                    <path id="Vector-3" data-name="Vector" d="M2.983,0H.9a.9.9,0,0,0,0,1.8H2.983a.9.9,0,1,0,0-1.8Z" transform="translate(12.75 22.3)" fill="#fff" />
                    <path id="Vector-4" data-name="Vector" d="M24.983,3.7,15.817.263a5.8,5.8,0,0,0-3.45,0L3.2,3.7A5.305,5.305,0,0,0,0,8.313v13.5a5.534,5.534,0,0,0,1.967,3.933l9.167,6.85a5.2,5.2,0,0,0,5.883,0l9.167-6.85a5.565,5.565,0,0,0,1.967-3.933V8.313A5.272,5.272,0,0,0,24.983,3.7Zm-.617,15.717c-.033,3.45-.983,4.317-4.567,4.317H8.383c-3.65,0-4.567-.9-4.567-4.517V15.329A.825.825,0,0,1,4.65,14.5H23.533a.825.825,0,0,1,.833.833Zm0-7.567a.825.825,0,0,1-.833.833H4.65a.825.825,0,0,1-.833-.833V10.779c0-3.267.767-4.317,3.617-4.5.3,0,.617-.017.95-.017H19.8c3.65,0,4.567.9,4.567,4.517Z" transform="translate(5.917 3.338)" fill="#fff" />
                </g>
            </g>
            <g id="Rectangle_1920" data-name="Rectangle 1920" transform="translate(928 163)" fill="none" stroke="#fff" strokeWidth="2" opacity="0.1">
                <rect width="64" height="64" rx="15" stroke="none" />
                <rect x="1" y="1" width="62" height="62" rx="14" fill="none" />
            </g>
        </g>
    </svg>

    )
}
export const ContactIcon = () => {
    return (<svg id="Component_9_2" data-name="Component 9 – 2" xmlns="http://www.w3.org/2000/svg" width="141" height="44" viewBox="0 0 141 44">
        <g id="Rectangle_1903" data-name="Rectangle 1903" fill="#fff" stroke="#999" strokeWidth="2" opacity="0.1">
            <rect width="141" height="44" rx="10" stroke="none" />
            <rect x="1" y="1" width="139" height="42" rx="9" fill="none" />
        </g>
        <g id="Group_24" data-name="Group 24" transform="translate(-181 -27)">
            <text id="Contact" transform="translate(233 55)" fontSize="18" fontFamily="JosefinSans-Regular, Josefin Sans" opacity="0.4"><tspan x="0" y="0">Contact</tspan></text>
            <g id="mail_black_24dp" transform="translate(205 40)">
                <path id="Path_1154" data-name="Path 1154" d="M0,0H18V18H0Z" fill="none" />
                <path id="Path_1155" data-name="Path 1155" d="M15.5,4H3.5A1.5,1.5,0,0,0,2,5.5v9A1.5,1.5,0,0,0,3.5,16h12A1.5,1.5,0,0,0,17,14.5v-9A1.5,1.5,0,0,0,15.5,4Zm-.3,3.188L10.3,10.255a1.486,1.486,0,0,1-1.59,0L3.8,7.188a.637.637,0,1,1,.675-1.08L9.5,9.25l5.025-3.142a.637.637,0,1,1,.675,1.08Z" transform="translate(-0.5 -1)" fill="#999" />
            </g>
        </g>
    </svg>


    )
}
export const UserIcon = () => {
    return (<svg id="user" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
        <path id="Vector" d="M0,0H20V20H0Z" fill="none" opacity="0" />
        <path id="Vector-2" data-name="Vector" d="M8.333,4.167A4.167,4.167,0,1,1,4.167,0,4.167,4.167,0,0,1,8.333,4.167Z" transform="translate(5.833 1.667)" className="iconColor" />
        <path id="Vector-3" data-name="Vector" d="M7.575,0C3.4,0,0,2.8,0,6.25a.413.413,0,0,0,.417.417H14.733a.413.413,0,0,0,.417-.417C15.15,2.8,11.75,0,7.575,0Z" transform="translate(2.425 12.083)" className="iconColor" />
    </svg>
    )
}
export const LockIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
        <g id="vuesax_bold_lock" data-name="vuesax/bold/lock" transform="translate(-172 -252)">
            <path id="Vector" d="M2.717,1.358A1.358,1.358,0,1,1,1.358,0,1.358,1.358,0,0,1,2.717,1.358Z" transform="translate(180.642 263.742)" className="iconColor" />
            <path id="Vector-2" data-name="Vector" d="M13.567,6.275V5.233C13.567,2.983,13.025,0,8.333,0S3.1,2.983,3.1,5.233V6.275C.767,6.567,0,7.75,0,10.658v1.55c0,3.417,1.042,4.458,4.458,4.458h7.75c3.417,0,4.458-1.042,4.458-4.458v-1.55C16.667,7.75,15.9,6.567,13.567,6.275ZM8.333,13.95a2.517,2.517,0,1,1,2.517-2.517A2.52,2.52,0,0,1,8.333,13.95ZM4.458,6.2H4.267V5.233c0-2.442.692-4.067,4.067-4.067S12.4,2.792,12.4,5.233v.975H4.458Z" transform="translate(173.667 253.667)" className="iconColor" />
            <path id="Vector-3" data-name="Vector" d="M0,0H20V20H0Z" transform="translate(192 272) rotate(180)" fill="none" opacity="0" />
        </g>
    </svg>

    )
}
export const EyeIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" className="iconColor" viewBox="0 0 16 16">
        <path d="M13.359 11.238C15.06 9.72 16 8 16 8s-3-5.5-8-5.5a7.028 7.028 0 0 0-2.79.588l.77.771A5.944 5.944 0 0 1 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.134 13.134 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755-.165.165-.337.328-.517.486l.708.709z" />
        <path d="M11.297 9.176a3.5 3.5 0 0 0-4.474-4.474l.823.823a2.5 2.5 0 0 1 2.829 2.829l.822.822zm-2.943 1.299.822.822a3.5 3.5 0 0 1-4.474-4.474l.823.823a2.5 2.5 0 0 0 2.829 2.829z" />
        <path d="M3.35 5.47c-.18.16-.353.322-.518.487A13.134 13.134 0 0 0 1.172 8l.195.288c.335.48.83 1.12 1.465 1.755C4.121 11.332 5.881 12.5 8 12.5c.716 0 1.39-.133 2.02-.36l.77.772A7.029 7.029 0 0 1 8 13.5C3 13.5 0 8 0 8s.939-1.721 2.641-3.238l.708.709zm10.296 8.884-12-12 .708-.708 12 12-.708.708z" />
    </svg>
    )
}
export const EyeSlashIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" className="iconColor" viewBox="0 0 16 16">
        <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
        <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
    </svg>
    )
}
export const WarningIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
        <g id="vuesax_linear_warning-2" data-name="vuesax/linear/warning-2" transform="translate(-172 -700)">
            <g id="warning-2" transform="translate(172 700)">
                <path id="Vector" d="M0,0V10.938" transform="translate(25 16.146)" fill="none" className="iconStrokeColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" />
                <path id="Vector-2" data-name="Vector" d="M37.854,13.708v14.25a6.612,6.612,0,0,1-3.271,5.688L22.208,40.792a6.59,6.59,0,0,1-6.563,0L3.271,33.646A6.562,6.562,0,0,1,0,27.958V13.708A6.612,6.612,0,0,1,3.271,8.021L15.646.875a6.59,6.59,0,0,1,6.563,0L34.583,8.021A6.587,6.587,0,0,1,37.854,13.708Z" transform="translate(6.062 4.167)" fill="none" className="iconStrokeColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" />
                <path id="Vector-3" data-name="Vector" d="M0,0H50V50H0Z" fill="none" opacity="0" />
                <path id="Vector-4" data-name="Vector" d="M0,0V.208" transform="translate(25 33.75)" fill="none" className="iconStrokeColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" />
            </g>
        </g>
    </svg>
    )
}
export const SuccessIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
        <g id="vuesax_bold_tick-square" data-name="vuesax/bold/tick-square" transform="translate(-748 -252)">
            <g id="tick-square" transform="translate(748 252)">
                <path id="Vector" d="M18.92,0H7.747C2.893,0,0,2.893,0,7.747v11.16c0,4.867,2.893,7.76,7.747,7.76h11.16c4.853,0,7.747-2.893,7.747-7.747V7.747C26.667,2.893,23.773,0,18.92,0Zm.787,10.267-7.56,7.56a1,1,0,0,1-1.413,0L6.96,14.053A1,1,0,0,1,8.373,12.64l3.067,3.067,6.853-6.853a1,1,0,0,1,1.413,1.413Z" transform="translate(2.667 2.667)" fill="#008542" />
                <path id="Vector-2" data-name="Vector" d="M0,0H32V32H0Z" transform="translate(32 32) rotate(180)" fill="none" opacity="0" />
            </g>
        </g>
    </svg>
    )
}
export const ErrorIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
        <g id="vuesax_bold_forbidden-2" data-name="vuesax/bold/forbidden-2" transform="translate(-108 -700)">
            <g id="forbidden-2" transform="translate(108 700)">
                <path id="Vector" d="M0,0H32V32H0Z" fill="none" opacity="0" />
                <path id="Vector-2" data-name="Vector" d="M25.493,6.64,20.027,1.173A4.633,4.633,0,0,0,17.2,0H9.467A4.633,4.633,0,0,0,6.64,1.173L1.173,6.64A4.633,4.633,0,0,0,0,9.467V17.2a4.633,4.633,0,0,0,1.173,2.827L6.64,25.493a4.633,4.633,0,0,0,2.827,1.173H17.2a4.633,4.633,0,0,0,2.827-1.173l5.467-5.467A4.633,4.633,0,0,0,26.667,17.2V9.467A4.633,4.633,0,0,0,25.493,6.64ZM18.707,17.293a1.006,1.006,0,0,1,0,1.413,1,1,0,0,1-1.413,0l-3.96-3.96-3.96,3.96a1,1,0,0,1-1.413,0,1.006,1.006,0,0,1,0-1.413l3.96-3.96L7.96,9.373A1,1,0,0,1,9.373,7.96l3.96,3.96,3.96-3.96a1,1,0,0,1,1.413,1.413l-3.96,3.96Z" transform="translate(2.667 2.667)" fill="#c71b1b" />
            </g>
        </g>
    </svg>
    )
}
export const WIcon = () => {
    return (<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
        <g id="vuesax_bold_information" data-name="vuesax/bold/information" transform="translate(-108 -508)">
            <g id="information" transform="translate(108 508)">
                <path id="Vector" d="M26.073,11.63l-1.8-2.107a3.038,3.038,0,0,1-.613-1.68V5.577A2.583,2.583,0,0,0,21.087,3H18.82a3.08,3.08,0,0,1-1.693-.613L15.02.59a2.668,2.668,0,0,0-3.347,0L9.54,2.39A3.038,3.038,0,0,1,7.86,3H5.553A2.583,2.583,0,0,0,2.98,5.577V7.843a3.019,3.019,0,0,1-.6,1.667L.58,11.63a2.685,2.685,0,0,0,0,3.333l1.8,2.12a2.982,2.982,0,0,1,.6,1.667v2.28A2.583,2.583,0,0,0,5.553,23.6h2.32a3.076,3.076,0,0,1,1.68.613l2.107,1.8a2.668,2.668,0,0,0,3.347,0l2.107-1.8a3.038,3.038,0,0,1,1.68-.613H21.06a2.583,2.583,0,0,0,2.573-2.573V18.763a3.038,3.038,0,0,1,.613-1.68l1.8-2.107A2.633,2.633,0,0,0,26.073,11.63ZM12.327,8.15a1,1,0,0,1,2,0v6.44a1,1,0,0,1-2,0Zm1,11.653A1.333,1.333,0,1,1,14.66,18.47,1.337,1.337,0,0,1,13.327,19.8Z" transform="translate(2.673 2.69)" fill="#fa0" />
                <path id="Vector-2" data-name="Vector" d="M0,0H32V32H0Z" transform="translate(32 32) rotate(180)" fill="none" opacity="0" />
            </g>
        </g>
    </svg>
    )
}

