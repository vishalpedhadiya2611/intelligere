import {axios} from 'axios';

const ApiCall = (url,method,handleResponse) => {
    if(method === 'get'){
        axios.get(url)
        .then(function(response){
            //handele success
            handleResponse(response)
        })
        .catch(function(error){
            //handel error
        })
        .then(function(){
            //always executed
        })
    }
}

// export default ApiCall
