import {
  BANK_STMT,
  DELETE_BANKSTMT,
  UPDATED_BANKSTMT,
  ADD_BANK_STMT,
  ADD_NEW_ROW,
  COMPANYLIST,
  BANKLIST,
  LEDGERLIST,
  REMOVE_UPDATED_BANKSTMT,
  ON_ENTER,
  ADD_SELLER_DATA,
  ADD_BUYER_DATA,
  ADD_OTHER_DATA,
  ADD_PRODUCT_DATA,
} from "../constant/BankStatementTypes";

export const BankStatementData = (data) => ({
  type: BANK_STMT,
  payload: data,
});

export const deleteBankStmt = (id) => ({
  type: DELETE_BANKSTMT,
  payload: id,
});

export const addBankStatementData = () => ({
  type: ADD_BANK_STMT,
});

export const updatedBankStmtList = (data, e, index) => ({
  type: UPDATED_BANKSTMT,
  payload: data,
  eventInput: e,
  arrayIndex: index,
});
export const updateOnEnter = (parentIndex, childIndex, e) => ({
  type: ON_ENTER,
  parentIndex,
  childIndex,
  e,
});

export const addNewRow = (id) => ({
  type: ADD_NEW_ROW,
  payload: id,
});

export const companyListAction = (data) => {
  return {
    type: COMPANYLIST,
    payload: data,
  };
};

export const bankListAction = (data) => ({
  type: BANKLIST,
  payload: data,
});

export const ledgerListAction = (data) => ({
  type: LEDGERLIST,
  payload: data,
});

export const removeUpdatedBankStmtList = (data) => ({
  type: REMOVE_UPDATED_BANKSTMT,
  payload: data,
});

export const addSellerData = (data) => ({
  type: ADD_SELLER_DATA,
  payload: data,
});

export const addBuyerData = (data) => ({
  type: ADD_BUYER_DATA,
  payload: data,
});

export const addOtherData = (data) => ({
  type: ADD_OTHER_DATA,
  payload: data,
});

export const addProductData = (data) => ({
  type: ADD_PRODUCT_DATA,
  payload: data,
});

// export const Add
