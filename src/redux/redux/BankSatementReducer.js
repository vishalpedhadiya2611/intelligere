import { updatedBankStmtList } from "../action/BankStatementAction";
import {
  BANK_STMT,
  DELETE_BANKSTMT,
  UPDATED_BANKSTMT,
  ADD_BANK_STMT,
  ADD_NEW_ROW,
  REMOVE_UPDATED_BANKSTMT,
  ON_ENTER,
} from "../constant/BankStatementTypes";

const initialState = {
  bankStmtData: [],
  updatedBankStmtList: [],
};

const emptyData = {
  id: Number,
  ListAmount1: null,
  ListLegder1: null,
  ListAmount2: null,
  ListLegder2: null,
  bank: "",
  Date:
    new Date().getFullYear() +
    "-" +
    String(new Date().getMonth() + 1).padStart(2, "0") +
    "-" +
    String(new Date().getDate()).padStart(2, "0"),
  Transaction:
    new Date().getFullYear() +
    "-" +
    String(new Date().getMonth() + 1).padStart(2, "0") +
    "-" +
    String(new Date().getDate()).padStart(2, "0"),
  Legder: "",
  Cheque_no: "",
  Credit: "",
  Debit: "",
  EditLegder: null,
  EditLegder2: null,
  Vouchetype: null,
  Vouchenumber: null,
};

const createNewBlock = [
  {
    id: "",
    type: "dr",
    particular: "",
    date:
      new Date().getFullYear() +
      "-" +
      String(new Date().getMonth() + 1).padStart(2, "0") +
      "-" +
      String(new Date().getDate()).padStart(2, "0"),
    values: "",
    valueType: "bank",
    rowType: "defalut",
    vouchetype: "",
    vouchenumber: "",
    transaction: "",
    originalValue: "",
    reference: "",
    AccountantNarration: "",
  },
  {
    id: "",
    type: "cr",
    rowType: "defalut",
    particular: "",
    date:
      new Date().getFullYear() +
      "-" +
      String(new Date().getMonth() + 1).padStart(2, "0") +
      "-" +
      String(new Date().getDate()).padStart(2, "0"),
    valueType: "ledger",
    values: "",
    vouchetype: "",
    vouchenumber: "",
    transaction: "",
    originalValue: "",
    reference: "",
    AccountantNarration: "",
  },
];

const BankStatementReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_BANK_STMT:
      const a = {
        ...state,
        updatedBankStmtList: [...state.updatedBankStmtList, createNewBlock],
      };
      return a;
    case BANK_STMT:
      return {
        ...state,
        bankStmtData: [...action.payload],
      };
    case DELETE_BANKSTMT:
      const delete_data = state.bankStmtData.filter(
        (bankStmtList) => bankStmtList.id !== action.payload
      );
      return {
        ...state,
        bankStmtData: delete_data,
      };
    case ADD_NEW_ROW:
      let orignal = state.bankStmtData.filter(
        (res) => res.id == action.payload
      );
      let id =
        state.updatedBankStmtList.findIndex(
          (res) => res.index == action.payload
        ) < 0;
      if (id) {
        const newRowData = {
          index: action.payload,
          cr: {
            num_rows: 2,
            values: [orignal[0].Credit, orignal[0].Credit],
          },
          dr: {
            num_rows: 2,
            values: [orignal[0].Debit, orignal[0].Debit],
          },
        };
        const result = {
          ...state,
          updatedBankStmtList: [...state.updatedBankStmtList, newRowData],
        };
        return result;
      }
      return state;
    case UPDATED_BANKSTMT:
      let changeData = state.bankStmtData.find(
        (data) => data.id === action.payload
      );
      let allUpdateBankStmtListChangeData = [...state.updatedBankStmtList];
      let updatedBankStmtListchangeData = [...state.updatedBankStmtList].find(
        (data) => data.index === action.payload
      );

      let newChangeData = parseFloat(action.eventInput.target.value);
      let eventInputName = action.eventInput.target.getAttribute("data-name");

      //First Time
      const itemIndex = allUpdateBankStmtListChangeData.findIndex(
        (data) => data.index === action.payload
      );

      if (newChangeData != changeData[action.eventInput.target.name]) {
        if (!updatedBankStmtListchangeData) {
          let updatedBankStmt = {};
          updatedBankStmt.index = changeData.id;
          updatedBankStmt.dr = {
            num_rows: eventInputName == "Debit" ? 2 : 1,
            values: [],
          };
          updatedBankStmt.cr = {
            num_rows: eventInputName == "Credit" ? 2 : 1,
            values: [],
          };
          //Debit
          if (eventInputName == "Debit") {
            let change =
              parseFloat(changeData.Debit) - parseFloat(newChangeData);
            updatedBankStmt.dr.values.push(parseFloat(newChangeData));
            updatedBankStmt.dr.values.push(parseFloat(change));
            updatedBankStmt.cr.values.push(parseFloat(changeData.Credit));
          } else {
            let change =
              parseFloat(changeData.Credit) - parseFloat(newChangeData);
            updatedBankStmt.cr.values.push(parseFloat(newChangeData));
            updatedBankStmt.cr.values.push(parseFloat(change));
            updatedBankStmt.dr.values.push(parseFloat(changeData.Debit));
          }

          return {
            ...state,
            updatedBankStmtList: [
              ...state.updatedBankStmtList,
              updatedBankStmt,
            ],
          };
        } else {
          let updatedBankStmt = updatedBankStmtListchangeData;
          if (eventInputName == "Debit") {
            updatedBankStmt.dr.num_rows = parseInt(
              updatedBankStmt.dr.num_rows + 1
            );
          } else {
            updatedBankStmt.cr.num_rows = parseInt(
              updatedBankStmt.cr.num_rows + 1
            );
          }
          if (eventInputName == "Debit") {
            if (newChangeData != 0) {
              let prevValue = updatedBankStmt.dr.values[action.arrayIndex];
              updatedBankStmt.dr.values[action.arrayIndex] =
                parseFloat(newChangeData);

              updatedBankStmt.dr.values.push(
                parseFloat(prevValue - newChangeData)
              );
            }
          } else {
            if (newChangeData != 0) {
              let prevValue = updatedBankStmt.cr.values[action.arrayIndex];
              updatedBankStmt.cr.values[action.arrayIndex] =
                parseFloat(newChangeData);

              updatedBankStmt.cr.values.push(
                parseFloat(prevValue - newChangeData)
              );
              allUpdateBankStmtListChangeData[itemIndex] = updatedBankStmt;
            }
          }
        }
      }

      return {
        ...state,
        updatedBankStmtList: allUpdateBankStmtListChangeData,
      };
    case REMOVE_UPDATED_BANKSTMT:
      return {
        ...state,
        updatedBankStmtList: action.payload,
      };
    case ON_ENTER:
      debugger;
      let Carray = [...state.updatedBankStmtList].filter(
        (data, index) => index == action.parentIndex
      )[0];
      let currentValue = action.e.target.value;

      if (currentValue != 0 && currentValue != "" && !isNaN(currentValue)) {
        let newValue = {};
        newValue.type = Carray[action.childIndex].type;
        newValue.particular = Carray[action.childIndex].particular;
        newValue.date = Carray[action.childIndex].date;
        newValue.rowType = Carray[action.childIndex].rowType;
        newValue.valueType = Carray[action.childIndex].valueType;
        newValue.values = (
          parseFloat(Carray[action.childIndex].originalValue) -
          parseFloat(Carray[action.childIndex].values)
        ).toFixed(2);
        newValue.originalValue = parseFloat(newValue.values).toFixed(2);
        newValue.reference = action.childIndex;
        Carray[action.childIndex].originalValue = currentValue;

        let newD = [...state.updatedBankStmtList];
        let newD2 = [...newD[action.parentIndex], newValue];
        newD[action.parentIndex] = newD2;
        return { ...state, updatedBankStmtList: newD };
      }
      return state;
    default:
      return state;
  }
};

export default BankStatementReducer;
