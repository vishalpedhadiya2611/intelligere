import {combineReducers} from 'redux';
import bankStatement from './BankSatementReducer';
import CommonReducer from './CommonReducer';
import InvoiceCreateReducer from './InvoiceCreateReducer';

const rootReducer = combineReducers({
    bankStatement, CommonReducer, InvoiceCreateReducer
})

export default rootReducer;