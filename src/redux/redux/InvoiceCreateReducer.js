import {
  ADD_BUYER_DATA,
  ADD_OTHER_DATA,
  ADD_PRODUCT_DATA,
  ADD_SELLER_DATA,
} from "../constant/BankStatementTypes";

const initialState = {
  sellerData: {},
  buyerData: {},
  otherData: {},
  productData: [],
};

const InvoiceCreateReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_SELLER_DATA:
      return { ...state, sellerData: action.payload };
    case ADD_BUYER_DATA:
      return { ...state, buyerData: action.payload };
    case ADD_OTHER_DATA:
      return { ...state, otherData: action.payload };
    case ADD_PRODUCT_DATA:
      return { ...state, productData: [...state.productData, action.payload] };
    default:
      return state;
  }
};

export default InvoiceCreateReducer;
