import React from 'react'
import { COMPANYLIST, BANKLIST, LEDGERLIST } from '../constant/BankStatementTypes';

const CommonReducer = (state = {}, action) => {
    switch (action.type) {
        case COMPANYLIST:
            return {...state, companyList : action.payload}
        case BANKLIST: 
            return {...state, bankList : action.payload}
        case LEDGERLIST:
            return {...state, ledgerList : action.payload}
        default:
            return state;
    }
}

export default CommonReducer
