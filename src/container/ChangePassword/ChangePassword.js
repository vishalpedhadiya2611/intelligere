import React, { useState } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import swal from "sweetalert";
import {
  ContactIcon,
  EyeIcon,
  EyeSlashIcon,
  LockIcon,
  LogoIcon,
  UserIcon,
} from "../../components/Icons/Icons";

import logoDDS from "./../../assets/logoDDS.svg";
import axios from "axios";

const ChangePassword = () => {
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const history = useHistory();
    const [showPassword, setShaowpassword] = useState(false);

    function handlePassword(){
        if(!email){
            swal("Email is incorrect","","warning");
            return;
        }
        if(!password){
            swal("Enter a Valid Password","","warning");
            return;
        }
        const passwordData ={
            "password": password,   
            "email": email         
        }
        const api = axios.create({
            baseURL: `https://api.digitaldocsys.in/api`
        })
        api.post('/password-reset-complete', passwordData)
        .then(res => {
            console.log(res);
            history.push("/login");
          })
          .catch(error => {
            console.log(error);
          })
    }

  return (
    <div>
      <Container fluid>
        <Row className="justify-content-center align-items-center">
          <Col md={10} className="py-md-5 py-3 d-flex justify-content-between">
            <Link to="/" className="logodds">
              <img src={logoDDS} alt="logo" />
            </Link>
            <Link to="/">
              <ContactIcon />
            </Link>
          </Col>
          <Col lg={5}>
            <div className="text-center py-md-4 pb-3">
              <div className="logoicon mb-4">
                <LogoIcon />
              </div>
              <div className="font-size-md-16px font-size-14px text-uppercase opacity-60 text-primary pt-3">
                Welcome to
              </div>
              <div className="font-size-md-32px font-size-18px font-weight-700 text-primary ">
                smart.cheque
              </div>
            </div>
            <Card className="loginCard">
              <Card.Body>
                <div className="form-group">
                  <label className="font-size-14px pl-2 opacity-60">
                    Email
                  </label>
                  <div className="position-relative mb-3">
                    <input
                      type="email"
                      className="form-control w-100"
                      placeholder="Email"
                      autoComplete="off"
                      onChange={(e) => setEmail(e.target.value)}
                    />
                    <UserIcon />
                  </div>
                </div>
                <div className="form-group">
                  <label className="font-size-14px pl-2 opacity-60">
                    New Password
                  </label>
                  <div className="position-relative mb-3">
                    <input
                      type={showPassword ? "text" : "password"}
                      className="form-control"
                      placeholder="New Password"
                      autoComplete="off"
                      onChange={(e) => setPassword(e.target.value)}
                    />
                    <LockIcon />
                    <button
                        type="button"
                        className="showPassword btn-link btn p-0"
                        onClick={() => setShaowpassword(!showPassword)}
                      >
                        {!showPassword ? <EyeIcon /> : <EyeSlashIcon />}
                      </button>
                  </div>
                </div>
                <button
                  type="submit"
                  className="btn btn-primary w-100 mt-3"
                  onClick={handlePassword}
                >
                  Change Password
                </button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
export default ChangePassword;
