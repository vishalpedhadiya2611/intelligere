import React, { useEffect, useState } from "react";
import "./payment.css";
import axios from "axios";
import { useHistory } from "react-router-dom";

import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import swal from "sweetalert";
import {
  Card,
  Col,
  Container,
  Row,
  Button,
  Table,
  Form,
} from "react-bootstrap";
import { Title } from "../../../components/Texonomy";
import { FaPencilAlt, FaTrashAlt } from "react-icons/fa";

const Payment = () => {
  const history = useHistory();
  const date = new Date().toLocaleString();
  const [org, setOrg] = useState();
  const [payeeName, setPayeeName] = useState("");
  const [bank, setBank] = useState("");
  const [accnumber, setaccNumber] = useState("");
  const [chequedate, setChequeDate] = useState("");
  const [chequeAmount, setChequeAmount] = useState("");
  const [narration, setNarration] = useState("");
  const [orglist, setOrgList] = useState([]);
  const [paymentList, setPaymentList] = useState([]);
  const [bnklist, setbnkList] = useState([]);
  const [editing, setEditing] = useState(false);
  const [ids, setIds] = useState();
  const PaymentData = {
    orgn: org,
    bnk_id: bank,
    pymnt_nm: payeeName,
    pymnt_ac_no: accnumber,
    pymnt_chq_dt: chequedate,
    pymnt_chq_amt: chequeAmount,
    narration: narration,
  };
  const token = localStorage.getItem("user");
  const api = axios.create({
    baseURL: `https://api.digitaldocsys.in/api`,
  });
  useEffect(() => {
    const GetList = async () => {
      const result = await api.get("/orglist/", {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      });
      setOrgList(result.data);
      console.log(result.data);
      if (result.data.length > 0) {
        const list = await api.get("/pyamentlist/", {
          headers: {
            Authorization: ` Bearer ${token}`,
          },
        });
        console.log(list.data);
        setPaymentList(list.data);
        console.log(paymentList);

        const bnklist = await api.get("/bnklist/", {
          headers: {
            Authorization: ` Bearer ${token}`,
          },
        });
        console.log(bnklist.data);
        var lookup = {};
        var items = bnklist.data;
        var finalresult = [];
        for (var i = 0; i < items.length; i++) {
          var name = items[i].mstbnk_nm;
          if (!(name in lookup)) {
            lookup[name] = 1;
            finalresult.push(items[i]);
          }
        }
        setbnkList(finalresult);
        console.log(finalresult);
        setbnkList(finalresult);
      } else {
        swal("add organization detail", "", "warning");
        history.push("/organization");
      }
    };
    GetList();
    // eslint-disable-next-line
  }, []);

  const SavePayment = () => {
    setEditing(false);
    if (!org) {
      swal("Please select organization", "Incorrect Input", "warning");
      return;
    }
    if (!bank) {
      swal("Please select bank", "Incorrect Input", "warning");
      return;
    }
    if (!payeeName) {
      swal("Please Enter payee  Name", "Incorrect Input", "warning");
      return;
    }
    if (!bank) {
      swal("Please Enter bank Name", "Incorrect Input", "warning");
      return;
    }
    if (!accnumber) {
      swal("Please Enter account number", "Incorrect Input", "warning");
      return;
    }
    if (!chequedate) {
      swal("Please Enter cheque date", "Incorrect Input", "warning");
      return;
    }
    if (!chequeAmount) {
      swal("Please Enter cheque amount", "Incorrect Input", "warning");
      return;
    }
    if (
      payeeName === paymentList.pymnt_nm &&
      chequedate === paymentList.pymnt_chq_dt &&
      chequeAmount === paymentList.pymnt_chq_amt
    ) {
      swal("Please Enter cheque amount", "Incorrect Input", "warning");
      return;
    }
    listCheck();
    api
      .post("/payment/", PaymentData, {
        headers: {
          Authorization: ` Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
      .then((res) => {
        swal("you can add payment again", "", "success");
        window.location.reload();
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const listCheck = () => {
    for (const list of paymentList) {
      if (
        list.pymnt_nm === payeeName &&
        list.pymnt_chq_dt === chequedate &&
        list.pymnt_chq_amt === chequeAmount
      ) {
        swal("the record is already exist", "", "warning");
        return;
      }
    }
  };

  const deletePayment = (id) => {
    setEditing(false);
    api
      .delete(`/payment/${id}`, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res);
        window.location.reload();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const editPayment = (id) => {
    setIds(id);
    setEditing(true);
    api
      .get(`/payment/${id}`, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res.data);
        setOrg(res.data.orgn);
        setPayeeName(res.data.pymnt_nm);
        setBank(res.data.bnk_id);
        setaccNumber(res.data.pymnt_ac_no);
        setChequeDate(res.data.pymnt_chq_dt);
        setChequeAmount(res.data.pymnt_chq_amt);
        setNarration(res.data.narration);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const Update = () => {
    if (!org) {
      swal("Please select organization", "Incorrect Input", "warning");
      return;
    }
    if (!bank) {
      swal("Please select bank", "Incorrect Input", "warning");
      return;
    }
    if (!payeeName) {
      swal("Please Enter payee  Name", "Incorrect Input", "warning");
      return;
    }
    if (!bank) {
      swal("Please Enter bank Name", "Incorrect Input", "warning");
      return;
    }
    if (!accnumber) {
      swal("Please Enter account number", "Incorrect Input", "warning");
      return;
    }
    if (!chequedate) {
      swal("Please Enter cheque date", "Incorrect Input", "warning");
      return;
    }
    if (!chequeAmount) {
      swal("Please Enter cheque amount", "Incorrect Input", "warning");
      return;
    }
    if (
      payeeName === paymentList.pymnt_nm &&
      chequedate === paymentList.pymnt_chq_dt &&
      chequeAmount === paymentList.pymnt_chq_amt
    ) {
      swal("Please Enter cheque amount", "Incorrect Input", "warning");
      return;
    }
    const id = ids;
    api
      .put(`/payment/${id}/`, PaymentData, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
      .then((res) => {
        swal("Edit Done!", "You record is updated", "success").then(
          window.location.reload()
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // eslint-disable-next-line
  const submit = () => {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div>
            <h1>Are you sure?</h1>
            <p style={{ color: "white" }}>You want to add payment</p>
            <Button
              style={{
                fontSize: "15px",
                background: "#083546",
                color: "white",
                borderRadius: "20px",
                borderColor: "#083546",
                width: "25%",
              }}
              onClick={onClose}
            >
              No
            </Button>
            <Button
              style={{
                fontSize: "15px",
                background: "#083546",
                color: "white",
                borderRadius: "20px",
                borderColor: "#083546",
                width: "25%",
              }}
              onClick={() => {
                onClose();
              }}
            >
              Yes, Add!
            </Button>
          </div>
        );
      },
    });
  };

  return (
    <div>
      <Container fluid className="rowCol">
        <Row>
          <Col md={4}>
            <Title title="Payment Details" />
            <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
              <Form>
                <Col>
                  <div className="form-group mb-3">
                    <label>Select Organization</label>
                    <select
                      value={org}
                      onChange={(e) => setOrg(e.target.value)}
                      className="form-control"
                    >
                      <option>Choose an organization</option>
                      {orglist.map((item) => (
                        <option
                          key={item.orgn_id}
                          value={item.orgn_id}
                          label={item.orgn_nm}
                        ></option>
                      ))}
                    </select>
                  </div>
                </Col>
                <Col>
                  <div className="form-group mb-3">
                    <label>Select Organization</label>
                    <select
                      value={bank}
                      onChange={(e) => setBank(e.target.value)}
                      className="form-control"
                    >
                      <option>Choose a bank</option>
                      {bnklist.map((item) => (
                        <option
                          key={item.bnk_id}
                          value={item.bnk_id}
                          label={item.mstbnk_nm}
                        ></option>
                      ))}
                    </select>
                  </div>
                </Col>
                <Col>
                  <div className="form-group mb-3">
                    <label>Payee Name</label>
                    <input
                      value={payeeName}
                      type="text"
                      onChange={(e) => setPayeeName(e.target.value)}
                      className="form-control"
                    />
                  </div>
                </Col>
                <Col>
                  <div className="form-group mb-3">
                    <label>Account No</label>
                    <input
                      value={accnumber}
                      type="text"
                      onChange={(e) => setaccNumber(e.target.value)}
                      className="form-control"
                    />
                  </div>
                </Col>
                <Col>
                  <div className="form-group mb-3">
                    <label>Cheque Date</label>
                    <input
                      value={chequedate}
                      type="date"
                      onChange={(e) => setChequeDate(e.target.value)}
                      className="form-control"
                    />
                  </div>
                </Col>
                <Col>
                  <div className="form-group mb-3">
                    <label>Cheque Amount</label>
                    <input
                      value={chequeAmount}
                      type="text"
                      onChange={(e) => setChequeAmount(e.target.value)}
                      className="form-control"
                    />
                  </div>
                </Col>
                <Col>
                  <div className="form-group mb-3">
                    <label>Narration</label>
                    <input
                      value={narration}
                      type="text"
                      onChange={(e) => setNarration(e.target.value)}
                      className="form-control"
                    />
                  </div>
                </Col>
              </Form>
              <div className="text-right">
                {editing ? (
                  <Button variant="primary" onClick={Update}>
                    Update
                  </Button>
                ) : (
                  <Button variant="primary" onClick={SavePayment}>
                    Save
                  </Button>
                )}
              </div>
            </div>
          </Col>
          <Col md={8}>
            <Title title="Payment List" />
            <Col>
              <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
                <Table responsive hover className="mt-3">
                  <thead>
                    <tr>
                      <th>Payee Name</th>
                      <th>Bank Name</th>
                      <th>Cheque Date</th>
                      <th>Cheque amt</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    {paymentList.map((list) => (
                      <tr key={list.pymnt_id}>
                        <td>{list.pymnt_nm}</td>
                        <td>{list.bnk_nm}</td>
                        <td>{list.pymnt_chq_dt}</td>
                        <td>{list.pymnt_chq_amt}</td>
                        <td>
                          <FaPencilAlt
                            onClick={() => editPayment(list.pymnt_id)}
                          />
                        </td>
                        <td>
                          <FaTrashAlt
                            onClick={() => deletePayment(list.pymnt_id)}
                          />
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
                <div className="text-right">
                  <Button href="./print" variant="primary">
                    Go to Print
                  </Button>
                </div>
              </div>
            </Col>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Payment;
