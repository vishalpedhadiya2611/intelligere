import React, { useState, useRef } from 'react';
import './dashboard.css';
import {  Col, Container, Row,Table } from 'react-bootstrap';

const Dashboard = () => {
    const [checked, setChecked] = useState(false);
    let btnRef = useRef();

    const onBtnClick = e => {
        if (btnRef.current) {
            btnRef.current.setAttribute("disabled", "disabled");
            setChecked(true);
        } else {
            setChecked(false);
        }
    }
    return (
        <div  >
            <Container style={{textAlign: "center" }} >
		    <Row >
			<Col xs={{offset: 0, span:12}}  lg={{offset: 0, span:12}}  md={{offset: 0, span:12}}>
            <div className="table-list">
                <h1 className="pending">Pending Task</h1>
                <Table>
                    <thead>
                        <tr>
                            <th>Payee Name</th>
                            <th>Bank Name</th>
                            <th>Cheque Date</th>
                            <th>Cheque Amt</th>
                            <th>Cheque No</th>
                            <th>Verify</th>
                            <th>Mail</th>
                            <th>CSV</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>janak kumar</td>
                            <td>hdfc</td>
                            <td>2/09/2012</td>
                            <td>5000</td>
                            <td>12345</td>
                            <td><input type="checkbox" name="checked" ref={btnRef} onChange={onBtnClick} /></td>
                            <td><input type="checkbox" name="mail" disabled={!checked} /></td>
                            <td><input type="checkbox" name="cms" disabled={!checked} /></td>
                        </tr>
                        <tr>
                            <td>janak kumar</td>
                            <td>hdfc</td>
                            <td>2/09/2012</td>
                            <td>5000</td>
                            <td>12345</td>
                            <td><input type="checkbox" name="checked" ref={btnRef} onChange={onBtnClick} /></td>
                            <td><input type="checkbox" name="mail" disabled={!checked} /></td>
                            <td><input type="checkbox" name="cms" disabled={!checked} /></td>
                        </tr>
                    </tbody>
                </Table>
            
                </div>
           
		</Col>
		</Row>	
	</Container>
    </div>
    )
}

export default Dashboard
