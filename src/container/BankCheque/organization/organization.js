/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, useEffect, Fragment } from "react";
import axios from "axios";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import {
  Card,
  Table,
  Col,
  Container,
  Row,
  Button,
  Form,
} from "react-bootstrap";
import { Title } from "../../../components/Texonomy";
import { FaPencilAlt, FaTrashAlt } from "react-icons/fa";

const organization = () => {
  const history = useHistory("");
  const [orgName, setOrgName] = useState("");
  const [orgEmail, setOrgEmail] = useState("");
  const [orgMobile, setOrgMobile] = useState("");
  const [ids, setIds] = useState("");
  // eslint-disable-next-line
  const [error, setError] = useState(false);
  const [orgList, setOrgList] = useState([]);
  // const [bankList, setBankList] = useState([]);
  const [editing, setEditing] = useState(false);
  const orgdata = {
    orgn_nm: orgName,
    orgn_email: orgEmail,
    orgn_mobile: orgMobile,
  };
  const SaveOrg = () => {
    setEditing(false);
    if (!orgName) {
      swal("Please Enter Organization Name", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (!orgEmail) {
      swal("Please Enter Organization Email", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (!orgMobile) {
      swal("Please Enter Mobile Number", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    const token = localStorage.getItem("user");
    console.log(token);
    const api = axios.create({
      baseURL: `https://api.digitaldocsys.in/api/`,
    });
    api
      .post("org/", orgdata, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((res) => {
        clear();
        history.push("/bank");
        if (token) {
          console.log(res.data);
        }
      })
      .catch((error) => {
        swal("Unauthorized Access", "", "error");
        console.log(error);
        console.log(orgdata);
      });
  };
  const deletUser = (id) => {
    setEditing(false);
    const token = localStorage.getItem("user");
    console.log(token);
    const api = axios.create({
      baseURL: `https://api.digitaldocsys.in/api`,
    });

    api
      .delete(`/org/${id}`, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result);
        history.push("/organization");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const updateUser = (id) => {
    setIds(id);
    setEditing(true);
    const token = localStorage.getItem("user");
    const api = axios.create({
      baseURL: `https://api.digitaldocsys.in/api`,
    });
    api
      .get(`/org/${id}`, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res.data);
        setOrgName(res.data.orgn_nm);
        setOrgEmail(res.data.orgn_email);
        setOrgMobile(res.data.orgn_mobile);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const editUser = () => {
    const id = ids;
    const token = localStorage.getItem("user");
    const api = axios.create({
      baseURL: `https://api.digitaldocsys.in/api`,
    });
    api
      .put(`/org/${id}/`, orgdata, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((res) => {
        clear();
        swal("Good job!", "You record is updated", "success");
        console.log(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  useEffect(() => {
    const GetData = async () => {
      const token = localStorage.getItem("user");
      console.log(token);
      const api = axios.create({
        baseURL: `https://api.digitaldocsys.in/api`,
      });
      const result = await api.get("/org/", {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      });
      console.log(result.data);
      setOrgList(result.data);
    };
    GetData();
  }, []);
  const clear = () => {
    setOrgName("");
    setOrgEmail("");
    setOrgMobile("");
  };
  return (
    <>
      <Container fluid className="rowCol">
        <Row>
          <Col md={4}>
            <Title title="Register Organization" />
            <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
              <Form>
                <Col>
                  <div className="form-group mb-3">
                    <label>Organization Name</label>
                    <input
                      type="text"
                      className="input"
                      value={orgName}
                      onChange={(e) => setOrgName(e.target.value)}
                      className="form-control"
                    />
                  </div>
                </Col>
                <Col>
                  <div className="form-group mb-3">
                    <label>Email Id</label>
                    <input
                      type="text"
                      className="input"
                      value={orgEmail}
                      onChange={(e) => setOrgEmail(e.target.value)}
                      className="form-control"
                    />
                  </div>
                </Col>
                <Col>
                  <div className="form-group mb-3">
                    <label>Mobile Number</label>
                    <input
                      type="text"
                      className="input"
                      value={orgMobile}
                      onChange={(e) => setOrgMobile(e.target.value)}
                      className="form-control"
                    />
                  </div>
                </Col>
                <Col>
                  <div className="form-group mb-3 text-right">
                    {editing ? (
                      <Fragment>
                        <Button variant="primary" onClick={editUser}>
                          update
                        </Button>
                      </Fragment>
                    ) : (
                      <Fragment>
                        <Button variant="primary" onClick={SaveOrg}>
                          Save
                        </Button>
                        <Button
                          variant="primary"
                          onClick={clear}
                          className="ml-2"
                        >
                          Cancel
                        </Button>
                      </Fragment>
                    )}
                  </div>
                </Col>
              </Form>
            </div>
          </Col>
          <Col md={8}>
            <Title title="Organization List" />
            <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
              <Table responsive hover>
                <thead>
                  <tr>
                    <th>Organization Name</th>
                    <th>Email Id</th>
                    <th>Mobile Number</th>
                    <th>Tally Connect</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {orgList.length > 0 ? (
                    orgList.map((user) => (
                      <tr key={user.orgn_id}>
                        <td>{user.orgn_nm}</td>
                        <td>{user.orgn_email}</td>
                        <td>{user.orgn_mobile}</td>
                        <td>
                          <input name="tally" type="checkbox" />
                        </td>
                        <td>
                          <FaPencilAlt
                            onClick={() => {
                              updateUser(user.orgn_id);
                            }}
                          />
                        </td>
                        <td>
                          <FaTrashAlt
                            onClick={() => {
                              deletUser(user.orgn_id);
                            }}
                          />
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr>
                      <td colSpan={5}>No users</td>
                    </tr>
                  )}
                </tbody>
              </Table>
              <div className="text-right">
                <Button variant="primary" href="./bank">
                  Go To Bank
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default organization;
