import React, { useState, useRef } from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import swal from "sweetalert";
import Header from "../header";
import { useHistory } from "react-router-dom";
import { Card, Col, Container, Row, Button } from "react-bootstrap";

const Signup = () => {
  const [checked, setChecked] = useState(false);
  const onBtnClick = (e) => {
    if (checked) {
      setChecked(false);
    } else {
      setChecked(true);
    }
  };
  const history = useHistory();
  // eslint-disable-next-line
  const { handleSubmit } = useForm();
  let userRef = useRef("");
  let emailRef = useRef("");
  let mobileRef = useRef("");
  let passRef = useRef("");
  let cpassRef = useRef("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [mobile, setMobile] = useState("");
  const [user, setUser] = useState("");
  const [confirmPwd, setConfirmPwd] = useState("");
  // eslint-disable-next-line
  const [error, setError] = useState(false);
  const handleSignUp = () => {
    if (!user) {
      swal("Please Enter username", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (!email) {
      swal("Please Enter Email", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (!mobile) {
      swal("Please Enter Mobile Number", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (!password) {
      swal("Please Enter Password", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (password.length < 6) {
      swal(
        "Password too short",
        "Please enter more than 6 characters",
        "warning"
      );
      setError(true);
      return;
    }
    if (password !== confirmPwd) {
      swal("Password is not matching", "", "warning");
      setError(true);
      return;
    }
    if (!checked) {
      swal(
        "Terms & Conditions isn't accepted",
        "Please check the checkbox",
        "warning"
      );
      setError(true);
      return;
    }
    const signupData = {
      name: user,
      email: email,
      mobile: mobile,
      password: password,
    };
    const api = axios.create({
      baseURL: `https://api.digitaldocsys.in/api`,
    });

    // eslint-disable-next-line
    const emailverify = () => {
      api.get("/email-verify/").then((res) => localStorage.setItem("user"));
    };
    api
      .post("/register/", signupData)
      .then((res) => {
        history.push("/");
        console.log(res.data);
      })
      .catch((error) => {
        console.log(error);
        swal("Email Already registered", "", "error");
      });
  };
  const clear = function () {
    userRef.current.value = "";
    emailRef.current.value = "";
    mobileRef.current.value = "";
    passRef.current.value = "";
    cpassRef.current.value = "";
  };

  return (
    <div>
      <Header />
      <Container style={{ textAlign: "center" }}>
        <Row style={{ display: "flex", justifyContent: "center" }}>
          <Col
            xs={{ offset: 0, span: 12 }}
            lg={{ offset: 0, span: 6 }}
            md={{ offset: 0, span: 10 }}
          >
            <Card
              style={{
                padding: "7% 3%",
                borderRadius: "10px",
                backgroundColor: "#0f4d6a",
                color: "white",
              }}
            >
              <h1>User Registration</h1>
              <br />
              <div>
                <label>Username</label>
                <br />
                <input
                  style={{ width: "70%" }}
                  type="text"
                  name="user"
                  onChange={(e) => setUser(e.target.value)}
                  ref={userRef}
                />
              </div>
              <br />
              <div>
                <label>Email</label>
                <br />
                <input
                  style={{ width: "70%" }}
                  type="email"
                  name="email"
                  onChange={(e) => setEmail(e.target.value)}
                  ref={emailRef}
                />
              </div>
              <br />
              <div>
                <label>Mobile Number</label>
                <br />
                <input
                  style={{ width: "70%" }}
                  type="text"
                  name="mobile"
                  onChange={(e) => setMobile(e.target.value)}
                  ref={mobileRef}
                />
              </div>
              <br />
              <div>
                <label>Enter Password</label>
                <br />
                <input
                  style={{ width: "70%" }}
                  type="password"
                  name="password"
                  onChange={(e) => setPassword(e.target.value)}
                  ref={passRef}
                />
              </div>
              <br />
              <div>
                <label>Confirm Password</label>
                <br />
                <input
                  style={{ width: "70%" }}
                  type="password"
                  name="confpassword"
                  onChange={(e) => setConfirmPwd(e.target.value)}
                  ref={cpassRef}
                />
              </div>
              <br />
              <br />
              <Row style={{ width: "100%", margin: "0%" }}>
                <Col
                  style={{ textAlign: "center", width: "100%", padding: "0%" }}
                  xs={12}
                >
                  <label>
                    I agree to the{" "}
                    <a href="/termconditions">terms and conditions</a>
                  </label>
                  &nbsp;&nbsp;&nbsp;
                  <input
                    style={{ width: "5%", height: "50%" }}
                    type="checkbox"
                    onChange={onBtnClick}
                  />
                </Col>
              </Row>
              <br />
              <br />
              <div>
                <Button
                  style={{
                    fontSize: "15px",
                    background: "#083546",
                    color: "white",
                    borderRadius: "20px",
                    borderColor: "#083546",
                    width: "25%",
                  }}
                  className="btnsave"
                  type="submit"
                  onClick={handleSignUp}
                >
                  Save
                </Button>
                &nbsp; &nbsp; &nbsp;
                <Button
                  style={{
                    fontSize: "15px",
                    background: "#083546",
                    color: "white",
                    borderRadius: "20px",
                    borderColor: "#083546",
                    width: "25%",
                  }}
                  className="btnsave"
                  type="submit"
                  onClick={clear}
                >
                  Clear
                </Button>
              </div>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Signup;
