import React, { useState, useRef, useEffect } from "react";
import axios from "axios";
import { Card, Col, Container, Row, Button, Table } from "react-bootstrap";

import { Title } from "../../../components/Texonomy";
import { UploadCheque } from "../../../components/Modal/UploadCheque";
const PaymentVerification = () => {
  const [checked1, setChecked1] = useState(false);
  const [paymentList, setPaymentList] = useState([]);

  const token = localStorage.getItem("user");
  const [ids, setIds] = useState();

  const [cheqnodata, setCheqnoData] = useState(null);
  const [showVoucherEntry, setShowVoucherEntry] = useState(false);

  const api = axios.create({
    baseURL: `https://api.digitaldocsys.in`,
  });
  let btnRef1 = useRef();
  const onBtnClick1 = (e) => {
    if (btnRef1.current) {
      btnRef1.current.setAttribute("disabled", "disabled");
      setChecked1(true);
    }
  };

  

  const StatusChange = (id) => {
    api
      .put(
        `api/checkverify/${id}/`,
        { paymentactive_status: "1", PYMENT_CHQ_NO: cheqnodata },
        {
          headers: {
            Authorization: ` Bearer ${token}`,
          },
        }
      )
      .then((res) => {
        console.log(res);
        window.location.reload();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    const GetData = async () => {
      const list = await api.get("api/getallpymentdashbord/", {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      });
      setPaymentList(list.data);
      console.log(list.data);
    };
    GetData();
    // eslint-disable-next-line
  }, []);

  return (
    <div>
      <div className="rowCol customContainer">
        <Col>
          <Title title="Payment Verification" />
          <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
            <Table responsive hover className="mt-3">
              <thead>
                <tr>
                  <th>Payee Name</th>
                  <th>Bank name</th>
                  <th>Cheque Date</th>
                  <th>Cheque Amt</th>
                  <th>Cheque No</th>
                  <th>Upload Cheque Image</th>
                  <th>Verify</th>
                </tr>
              </thead>
              <tbody>
                {paymentList[0] ? (
                  <tr>
                    <td>{paymentList[0].pymnt_nm}</td>
                    <td>{paymentList[0].bnk_nm}</td>
                    <td>{paymentList[0].pymnt_chq_dt}</td>
                    <td>{paymentList[0].pymnt_chq_amt}</td>
                    {cheqnodata ? (
                      <td>
                        <input
                          name="chno"
                          value={cheqnodata}
                          onChange={(e) => setCheqnoData(e.target.value)}
                        />
                      </td>
                    ) : (
                      <td>{paymentList[0].PYMENT_CHQ_NO}</td>
                    )}
                    <td>
                      <input
                        type="checkbox"
                        name="mail"
                        ref={btnRef1}
                        onChange={onBtnClick1}
                        onClick={() => {
                          setIds(paymentList[0].pymnt_id);
                          setShowVoucherEntry(!showVoucherEntry)
                        }}
                      />
                    </td>
                    <td>
                      <input
                        type="checkbox"
                        name="checked"
                        disabled={!checked1}
                        onClick={() => {
                          StatusChange(paymentList[0].pymnt_id);
                        }}
                      />
                    </td>
                  </tr>
                ) : (
                  <tr></tr>
                )}
              </tbody>
            </Table>
          </div>
        </Col>
      </div>
      <UploadCheque show={showVoucherEntry} handleClose={() => setShowVoucherEntry(false)} ids={ids} api={api} token={token} setCheqnoData={setCheqnoData}/>
    </div>
  );
};

export default PaymentVerification;
