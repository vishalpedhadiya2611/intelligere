import React, { useRef, useState } from "react";
import ReactToPrint from "react-to-print";
import { Col, Container, Row, Button } from "react-bootstrap";
import Header from "./header";
import axios from 'axios';

const PrintExample = () => {
  const componentRef = useRef();
  const pageStyle = `
    @page {
      size:203.2mm 93.13mm landscape !important;
      margin:0;
      -webkit-print-color-adjust: exact;
    }

    @media print {
      html, body {
        height: initial !important;
        overflow: initial !important;
      }
    }
  `;
  const [blnkimg, setBlnkImg] = useState([]);
  const api = axios.create({
      baseURL: `https://api.digitaldocsys.in/api`
  })
  const GetData = async () => {
      const list = await api.get("/getblankimg/", {})
      setBlnkImg('https://api.digitaldocsys.in' + list.data.image_url);
  }
  return (
    <div>
      <Header />
      <Container style={{ textAlign: "center" }}>
        <Row style={{ display: "flex", justifyContent: "center" }}>
          <Col
            xs={12}
            lg={6}
            md={10}
          >
          <Button
            onClick={() => { GetData() }}
            style={{
              marginLeft: "50px",
              fontSize: "15px",
              background: "#083546",
              color: "white",
              borderRadius: "20px",
              borderColor: "#083546",
              width: "auto",
            }}
          >
            Show Image
          </Button>
            <ReactToPrint
              trigger={() => (
                <Button
                  style={{
                    marginLeft: "50px",
                    fontSize: "15px",
                    background: "#083546",
                    color: "white",
                    borderRadius: "20px",
                    borderColor: "#083546",
                    width: "auto",
                  }}
                >
                  Print this out!
                </Button>
              )}
              content={() => componentRef.current}
              pageStyle={pageStyle}
            />
            <div ref={componentRef}>
              <div style={{ width: "8.34cm", height: "4.31cm" }}>
                <img src={blnkimg} alt="print" />
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default PrintExample;
