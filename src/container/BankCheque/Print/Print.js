import axios from "axios";
import React, { useRef, useEffect, useState } from "react";
import Arrow from "../Arrow/Arrow";
import { Card, Table, Col, Container, Row, Button } from "react-bootstrap";
import ReactToPrint from "react-to-print";
import { Title } from "../../../components/Texonomy";

const Print = () => {
  const componentRef = useRef();
  const pageStyle = `
        @page {
        size:202.00mm 93.13mm landscape !important;
        margin:0;
        -webkit-print-color-adjust: exact;
        }

        @media print {
        html, body {
            height: initial !important;
            overflow: initial !important;
        }
        }
    `;
  const [paymentList, setPaymentList] = useState([]);
  const [blnkimg, setBlnkImg] = useState([]);
  const token = localStorage.getItem("user");
  const api = axios.create({
    baseURL: `https://api.digitaldocsys.in/api`,
  });
  useEffect(() => {
    const GetData = async () => {
      const list = await api.get("/getallpaymentlist/", {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      });
      setPaymentList(list.data);
      console.log(list.data);
    };
    GetData();
    // eslint-disable-next-line
  }, []);

  const PrintPay = (id) => {
    api
      .get(`/payment/${id}`, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res.data);
      })
      .catch((error) => {
        console.log(error);
      });

    api
      .get(`/getblankimg/`, {})
      .then((res) => {
        console.log(res.data);
        setBlnkImg("https://api.digitaldocsys.in" + res.data.image_url);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <Container fluid className="rowCol">
        <Row>
          <Col md={4}>
            <Title title="Print Cheque" />
            <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
              <Table responsive hover className="mt-3">
                <thead>
                  <tr>
                    <th>Payee Name</th>
                    <th>Bank Name</th>
                    <th>Cheque Date</th>
                    <th>Check Amount</th>
                    <th>Select for Print</th>
                  </tr>
                </thead>
                <tbody>
                  {paymentList.map((item) => (
                    <tr key={item.pymnt_id}>
                      <td>{item.pymnt_nm}</td>
                      <td>{item.bnk_nm}</td>
                      <td>{item.pymnt_chq_dt}</td>
                      <td>{item.pymnt_chq_amt}</td>
                      <td>
                        <input
                          type="checkbox"
                          name="print"
                          onClick={() => {
                            PrintPay(item.pymnt_id);
                          }}
                          className="form-control"
                        />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </Col>
          <Col md={8}>
            <Title title="Step One" />
            <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
              <h4>Place the Cheque in this way</h4>
              <Arrow />
              <Container style={{ textAlign: "center" }} className="mt-3">
                <Col className="text-right">
                  <ReactToPrint
                    trigger={() => (
                      <Button variant="primary">Print this out!</Button>
                    )}
                    content={() => componentRef.current}
                    pageStyle={pageStyle}
                  />
                  <div style={{ display: "none" }}>
                    <div ref={componentRef}>
                      <div style={{ width: "18.34cm", height: "14.31cm" }}>
                        <img
                          src={blnkimg}
                          onerror="this.style.display='none'"
                        />
                      </div>
                    </div>
                  </div>
                </Col>
              </Container>
            </div>
          </Col>
        </Row>
        <Row className="text-center">
          <Col>
            <Button variant="primary" type="submit" href="./verification">
              Verify Payment
            </Button>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Print;
