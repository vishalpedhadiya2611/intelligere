/* eslint-disable react-hooks/rules-of-hooks */
import React, {useState} from 'react';
import axios from 'axios';
import swal from 'sweetalert';
import { useHistory } from "react-router-dom";
import {  Card, Col, Container, Row, Button } from 'react-bootstrap';
import Header from './header';
const changePassword = () => {
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const history = useHistory();

    function handlePassword(){
        if(!email){
            swal("Email is incorrect","","warning");
            return;
        }
        if(!password){
            swal("Enter a Valid Password","","warning");
            return;
        }
        const passwordData ={
            "password": password,   
            "email": email         
        }
        const api = axios.create({
            baseURL: `https://api.digitaldocsys.in/api`
        })
        api.post('/password-reset-complete', passwordData)
        .then(res => {
            console.log(res);
            history.push("/login");
          })
          .catch(error => {
            console.log(error);
          })
    }
    return (
        <div> 
        <Header />
        <Container style={{textAlign: "center" }}>
        <Row style={{ display: "flex", justifyContent:"center"}}>
        <Col xs={{offset: 0, span:12}}  lg={{offset: 0, span:5}}  md={{offset: 0, span:10}}>
        <Card style={{padding: "7% 3%",
            borderRadius: "10px",
            backgroundColor:"#0f4d6a",
            color:"white"}}>
        <div>
			<label >Email</label><br/>
			<input   type="text" className="input" value={email} onChange={e => setEmail(e.target.value)} />
			</div>
            <br/>
			<div>
			<label >New Password</label><br/>
			<input   type="text" className="input" value={password} onChange={e => setPassword(e.target.value)} />
			</div>
        
        <br/><br/><br/>
        <div>
        <Button style ={{fontSize:"15px",background:"#083546" , color:"white", borderRadius:"20px" ,  borderColor: "#083546", width:"auto" }} type="submit" onClick={handlePassword}>Change Password</Button><br/>
        </div>
    </Card>
    </Col>
    </Row>	
</Container>
</div>
    )
}

export default changePassword