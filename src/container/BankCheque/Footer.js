import React from 'react';
import DDS  from '../images/DDS1-2.png';
import {  Col, Container, Row} from 'react-bootstrap';
const Footer = () => {
    return (
        <Container style={{textAlign: "center" ,marginTop:"2%"}}>
			<Row style={{ display: "flex", justifyContent:"center"}}>
			<Col xs={{offset: 0, span:12}}  lg={{offset: 0, span:6}}  md={{offset: 0, span:10}}>
        <div style={{textAlign: "center",color: "white"}}>  
            <img className="img" src={DDS} alt="logo" />

        </div>
        </Col>
        
        </Row>
       </Container>
    )
}

export default Footer;