import React from "react";
import { Card, Col, Container, Row, Button } from "react-bootstrap";
import { Title } from "../../../components/Texonomy";
import Header from "../header";
const ExternalSoftware = () => {
  return (
    <div>
      <Container>
        <Title title="Welcome to External Software Details" />
        <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
        <Row>
          <Col>
              <div className="form-group mb-3">
                <label>Select Platform</label>
                <select className="form-control">
                  <option></option>
                  <option>tally</option>
                </select>
              </div>
              <div className="form-group mb-3">
                <label>Organization Name</label>
                <input type="text" class="form-control" />
              </div>
              <div className="form-group text-right">
                <Button variant="primary" className="m-1">Save</Button>
                <Button variant="primary" className="m-1">Cancel</Button>
              </div>
          </Col>
        </Row>
        </div>
      </Container>
    </div>
  );
};

export default ExternalSoftware;
