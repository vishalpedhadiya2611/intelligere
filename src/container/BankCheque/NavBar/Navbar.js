import React from 'react';
import './Navbar.css';
import { Navbar } from 'react-bootstrap';
const Navbar2 = () => {

    const logout = () => {
      localStorage.removeItem('user');
    }

    return(
      <div>
        <Navbar expand="lg"  className="topnav " id="topnav " style={{padding: "0% 0% 0% 0%"}} >
       
            <Navbar.Collapse >
                {/* <Nav  > */}
                  <a href="/dashboard">Dashboard</a>
                  <a href="/payment">Payment</a>
                  <a href="/organization">Organization</a>
                  <a href="/bank">Bank</a>
                {/* </Nav>                 */}
            </Navbar.Collapse>
            <Navbar.Toggle />
            {/* <Navbar.Brand > */}
              {/* <Nav className= "justify-content-end"   > */}
            <a href="/login" className="justify-content-end logout" onClick={logout}>Logout</a>  
            {/* </Nav>   */}
           
            {/* </Navbar.Brand> */}
        </Navbar>
       

        <br/><br/><br/><br/>

        </div>
       
    )
}

export default Navbar2;