import React from 'react';
import './header.css';
import {  Row } from 'react-bootstrap';
const Header = () => {
    return (
        <Row style={{display: "flex", justifyContent:"center",textAlign: "center"}}>
            <div>
                <br/><br/>
                <h2 className="welcome">Welcome to</h2>
                <h1 className="title"><span className="smart">smart.</span><span className="cheque">cheque</span></h1>
                <br/><br/>
            </div>
        </Row>
       
    )
}

export default Header
