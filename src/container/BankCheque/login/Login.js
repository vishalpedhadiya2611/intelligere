/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from "react";
import axios from "axios";
import swal from "sweetalert";
import { Link, useHistory } from "react-router-dom";
import "typeface-josefin-sans";
import Header from "../header";
import "../all.css";
import { Card, Col, Container, Row, Button } from "react-bootstrap";
import { EyeIcon, EyeSlashIcon, LockIcon, LogoIcon, UserIcon } from "../../../components/Icons/Icons";

const login = () => {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // eslint-disable-next-line
  const [error, setError] = useState(false);
  const [showPassword, setShaowpassword] = useState(false);

  const handleLogin = () => {
    if (!email) {
      swal("Please Enter Email", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (!password || password.length < 6) {
      swal("Please Enter Password", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    const loginData = {
      email: email,
      password: password,
    };

    const api = axios.create({
      baseURL: `https://api.digitaldocsys.in/api`,
    });
    api
      .post("/login/", loginData)
      .then((res) => {
        console.log(res.data.tokens);
        const token = res.data.tokens.access;
        localStorage.setItem("user", token);
        history.push("/organization");
      })
      .catch((error) => {
        swal("Unauthorized Access", "", "error");
        setError(true);
      });
  };
  const register = () => {
    history.push("/Signup");
  };

  return (
    <div className="TM_LoginPage">
      <Container fluid>
        <Row className="justify-content-center align-items-center">
          <Col md={10}>
            <Card
              style={{
                padding: "7% 3%",
                borderRadius: "10px",
                backgroundColor: "#0f4d6a",
                color: "white",
              }}
            >
              <div>
                <label>Email</label>
                <br />
                <input
                  type="text"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <br />
              <div>
                <label>Password</label>
                <br />
                <input
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
              <br />
              <div>
                <Button
                  style={{
                    fontSize: "15px",
                    background: "#083546",
                    color: "white",
                    borderRadius: "20px",
                    borderColor: "#083546",
                    width: "25%",
                  }}
                  type="submit"
                  onClick={handleLogin}
                >
                  Login
                </Button>
                <br />
                <br />
                <a
                  style={{ fontSize: "15px", color: "white" }}
                  href="/forgetPassword"
                >
                  Forget password
                </a>
              </div>
              <br />
              <div>
                <h3>New to smart.cheque?</h3>
                <Button
                  style={{
                    fontSize: "15px",
                    background: "#083546",
                    color: "white",
                    borderRadius: "20px",
                    borderColor: "#083546",
                    width: "45%",
                  }}
                  type="submit"
                  onClick={register}
                >
                  Register Here
                </Button>
              </div>
            </Card>
          </Col>
          <Col lg={5}>
            <div className="text-center py-md-4 pb-3">
              <div className="logoicon mb-4">
                <LogoIcon />
              </div>
              <div className="font-size-md-16px font-size-14px text-uppercase opacity-60 text-primary pt-3">
                Welcome to
              </div>
              <div className="font-size-md-32px font-size-18px font-weight-700 text-primary ">
                smart.cheque
              </div>
            </div>
            <Card className="loginCard">
              <Card.Body>
                  <div className="form-group">
                    <label className="font-size-14px pl-2 opacity-60">
                      Username
                    </label>
                    <div className="position-relative mb-3">
                      <input
                        type="email"
                        className="form-control"
                        placeholder="Enter Username"
                        autoComplete="off"
						onChange={e => setEmail(e.target.value)}
                      />
                      <UserIcon />
                    </div>
                  </div>
                  <div className="form-group">
                    <label className="font-size-14px pl-2 opacity-60">
                      Password
                    </label>
                    <div className="position-relative mb-3">
                      <input
                        type={showPassword ? "text" : "password"}
                        className="form-control"
                        placeholder="Enter Password"
                        autoComplete="off"
						onChange={e => setPassword(e.target.value)}
                      />
                      <LockIcon />
                      <button
                        type="button"
                        className="showPassword btn-link btn p-0"
                        onClick={() => setShaowpassword(!showPassword)}
                      >
                        {!showPassword ? <EyeIcon /> : <EyeSlashIcon />}
                      </button>
                    </div>
                  </div>
                  <button type="submit" className="btn btn-primary w-100 mt-3" onClick={handleLogin}>
                    Login
                  </button>
                  <div className="text-center pt-3">
                    <Link to="/" className="text-primary btn btn-link p-0">
                      Forgot Password
                    </Link>
                  </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default login;
