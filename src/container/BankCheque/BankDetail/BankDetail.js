import React, { useState, useEffect } from "react";
import axios from "axios";
import "./BankDetail.css";
import { useHistory } from "react-router-dom";
import ReactTooltip from "react-tooltip";
import swal from "sweetalert";
import { Card, Col, Container, Row, Button, Table } from "react-bootstrap";
import { Title } from "../../../components/Texonomy";
import { FaTrashAlt, FaPencilAlt, FaQuestionCircle } from "react-icons/fa";

const BankDetail = () => {
  const history = useHistory();
  const [chImage, setChImage] = useState(null);
  const [bankEmail, setBankEmail] = useState("");
  const [org, setOrg] = useState("");
  const [bank, setBank] = useState("");
  const [orglist, setOrgList] = useState([]);
  const [bankList, setBankList] = useState([]);
  const [userbnkList, setUserbnkList] = useState([]);
  const [editing, setEditing] = useState(false);
  const [ids, setIds] = useState();
  const [qrcode, setQrCode] = useState(false);
  const [picedit, setPicEdit] = useState(false);
  let BankDataCover = new FormData();
  BankDataCover.append("cover", chImage);
  BankDataCover.append("bnk_msg_to", bankEmail);
  BankDataCover.append("masterbank_id", bank);
  BankDataCover.append("orgn_id", org);
  // console.log(BankDataCover)
  // Display the key/value pairs
  for (var pair of BankDataCover.entries()) {
    console.log(pair[0] + ", " + pair[1]);
  }
  const token = localStorage.getItem("user");
  const api = axios.create({
    baseURL: `https://api.digitaldocsys.in`,
  });
  useEffect(() => {
    const GetList = async () => {
      const result = await api.get("/api/orglist/", {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      });
      console.log(result.data);
      setOrgList(result.data);

      const list = await api.get("/api/mstbnklist/", {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      });
      setBankList(list.data);
      console.log(list.data);

      // const banklist = await api.get('/assignbank', {
      //     headers: {
      //         Authorization: `Bearer ${token}`,
      //     }
      // })
      // console.log(banklist.data);
      // setUserbnkList(banklist.data)

      const userbnkList = await api.get("/api/alldata", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      console.log(userbnkList.data);
      setUserbnkList(userbnkList.data);
    };
    GetList();
    // eslint-disable-next-line
  }, []);

  const SaveBankDetail = () => {
    if (!org) {
      swal("Please Enter Org Name", "Incorrect Input", "warning");
      return;
    }
    if (!bank) {
      swal("Please Enter bank Name", "Incorrect Input", "warning");
      return;
    }
    api
      .post("/assignbank", BankDataCover, {
        headers: {
          Authorization: ` Bearer ${token}`,
          "content-type": `multipart/form-data`,
        },
      })
      .then((res) => {
        history.push("/bank");
        console.log(res.data);
        window.location.reload();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const DeleteBank = (id) => {
    setEditing(false);
    api
      .delete(`/api/savebank/${id}`, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((res) => {
        swal("Your data is successfully deleted", "", "success");
        window.location.reload();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const edit = (id) => {
    setIds(id);
    setEditing(true);
    api
      .get(`/api/savebank/${id}`, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res.data);
        setOrg(res.data.orgn_id);
        setBank(res.data.masterbank_id);
        setBankEmail(res.data.bnk_msg_to);
        // setChImage(res.data.cover);
        // Display the key/value pairs
        for (var pair of BankDataCover.entries()) {
          console.log(pair[0] + ", " + pair[1]);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const Update = () => {
    const id = ids;
    if (picedit) {
      api
        .put(`/api/savebank/${id}/`, BankDataCover, {
          headers: {
            Authorization: ` Bearer ${token}`,
            "content-type": `multipart/form-data`,
          },
        })
        .then((res) => {
          window.location.reload();
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      api
        .put(
          `/api/savebanknocov/${id}/`,
          {
            bnk_msg_to: bankEmail,
            masterbank_id: bank,
            orgn_id: org,
          },
          {
            headers: {
              Authorization: ` Bearer ${token}`,
            },
          }
        )
        .then((res) => {
          window.location.reload();
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  return (
    <div>
      <Container fluid className="rowCol">
        <Title title="Bank Details" />
        <Row>
          <Col md={4}>
            <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
              <Col>
                <div className="form-group mb-3">
                  <label>Select Organization</label>
                  <select
                    value={org}
                    onChange={(e) => setOrg(e.target.value)}
                    className="form-control"
                  >
                    <option>Choose an organization</option>
                    {orglist.map((item) => (
                      <option
                        key={item.orgn_id}
                        value={item.orgn_id}
                        label={item.orgn_nm}
                      ></option>
                    ))}
                  </select>
                </div>
              </Col>
              <Col>
                <div className="form-group mb-3">
                  <label>
                    Select Bank{" "}
                    <span>
                      <FaQuestionCircle data-tip data-for="registerTip" size="12px"/>
                      <ReactTooltip
                        id="registerTip"
                        place="right"
                        effect="solid"
                        type="light"
                      >
                        If Bank Name is not Listed. Contact Us
                      </ReactTooltip>
                    </span>
                  </label>
                  <br />
                  <select
                    value={bank}
                    onChange={(e) => setBank(e.target.value)}
                    className="form-control"
                  >
                    <option>Choose a bank</option>
                    {bankList.map((item) => (
                      <option
                        key={item.mstbnk_id}
                        value={item.mstbnk_id}
                        label={item.mstbnk_nm}
                      ></option>
                    ))}
                  </select>
                </div>
              </Col>
              <Col>
                <div>
                  {!editing ? (
                    <div className="form-group mb-3">
                      <label>Upload Bank Blank Cheque</label>
                      <input
                        accept="image/png, image/jpeg"
                        type="file"
                        id="image"
                        onChange={(e) => setChImage(e.target.files[0])}
                        style={{ height: "37px" }}
                        className="form-control"
                      />
                      <h6 className="mt-3">OR</h6>
                      <a
                        style={{ color: "lightblue", cursor: "pointer" }}
                        onClick={(e) => setQrCode(true)}
                      >
                        <h6>Click Picture</h6>
                      </a>
                      <div className="text-center">
                        {qrcode ? (
                          <img
                            width="50%"
                            alt="qrcode"
                            src="https://qrcode.tec-it.com/API/QRCode?data=QR+Code+Generator+by+TEC-IT"
                          ></img>
                        ) : (
                          <div></div>
                        )}
                      </div>
                    </div>
                  ) : picedit ? (
                    <div>
                      <label>Upload Bank Blank Cheque</label>
                      <input
                        accept="image/png, image/jpeg"
                        type="file"
                        id="image"
                        onChange={(e) => setChImage(e.target.files[0])}
                        style={{ height: "37px !important" }}
                        className="form-control"
                      />
                      <h4>OR</h4>
                      <a onClick={(e) => setQrCode(true)}>
                        <h2>Click Picture</h2>
                      </a>
                      <div>
                        {qrcode ? (
                          <img
                            style={{ height: "50%", width: "50%" }}
                            alt="qrcode"
                            src="https://qrcode.tec-it.com/API/QRCode?data=QR+Code+Generator+by+TEC-IT"
                          ></img>
                        ) : (
                          <div></div>
                        )}
                      </div>
                    </div>
                  ) : (
                    <a
                      style={{ color: "lightblue", cursor: "pointer" }}
                      onClick={(e) => setPicEdit(true)}
                    >
                      <h2>Click to Edit the Picture</h2>
                    </a>
                  )}
                </div>
              </Col>
              <Col>
                <div className="form-group mb-3">
                  <label>Email</label>
                  <br />
                  <input
                    value={bankEmail}
                    type="email"
                    onChange={(e) => setBankEmail(e.target.value)}
                    className="form-control"
                  />
                </div>
              </Col>
              <Col>
                <div className="form-group mb-3 text-right">
                  {editing ? (
                    <Button variant="primary" onClick={Update}>
                      Update
                    </Button>
                  ) : (
                    <Button variant="primary" onClick={SaveBankDetail}>
                      Save
                    </Button>
                  )}
                </div>
              </Col>
            </div>
          </Col>
          <Col md={8}>
            <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
              <Table>
                <thead>
                  <tr>
                    <th>Organization Name</th>
                    <th>Bank Name</th>
                    <th>Email Id</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {userbnkList.map((item) => (
                    <tr key={item.bnk_id}>
                      <td>{item.orgn_nm}</td>
                      <td>{item.mstbnk_nm}</td>
                      <td>{item.bnk_msg_to}</td>
                      <td>
                        <FaPencilAlt onClick={() => edit(item.bnk_id)} />
                        <ReactTooltip
                          id="editTip"
                          place="top"
                          effect="solid"
                          type="light"
                        >
                          Edit
                        </ReactTooltip>
                      </td>
                      <td>
                        <FaTrashAlt
                          onClick={() => {
                            DeleteBank(item.bnk_id);
                          }}
                        />
                        <ReactTooltip
                          id="deleteTip"
                          place="top"
                          effect="solid"
                          type="light"
                        >
                          Delete
                        </ReactTooltip>
                      </td>
                    </tr>
                  ))}
                  <tr></tr>
                </tbody>
              </Table>
              <div className="text-right">
                <Button variant="primary" href="./payment">
                  Go to Payment
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default BankDetail;
