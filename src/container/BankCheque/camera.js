import React, { useRef, Fragment, useState } from "react";
import { Camera } from "react-cam";
// import ReactCrop from 'react-image-crop';
import { Card, Col, Container, Row, Button } from "react-bootstrap";
import Header from "./header";
const camera = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [imagCapture, setCapture] = useState("");
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const cam = useRef(null);
  function capture(imgSrc) {
    localStorage.setItem("img", imgSrc);
    setCapture(localStorage.getItem("img"));
  }

  return (
    <div className="rowCol cameraContainer">
      <Row>
        <Col>
          <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
            <div className="text-center">
              <h6>This Side Top</h6>
              <Camera
                showFocus={true}
                front={false}
                capture={capture}
                ref={cam}
                width="80%"
                height="80%"
                focusWidth="65%"
                focusHeight="40%"
                btnColor="lighblue"
              />
            </div>
            <div>
              <div className="text-center mt-3">
                <Button
                  variant="primary"
                  onClick={(img) => cam.current.capture(img)}
                >
                  Take image
                </Button>
              </div>
              <div>
                <img src={imagCapture} />
              </div>
              <div className="text-right">
                <input
                  type="file"
                  onChange={(e) => setCapture(e.target.value)}
                ></input>
                <Button variant="primary" href="./print" className="mx-1">
                  Save
                </Button>
                <Button variant="primary" className="mx-1">
                  Delete
                </Button>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default camera;
