import React, { useEffect, useState } from "react";
import "./report.css";
import axios from "axios";
import { Table, Col, Container, Row, Image, Form } from "react-bootstrap";
import { Title } from "../../../components/Texonomy";

const Report = () => {
  const [picsource, setPicSource] = useState("");
  const [picture, setPicture] = useState(false);
  const [item, setItem] = useState([]);
  const [lists, setList] = useState(item);
  const [category, setCategory] = useState("");
  const [min, setMin] = useState("");
  const [max, setMax] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const token = localStorage.getItem("user");
  const api = axios.create({
    baseURL: `https://api.digitaldocsys.in/api`,
  });

  //e = event for the value when select changes
  const handleFilterChange = (e, filterType) => {
    //changes state
    switch (filterType) {
      case "category":
        setCategory(e.target.value);
        break;
      case "min":
        setMin(e.target.value);
        break;
      case "max":
        setMax(e.target.value);
        break;
      case "startDate":
        setStartDate(e.target.value);
        break;
      case "endDate":
        setEndDate(e.target.value);
        break;
      default:
        break;
    }
  };

  const showPic = (id) => {
    api
      .get(`/getprintedimg/${id}/`, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res);
        const imagurl =
          "https://api.digitaldocsys.in" + String(res.data.priented_imag);
        console.log(imagurl);
        setPicSource(imagurl);
        console.log(picsource);
        console.log("show pic");
        setPicture(true);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const CSVDown = (id) => {
    api
      .get(`/genreatecsv/${id}/`, {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res);
        window.open(res.request.responseURL);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    const GetList = async () => {
      const items = await api.get("/pyamentlist/", {
        headers: {
          Authorization: ` Bearer ${token}`,
        },
      });
      setItem(items.data);
      console.log(items.data);
    };
    GetList();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    let filteredProducts = item;
    if (category !== "") {
      filteredProducts = filteredProducts.filter((list) =>
        list.pymnt_nm.toLowerCase().includes(category.toLowerCase())
      );
    }
    if (min !== "") {
      filteredProducts = filteredProducts.filter(
        (list) => list.pymnt_chq_amt > min
      );
    }
    if (max !== "") {
      filteredProducts = filteredProducts.filter(
        (list) => list.pymnt_chq_amt < max
      );
    }
    if (startDate !== "") {
      filteredProducts = filteredProducts.filter((list) =>
        list.pymnt_chq_dt.includes(startDate)
      );
    }
    setList(filteredProducts);
  }, [item, category, min, max, startDate, endDate]);

  return (
    <div className="rowCol customContainer">
      <Col xs={12}>
        <Title title="Reports" />
        <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
          <Row>
            <Col md={4}>
              <label className="font-size-12px pl-2">Date Range</label>
              <Row>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      onChange={(e) => handleFilterChange(e, "startDate")}
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      onChange={(e) => handleFilterChange(e, "endDate")}
                    />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col md={4}>
              <label className="font-size-12px pl-2">Payee Name</label>
              <Row>
                <Col>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      onChange={(e) => handleFilterChange(e, "category")}
                    />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col md={4}>
              <label className="font-size-12px pl-2">Amount Range</label>
              <Row>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      onChange={(e) => handleFilterChange(e, "min")}
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      onChange={(e) => handleFilterChange(e, "max")}
                    />
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>

          <Table responsive hover className="mt-3">
            <thead>
              <tr>
                <th>Payee Name</th>
                <th>Bank Name</th>
                <th>Payee a/c No.</th>
                <th>Cheque date</th>
                <th>Cheque Amt</th>
                <th>CSV</th>
              </tr>
            </thead>
            <tbody>
              {lists.map((list) => (
                <tr key={list.pymnt_id}>
                  <td
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      showPic(list.pymnt_id);
                    }}
                  >
                    {list.pymnt_nm}
                  </td>
                  <td
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      showPic(list.pymnt_id);
                    }}
                  >
                    {list.bnk_nm}
                  </td>
                  <td
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      showPic(list.pymnt_id);
                    }}
                  >
                    {list.pymnt_ac_no}
                  </td>
                  <td
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      showPic(list.pymnt_id);
                    }}
                  >
                    {list.pymnt_chq_dt}
                  </td>
                  <td
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      showPic(list.pymnt_id);
                    }}
                  >
                    {list.pymnt_chq_amt}
                  </td>
                  <td>
                    <input
                      type="checkbox"
                      name="cms"
                      onClick={() => {
                        CSVDown(list.pymnt_id);
                      }}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </Col>
      <Row>
        <Col xs={12}>
          {picture ? (
            <Image style={{ width: "50%" }} src={picsource} alt="hello" />
          ) : (
            <div></div>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default Report;
