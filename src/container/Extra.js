import React, { useState } from "react"
import { Button } from "react-bootstrap";
import { Modal1 } from "../components/Modal/Modal1";
import { Modal2 } from "../components/Modal/Modal2";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Error, Success, Warning } from "../components/Message/Message";

const Extra = () => {
    //modal 1
    const [showModal1, setShowModal1] = useState(false);
    const handleCloseModal1 = () => setShowModal1(false);
    const handleShowModal1 = () => setShowModal1(true);
    //modal 2
    const [showModal2, setShowModal2] = useState(false);
    const handleCloseModal2 = () => setShowModal2(false);
    const handleShowModal2 = () => setShowModal2(true);
    //toast
    const success = () => toast.success(<Success text="Success Message Here" />);
    const error = () => toast.error(<Error text="Error Message Here" />);
    const warning = () => toast.warning(<Warning text="Warning Message Here" />);
    return (<>
        <h1>Modals</h1>
        <Button type="button" variant="primary" onClick={handleShowModal1}>Modal 1</Button>
        <Modal1 show={showModal1} handleClose={handleCloseModal1} title="Title Goes Here" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s," />
        <Button type="button" variant="primary" onClick={handleShowModal2}>Modal 2</Button>
        <Modal2 show={showModal2} handleClose={handleCloseModal2} title="Title Goes Here" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s," />
        <Button type="button" variant="primary" onClick={success}>success</Button>
        <Button type="button" variant="primary" onClick={error}>Error</Button>
        <Button type="button" variant="primary" onClick={warning}>Warning</Button>
        <ToastContainer />
    </>)
}
export default Extra;