import React, { useState, useRef } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import swal from "sweetalert";
import {
  ContactIcon,
  EyeIcon,
  EyeSlashIcon,
  LockIcon,
  LogoIcon,
  UserIcon,
} from "../../components/Icons/Icons";

import logoDDS from "./../../assets/logoDDS.svg";
import axios from "axios";
import { useForm } from "react-hook-form";

const Register = () => {
  const [showPassword, setShaowpassword] = useState(false);
  const [checked, setChecked] = useState(false);
  const onBtnClick = (e) => {
    if (checked) {
      setChecked(false);
    } else {
      setChecked(true);
    }
  };
  const history = useHistory();
  // eslint-disable-next-line
  const { handleSubmit } = useForm();
  let userRef = useRef("");
  let emailRef = useRef("");
  let mobileRef = useRef("");
  let passRef = useRef("");
  let cpassRef = useRef("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [mobile, setMobile] = useState("");
  const [user, setUser] = useState("");
  const [confirmPwd, setConfirmPwd] = useState("");
  // eslint-disable-next-line
  const [error, setError] = useState(false);

  const handleSignUp = () => {
    if (!user) {
      swal("Please Enter username", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (!email) {
      swal("Please Enter Email", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (!mobile) {
      swal("Please Enter Mobile Number", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (!password) {
      swal("Please Enter Password", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (password.length < 6) {
      swal(
        "Password too short",
        "Please enter more than 6 characters",
        "warning"
      );
      setError(true);
      return;
    }
    if (password !== confirmPwd) {
      swal("Password is not matching", "", "warning");
      setError(true);
      return;
    }
    if (!checked) {
      swal(
        "Terms & Conditions isn't accepted",
        "Please check the checkbox",
        "warning"
      );
      setError(true);
      return;
    }
    const signupData = {
      name: user,
      email: email,
      mobile: mobile,
      password: password,
    };
    const api = axios.create({
      baseURL: `https://api.digitaldocsys.in/api`,
    });

    // eslint-disable-next-line
    const emailverify = () => {
      api.get("/email-verify/").then((res) => localStorage.setItem("user"));
    };
    api
      .post("/register/", signupData)
      .then((res) => {
        history.push("/login");
        console.log(res.data);
      })
      .catch((error) => {
        console.log(error);
        swal("Email Already registered", "", "error");
      });
  };
  const clear = function () {
    userRef.current.value = "";
    emailRef.current.value = "";
    mobileRef.current.value = "";
    passRef.current.value = "";
    cpassRef.current.value = "";
  };

  return (
    <>
      <div className="TM_LoginPage">
        <Container fluid>
          <Row className="justify-content-center align-items-center">
            <Col
              md={10}
              className="py-md-5 py-3 d-flex justify-content-between"
            >
              <Link to="/" className="logodds">
                <img src={logoDDS} alt="logo" />
              </Link>
              <Link to="/">
                <ContactIcon />
              </Link>
            </Col>
            <Col lg={5}>
              <div className="text-center py-md-4 pb-3">
                <div className="logoicon mb-4">
                  <LogoIcon />
                </div>
                <div className="font-size-md-16px font-size-14px text-uppercase opacity-60 text-primary pt-3">
                  Welcome to
                </div>
                <div className="font-size-md-32px font-size-18px font-weight-700 text-primary ">
                  smart.cheque
                </div>
              </div>
              <Card className="loginCard">
                <Card.Body>
                  <div className="form-group">
                    <label className="font-size-14px pl-2 opacity-60">
                      Username
                    </label>
                    <div className="position-relative mb-3">
                      <input
                        type="text"
                        className="form-control w-100"
                        placeholder="Username"
                        autoComplete="off"
                        name="user"
                        onChange={(e) => setUser(e.target.value)}
                        ref={userRef}
                      />
                      <UserIcon />
                    </div>
                  </div>
                  <div className="form-group">
                    <label className="font-size-14px pl-2 opacity-60">
                      Email
                    </label>
                    <div className="position-relative mb-3">
                      <input
                        type="email"
                        className="form-control w-100"
                        placeholder="Email"
                        autoComplete="off"
                        name="email"
                        onChange={(e) => setEmail(e.target.value)}
                        ref={emailRef}
                      />
                      <UserIcon />
                    </div>
                  </div>
                  <div className="form-group">
                    <label className="font-size-14px pl-2 opacity-60">
                      Mobile Number
                    </label>
                    <div className="position-relative mb-3">
                      <input
                        type="text"
                        className="form-control w-100"
                        placeholder="Mobile Number"
                        autoComplete="off"
                        name="mobile"
                        onChange={(e) => setMobile(e.target.value)}
                        ref={mobileRef}
                      />
                      <UserIcon />
                    </div>
                  </div>
                  <div className="form-group">
                    <label className="font-size-14px pl-2 opacity-60">
                      New Password
                    </label>
                    <div className="position-relative mb-3">
                      <input
                        type={showPassword ? "text" : "password"}
                        className="form-control w-100"
                        placeholder="New Password"
                        autoComplete="off"
                        name="password"
                        onChange={(e) => setPassword(e.target.value)}
                        ref={passRef}
                      />
                      <LockIcon />
                      <button
                        type="button"
                        className="showPassword btn-link btn p-0"
                        onClick={() => setShaowpassword(!showPassword)}
                      >
                        {!showPassword ? <EyeIcon /> : <EyeSlashIcon />}
                      </button>
                    </div>
                  </div>
                  <div className="form-group">
                    <label className="font-size-14px pl-2 opacity-60">
                      Conform Password
                    </label>
                    <div className="position-relative mb-3">
                      <input
                        type={showPassword ? "text" : "password"}
                        className="form-control w-100"
                        placeholder="Conform Password"
                        autoComplete="off"
                        name="confpassword"
                        onChange={(e) => setConfirmPwd(e.target.value)}
                        ref={cpassRef}
                      />
                      <LockIcon />
                      <button
                        type="button"
                        className="showPassword btn-link btn p-0"
                        onClick={() => setShaowpassword(!showPassword)}
                      >
                        {!showPassword ? <EyeIcon /> : <EyeSlashIcon />}
                      </button>
                    </div>
                  </div>
                  <div className="form-group">
                <Col
                  style={{ textAlign: "center", width: "100%", padding: "0%" }}
                  xs={12}
                >
                  <label>
                    I agree to the{" "}
                    <a href="/termconditions">terms and conditions</a>
                  </label>
                  <input type="checkbox" onChange={onBtnClick} style={{width: '5%' }}/>
                </Col>
              </div>
                  
                  <button
                    type="submit"
                    className="btn btn-primary w-100 mt-3"
                    onClick={handleSignUp}
                  >
                    Register
                  </button>
                </Card.Body>
              </Card>
              <div className="text-center pt-md-4">
                <div className="font-size-md-20px font-size-16px pt-3">
                  Back To Login
                </div>
                <Link
                  to="/"
                  className="mt-3 font-size-md-16px font-size-14px ok-btn btn py-1 px-4 h-auto w-auto"
                >
                  Login Here
                </Link>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};
export default Register;
