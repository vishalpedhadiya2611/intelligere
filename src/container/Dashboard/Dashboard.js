import React, { useEffect, useState } from "react"
import { Button, Col, Container, Row, Table } from "react-bootstrap";
import { Title } from "../../components/Texonomy";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Chart from 'react-apexcharts'

const Dashboard = () => {
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');




   

    const [state, setState] = useState({
        series1: [20, 20, 20, 20, 20, 20],
        options1: {
            labels: ['LoremHere', 'LoremHere', 'LoremHere', 'LoremHere', "LoremHere", "LoremHere"],
            colors: ['#FF8546', '#00A4F9', '#1515AC', '#14B74F', '#F2D620', '#D13246'],
            legend: {
                position: 'bottom'
            },
            stroke: {
                show: false,
            },
            dataLabels: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    donut: {
                        size: '85%'
                    }
                }
            },
          
        },
        series2: [{
            name: 'apexchart',
            data: [30, 40, 35, 50, 49, 60],
        }, {
            name: 'apexchart2',
            data: [60, 49, 50, 35, 40, 30],
        }],
        options2: {
            chart: {
                id: 'apexchart'
            },
            stroke: {
                curve: 'smooth'
            },
            legend: {
                show: false
            },
            colors: ["#14B74F", "#D13246"],
            xaxis: {
                categories: ["5 July", "10 July", "15 July", "20 July", "25 July", "31 July"]
            },
            grid: {
                borderColor: '#8EB6CF',
                strokeDashArray: 5
            }

        },

    })
    return (<>
        <Container fluid>
            <Title title="Dashboard" />
            <Row>
                <Col lg={9} className="mb-md-4 mb-3">
                    <div className="WhiteShadowCard rowCol h-100">
                        <div className="border-bottom p-30px">
                            <Row>
                                <Col lg={4}>
                                    <div className="form-group">
                                        <select className="form-control">
                                            <option value="none" >
                                                Select a Company
                                            </option>
                                            <option>Comapny 1</option>
                                            <option>Comapny 2</option>
                                            <option>Comapny 3</option>
                                        </select>
                                    </div>
                                </Col>
                                <Col lg={6}>
                                    <div className="row">
                                        <Col className="col-md-6">
                                            <div className="form-group">
                                                <DatePicker
                                                    selected={startDate}
                                                    onChange={(date) => setStartDate(date)}
                                                    selectsStart
                                                    className="form-control"
                                                    startDate={startDate}
                                                    endDate={endDate}
                                                    placeholderText="From Date"
                                                />
                                            </div>
                                        </Col>
                                        <Col className="col-md-6">
                                            <div className="form-group">
                                                <DatePicker
                                                    selected={endDate}
                                                    onChange={(date) => setEndDate(date)}
                                                    selectsEnd
                                                    className="form-control"
                                                    startDate={startDate}
                                                    endDate={endDate}
                                                    minDate={startDate}
                                                    placeholderText="To Date"
                                                />
                                            </div>
                                        </Col>
                                    </div>
                                </Col>
                                <Col lg={2}>
                                    <Button variant="primary">Show Result</Button>
                                </Col>
                            </Row>
                        </div>
                        <div className="p-30px dashTableMain">
                            <Row>
                                <Col lg={9}>
                                    <Row>
                                        <Col lg={6}>
                                            <div className="font-size-md-18px font-size-14px font-weight-700 text-primary pb-1">Account Payable</div>
                                            <Table responsive hover className="dashTable">
                                                <thead>
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Party</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td className="text-primary">28 July 2021</td>
                                                        <td className="text-primary">Company Name Here</td>
                                                        <td className="text-secondary">50,000.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="text-primary">28 July 2021</td>
                                                        <td className="text-primary">Company Name Here</td>
                                                        <td className="text-secondary">50,000.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="text-primary">28 July 2021</td>
                                                        <td className="text-primary">Company Name Here</td>
                                                        <td className="text-secondary">50,000.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="text-primary">28 July 2021</td>
                                                        <td className="text-primary">Company Name Here</td>
                                                        <td className="text-secondary">50,000.00</td>
                                                    </tr>

                                                </tbody>
                                            </Table>
                                        </Col>
                                        <Col lg={6}>
                                            <div className="font-size-md-18px font-size-14px font-weight-700 text-primary pb-1">Account Receivable</div>
                                            <Table responsive hover className="dashTable">
                                                <thead>
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Party</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td className="text-primary">28 July 2021</td>
                                                        <td className="text-primary">Company Name Here</td>
                                                        <td className="text-secondary">50,000.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="text-primary">28 July 2021</td>
                                                        <td className="text-primary">Company Name Here</td>
                                                        <td className="text-secondary">50,000.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="text-primary">28 July 2021</td>
                                                        <td className="text-primary">Company Name Here</td>
                                                        <td className="text-secondary">50,000.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="text-primary">28 July 2021</td>
                                                        <td className="text-primary">Company Name Here</td>
                                                        <td className="text-secondary">50,000.00</td>
                                                    </tr>
                                                </tbody>
                                            </Table>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={3} className="pt-lg-4">
                                    <div className="borderBox h-105px mt-2">
                                        <div className="text-primary font-weight-700 font-size-md-16px font-size-14px pb-2">Bank Balance</div>
                                        <div className="font-size-md-20px font-size-14px font-size-16px">50,00,000.00</div>
                                    </div>
                                    <div className="borderBox h-105px mt-2">
                                        <div className="text-primary font-weight-700 font-size-md-16px font-size-14px pb-2">Total Turnover</div>
                                        <div className="font-size-md-20px font-size-14px font-size-16px ">50,00,000.00</div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </Col>
                <Col lg={3} className="mb-md-4 mb-3">
                    <div className="WhiteShadowCard h-100">
                        <div className="font-size-md-16px font-size-14px text-primary font-weight-700 p-30px">Important Date</div>
                        <div className="bg-light p-30px py-2">
                            <Row>
                                <Col xs={4} className="text-primary font-size-14px opacity-40">Date</Col>
                                <Col xs={8} className="text-primary font-size-14px opacity-40">Message</Col>
                            </Row>
                        </div>
                        <div className="p-30px py-2 border-bottom">
                            <Row className="align-items-center">
                                <Col xs={4} className="text-primary font-size-14px">28 July 2021</Col>
                                <Col xs={8} className="font-size-14px">Vestibulum id sem efficitur
                                    pretium elit sed, efficitur orci.</Col>
                            </Row>
                        </div>
                        <div className="p-30px py-2 border-bottom">
                            <Row className="align-items-center">
                                <Col xs={4} className="text-primary font-size-14px">28 July 2021</Col>
                                <Col xs={8} className="font-size-14px">Vestibulum id sem efficitur
                                    pretium elit sed, efficitur orci.</Col>
                            </Row>
                        </div>
                        <div className="p-30px py-2 border-bottom">
                            <Row className="align-items-center">
                                <Col xs={4} className="text-primary font-size-14px">28 July 2021</Col>
                                <Col xs={8} className="font-size-14px">Vestibulum id sem efficitur
                                    pretium elit sed, efficitur orci.</Col>
                            </Row>
                        </div>
                        <div className="p-30px py-2 border-bottom">
                            <Row className="align-items-center">
                                <Col xs={4} className="text-primary font-size-14px">28 July 2021</Col>
                                <Col xs={8} className="font-size-14px">Vestibulum id sem efficitur
                                    pretium elit sed, efficitur orci.</Col>
                            </Row>
                        </div>
                    </div>
                </Col>
                <Col lg={4} className="mb-md-4 mb-3">
                    <div className="WhiteShadowCard p-30px h-100">
                        <div className="font-size-md-18px font-size-14px font-weight-700 text-primary pb-2">Payment Module</div>
                        <Row>
                            <Col md={5} className="mb-md-0 mb-2">
                                <div className="font-size-md-16px font-size-14px text-primary">Entries To Be Verified</div>
                                <div className="font-size-md-16px font-size-14px ">0000000</div>
                            </Col>
                            <Col md={7} className="mb-md-0 mb-2">
                                <div className="font-size-md-16px font-size-14px text-primary ">Verified Entries</div>
                                <div className="font-size-md-16px font-size-14px ">0000000</div>
                            </Col>
                        </Row>
                    </div>
                </Col>
                <Col lg={4} className="mb-md-4 mb-3">
                    <div className="WhiteShadowCard p-30px h-100">
                        <div className="font-size-md-18px font-size-14px font-weight-700 text-primary pb-2">Bank Statement Module</div>
                        <Row>
                            <Col md={5} className="mb-md-0 mb-2">
                                <div className="font-size-md-16px font-size-14px text-primary">Entries To Be Verified</div>
                                <div className="font-size-md-16px font-size-14px ">0000000</div>
                            </Col>
                            <Col md={7} className="mb-md-0 mb-2">
                                <div className="font-size-md-16px font-size-14px text-primary ">Verified Entries</div>
                                <div className="font-size-md-16px font-size-14px ">0000000</div>
                            </Col>
                        </Row>
                    </div>
                </Col>
                <Col lg={4} className="mb-md-4 mb-3">
                    <div className="WhiteShadowCard p-30px h-100">
                        <div className="font-size-md-18px font-size-14px font-weight-700 text-primary pb-2">Invoice Module</div>
                        <Row>
                            <Col md={5} className="mb-md-0 mb-2">
                                <div className="font-size-md-16px font-size-14px text-primary">Entries To Be Verified</div>
                                <div className="font-size-md-16px font-size-14px ">0000000</div>
                            </Col>
                            <Col md={7} className="mb-md-0 mb-2">
                                <div className="font-size-md-16px font-size-14px text-primary ">Verified Entries</div>
                                <div className="font-size-md-16px font-size-14px ">0000000</div>
                            </Col>
                        </Row>
                    </div>
                </Col>
                <Col lg={3} className="mb-md-4 mb-3">
                    <div className="WhiteShadowCard p-30px h-100">
                        <Chart options={state.options1} series={state.series1} type="donut" width={'100%'} height={300} />
                    </div>
                </Col>
                <Col lg={9} className="mb-md-4 mb-3">
                    <div className="WhiteShadowCard p-30px h-100">
                        <div className="d-flex justify-content-between">
                            <div className="font-size-md-16px font-size-14px text-primary font-weight-700">Line Graph</div>
                            <select className="form-control w-100px">
                                <option>July 2021</option>
                                <option>June 2021</option>
                                <option>May 2021</option>
                            </select>
                        </div>
                        <Chart options={state.options2} series={state.series2} type="line" width={'100%'} height={300} />
                    </div>
                </Col>
            </Row>
        </Container>
    </>)
}
export default Dashboard;