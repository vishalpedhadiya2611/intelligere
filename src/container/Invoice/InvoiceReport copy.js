import React, { useEffect, useState } from "react";
import { Button, Container, Row, Col, Table } from "react-bootstrap";
import PdfContainer from "./PdfContainer";
import InvoiceContainer from "./InvoiceContainer";
import { Title } from "../../components/Texonomy";
import DatePicker from "react-datepicker";
import axios from "axios";

import "react-datepicker/dist/react-datepicker.css";
import { CompanyListAPI } from "../BankStatements/Api";
import { useDispatch } from "react-redux";
import { companyListAction } from "../../redux/action/BankStatementAction";
import moment from "moment";
function InvoiceReport() {
  const dispatch = useDispatch();
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [range, setRange] = useState({
    to: "",
    from: "",
  });
  const [companyList, setCompanyList] = useState([]);

  const [companyData, setCompanyData] = useState([
    {
        id: 54,
        companyname: "Raj Electronics (18-19)",
        Company: 1,
        invoice_no: "Invoice No.RA.I/20-21/0617",
        subtotal: 100.0,
        invoice_date: "2021-10-10",
        payment_mode: null,
        CGST: "SGSTCGST          247.50247.50",
        SGST: "SGSTCGST          247.50247.50",
        IGST: "SGSTCGST          247.50247.50",
        bank_details: "Company’s Bank DetailsBank Name : KALUPUR COMMERCIAL CO.OP. BANKA/c No. : 01336100406Branch &IFS Code : PANCHVATI & KCCBOPCV013",
        company_details: "Ra Electron ics (18 9AbmedabadGSTIN/UIN: 24alHnPSs2276H1Z3State Name = Gujarat, Gode -24Gontact 079 ~ 26461690 / 2646191 1,9825269099E-Mail = sundip@rajelectronics.com",
        legdername: null,
        legderdata: "PURCHASE",
        GSTlegderdata: null,
        GSTlegderamount: null,
        Narration: "INVOICEInvoice No.RA.I/20-21/0617\fto your company.",
        csvreceipt: [
            {
                "id": 1,
                "Products": "Machinewry",
                "HSN_SAC": "402",
                "GST_rate": 4,
                "quantity": 4,
                "Rate": "100",
                "Amount": "1000",
                "Total": "10000",
                "CGST": 2,
                "SGST": 2,
                "IGST": 2,
                "Invoice_data": 54
            }
        ]
    }
]);
  const [selectedCompany, setSelectedCompany] = useState(0);

  useEffect(async () => {
    // Company :List API
    // setCompanyList(companyListData);

    await axios({
      method : "get",
      url: 'http://127.0.0.1:8000/invoice/companyshow/'
    }).then(res => setCompanyList(res.data))
    .catch(err => console.log(err))
  }, []);

  const receiptinvoicereport = async () => {
    await axios({
      url: "http://127.0.0.1:8000/invoice/receiptinvoicereport/",
      method: "get",
      params: {
        subtotal_min: range.from,
        subtotal_max: range.to,
        Company: selectedCompany,
        invoice_date_after: startDate,
        invoice_date_before: endDate,
      },
    })
      .then((res) => {
        setCompanyData(res.data)})
      .catch((err) => console.log(err));
  };

  return (
    <>
      <Container fluid className="rowCol">
        <Title title="Invoice Report" />
        <Row>
          <Col xl={6} className="w-1366">
            <div className="WhiteShadowCard p-30px mb-3">
              <div className="form-group">
                <select className="form-control" onChange={(e) => setSelectedCompany(e.target.value)}>
                  <option
                    value="none"
                    selected
                    disabled
                  >
                    Select a Company
                  </option>
                  {companyList.map((res) => (
                    <option key={res.id} value={res.id}>
                      {res.comp_name}
                    </option>
                  ))}
                </select>
              </div>
              
              <Row>
                <Col lg={6}>
                  <label className="font-size-12px pl-2">Select Date</label>
                  <div className="row">
                    <Col className="col-md-6">
                      <div className="form-group">
                        <DatePicker
                          selected={startDate != "" ? new Date(startDate) : ""}
                          onChange={(date) =>
                            setStartDate(moment(date).format("YYYY-MM-DD"))
                          }
                          selectsStart
                          className="form-control"
                          startDate={startDate != "" ? new Date(startDate) : ""}
                          endDate={endDate != "" ? new Date(endDate) : ""}
                          placeholderText="From Date"
                          maxDate={endDate != "" ? new Date(endDate) : ""}
                        />
                      </div>
                    </Col>
                    <Col className="col-md-6">
                      <div className="form-group">
                        <DatePicker
                          selected={endDate != "" ? new Date(endDate) : ""}
                          onChange={(date) =>
                            setEndDate(moment(date).format("YYYY-MM-DD"))
                          }
                          selectsEnd
                          className="form-control"
                          startDate={startDate != "" ? new Date(startDate) : ""}
                          endDate={endDate != "" ? new Date(endDate) : ""}
                          minDate={startDate != "" ? new Date(startDate) : ""}
                          placeholderText="To Date"
                        />
                      </div>
                    </Col>
                  </div>
                </Col>
                <Col lg={6}>
                  <label className="font-size-12px pl-2">Select Range</label>
                  <div className="row">
                    <Col className="col-md-6">
                      <div className="form-group">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="From Range"
                          name="from"
                          onChange={(e) =>
                            setRange({
                              ...range,
                              [e.target.name]: e.target.value,
                            })
                          }
                        />
                      </div>
                    </Col>
                    <Col className="col-md-6">
                      <div className="form-group">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="To Range"
                          name="to"
                          onChange={(e) =>
                            setRange({
                              ...range,
                              [e.target.name]: e.target.value,
                            })
                          }
                        />
                      </div>
                    </Col>
                  </div>
                </Col>
              </Row>
              <Button variant="primary" onClick={receiptinvoicereport}>
                OK
              </Button>
            </div>
            <div className="WhiteShadowCard p-30px mb-3">
              <Table responsive hover>
                <thead>
                  <tr>
                    <th>SR No.</th>
                    <th>Company Name</th>
                    <th>From Date</th>
                    <th>Total Entires</th>
                  </tr>
                </thead>
                <tbody>
                  {companyData.map((data, ind) => (
                    <tr key={data.id}>
                      <td>1</td>
                      <td>{data.companyname}</td>
                      <td>{data.invoice_date}</td>
                      <td>
                          {data.csvreceipt.length}
                        <button className="ok-btn ml-2" >View</button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
            <div className="WhiteShadowCard p-30px mb-3 mb-xl-0">
              {/* <InvoiceContainer report={true} /> */}
            </div>
          </Col>
          <Col xl={6} className="w-1366">
            <div className="WhiteShadowCard h-100">
              <PdfContainer />
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default InvoiceReport;
