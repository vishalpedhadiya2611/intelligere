// var FormData = require('form-data');
import React, { useState, useRef, useEffect } from "react";
import { Button, Col, Container, Form, Row, Table } from "react-bootstrap";
import DatePicker from "react-datepicker";
import { useReactToPrint } from "react-to-print";
import { PlusIcon } from "../../components/Icons/Icons";
import { Title } from "../../components/Texonomy";
import InvoicePreview from "./InvoicePreview";
import HsnResponse from "./HSN.json";
import axios from "axios";
import {
  addBuyerData,
  addOtherData,
  addProductData,
  addSellerData,
} from "../../redux/action/BankStatementAction";
import { useDispatch } from "react-redux";
import moment from "moment";
import Select from "react-select";
import { useSelector } from "react-redux";
import { NewVoucherEntry } from "../../components/Modal/NewVoucherEntry";
import PDF from "../../doc/pdf.pdf";
import { toast } from "react-toastify";
import { Warning } from "../../components/Message/Message";

const InvoiceCreate = () => {
  const { sellerData, buyerData, otherData, productData } = useSelector(
    (state) => state.InvoiceCreateReducer
  );
  const [startDate, setStartDate] = useState(null);
  const [sellerInputData, setSellerInputData] = useState({});
  const [buyerInputData, setBuyerInputData] = useState({});
  const [otherInputData, setOtherInputData] = useState({});
  const [sellerCompanyList, setSellerCompanyList] = useState([
    {
      id: 1,
      comp_name: "DDS",
      user_company: "Rajan",
      comp_id: "dsfdf",
      mac_ad: "fdf",
      ip_add: "fdewf",
      VAT_no: "24073700962",
      CST_no: "24573700962",
      PAN_no: "AGGPS7898N",
      Bankname: "CitiBank",
      Account_no: "002405002564",
      Branch: "Ahmedabad",
      IFSC_code: "CITI000001",
    },
    {
      id: 2,
      comp_name: "DDS2",
      user_company: "Rajan2",
      comp_id: "dsfdf2",
      mac_ad: "fdf2",
      ip_add: "fdewf2",
      VAT_no: "24073700962",
      CST_no: "24573700962",
      PAN_no: "AGGPS7898N",
      Bankname: "CitiBank",
      Account_no: "002405002564",
      Branch: "Ahmedabad",
      IFSC_code: "CITI000001",
    },
  ]);
  const [buyerCompanyList, setBuyerCompanyList] = useState([
    {
      id: 1,
      companyname: "DDS",
      ledeger_name: "Rakesh DDS",
      ledeger_phone: "9876543212",
      ledeger_email: "rakesh@gmail.com",
      ledeger_address: "Sumkeru Center,Prahladnagar",
      ledeger_state: "Gujarat",
      ledeger_gstin: "GSTIN24988979687",
    },
    {
      id: 2,
      companyname: "rrd",
      ledeger_name: "Rakesh rrd",
      ledeger_phone: "9876543212",
      ledeger_email: "rakesh@gmail.com",
      ledeger_address: "Sumkeru Center,Prahladnagar",
      ledeger_state: "Gujarat",
      ledeger_gstin: "GSTIN24988979687",
    },
    {
      id: 3,
      companyname: "rrd4",
      ledeger_name: "Rakesh rrd4",
      ledeger_phone: "9876543212",
      ledeger_email: "rakesh@gmail.com",
      ledeger_address: "Sumkeru Center,Prahladnagar",
      ledeger_state: "Gujarat",
      ledeger_gstin: "GSTIN24988979687",
    },
  ]);
  const [searchValue, setSearchValue] = useState([]);
  const [options, setOptions] = useState();

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
    documentTitle: "E-Invoice",
  });
  const [hsnSacList, setHsnSacList] = useState(HsnResponse);
  const [hsnOptions, setHsnOptions] = useState([]);
  const [hsnProductOption, setHsnProductOption] = useState([]);

  const [hsnData, setHsnData] = useState({});
  const [igstShow, setIgstShow] = useState(false);
  const [productHsn, setProductHsn] = useState(true);
  const [hsnCode, setHsnCode] = useState(true);
  const bodyFormData = new FormData();

  //handleShowVoucherEntry
  const [ShowVoucherEntry, setShowVoucherEntry] = useState(false);
  const handleShowVoucherEntry = () => setShowVoucherEntry(true);
  const handleHideVoucherEntry = () => setShowVoucherEntry(false);

  let resetHSN = {
    id: null,
    Hsn_code: "",
    Description: "",
    CGst_rate: "",
    SGst_rate: "",
    IGst_rate: "",
    Per: "",
    Rate: "",
    user: "",
    quantity: "",
    disc: 0,
  };

  useEffect(() => {
    if (
      sellerData.seller_state == buyerData.buyer_state &&
      sellerData != undefined
    ) {
      setIgstShow(!igstShow);
    }
  }, [sellerData, buyerData]);

  const dispatch = useDispatch();

  useEffect(() => {
    sellerCompnayListAPI();
    HsnSacList();
  }, []);

  useEffect(() => {
    SearchOptionValue();
  }, [buyerCompanyList]);
  useEffect(() => {
    HsnSearchOption();
  }, [hsnSacList]);

  const HsnSearchOption = () => {
    let searchV = [];
    let SearchV2 = [];
    hsnSacList.map((data) => {
      searchV.push({ value: data.Hsn_code, label: data.Hsn_code });
      SearchV2.push({ value: data.Description, label: data.Description });
    });
    setHsnOptions(searchV);
    setHsnProductOption(SearchV2);
  };

  const SearchOptionValue = () => {
    let SearchV = [];
    buyerCompanyList.map((data) => {
      SearchV.push({ value: data.id, label: data.companyname });
    });
    setOptions(SearchV);
  };

  // Seller OnChange

  const OnInputChange = (InputName, InputValue, NextSpan) => {
    if (NextSpan) {
      removeSingle(NextSpan);
    }
    if (InputName[0] == "s") {
      setSellerInputData({ ...sellerInputData, [InputName]: InputValue });
    } else if (InputName[0] == "b") {
      setBuyerInputData({ ...buyerInputData, [InputName]: InputValue });
    } else {
      setOtherInputData({ ...otherInputData, [InputName]: InputValue });
    }
  };

  // Api Call

  const BuyerCompanyList = async () => {
    await axios({
      method: "get",
      url: "http://127.0.0.1:8000/invoice/legdershow/",
    })
      .then((res) => {
        setBuyerCompanyList(res.data);
      })
      .catch((err) => console.log(err));
  };

  const sellerCompnayListAPI = async () => {
    await axios({
      method: "get",
      url: "http://127.0.0.1:8000/invoice/companyshow/",
    })
      .then((res) => {
        setSellerCompanyList([
          {
            id: 0,
            comp_name: "select",
          },
          ...res.data,
        ]);
        BuyerCompanyList();
      })
      .catch((err) => console.log(err));
  };

  const HsnSacList = async () => {
    await axios({
      method: "get",
      url: "http://127.0.0.1:8000/invoice/UploadCSVView/",
    })
      .then((res) => {
        setHsnSacList(res.data);
      })
      .catch((err) => console.log(err));
  };

  const removeSingle = (e) => {
    e.remove();
  };

  const SubmitSellerDataAPI = async () => {
    const ClickedRow = document.getElementById("sellerForm");
    let listOFRequired = [...ClickedRow.getElementsByClassName("required")];
    listOFRequired.forEach((element) => {
      if (element.value == "" && !element.nextElementSibling) {
        const spanTag = document.createElement("span");
        spanTag.classList.add("requiredError");
        const message = document.createTextNode("Required !");
        spanTag.appendChild(message);
        spanTag.style.color = "red";
        spanTag.style.fontSize = "12px"
        element.after(spanTag);
      } 
      
      
    });

    let ErrorList = [...ClickedRow.getElementsByClassName("requiredError")];
   

    if (ErrorList.length == 0) {
      const res = {
        id: 2,
        seller_company: "Krishna Traders",
        seller_name: "Rakesh Dharam",
        seller_phone: "9876543212",
        seller_email: "rakesh@gmail.com",
        seller_address: "Sumeru Center",
        seller_website: "www.rakesh.com",
        seller_state: "Gujarat",
        seller_gstin: "GJ125",
      };

      const selectedcomapny = sellerCompanyList.filter(
        (data) => data.id == sellerInputData.seller_company
      )[0];

      const bankData = {
        seller_bankname: selectedcomapny.Bankname,
        seller_accountno: selectedcomapny.Account_no,
        seller_ifccode: selectedcomapny.IFSC_code,
        seller_gstno: selectedcomapny.CST_no,
        seller_panno: selectedcomapny.PAN_no,
        seller_tanno: selectedcomapny.VAT_no,
      };

      dispatch(addSellerData({ ...res, ...bankData }));
      await axios({
        url: "http://127.0.0.1:8000/invoice/sellerdata/",
        method: "post",
        data: sellerInputData,
      })
        .then((res) => {
          dispatch(addSellerData({ ...res.data, ...bankData }));
          setSellerInputData({});
        })
        .catch((err) => console.log(err));
    }
  };

  const SubmitBuyerDataAPI = async () => {
    // const res = {
    //   id: 4,
    //   buyer_company: "DDS",
    //   buyer_name: "Rakesh Dharam",
    //   buyer_phone: "9876543212",
    //   buyer_email: "rakesh@gmail.com",
    //   buyer_address: "Sumeru Center",
    //   buyer_state: "Gujarat",
    //   buyer_gstin: "GJ125",
    //   buyer_website: "www.buyer.com",
    // };

    // dispatch(addBuyerData(res));

    await axios({
      url: "http://127.0.0.1:8000/invoice/buyerdata/",
      method: "post",
      data: buyerInputData,
    })
      .then((res) => {
        dispatch(addBuyerData(res.data));
        setBuyerInputData({});
      })
      .catch((err) => console.log(err));
  };

  const SubmitOtherDetailsAPI = async () => {
    const ClickedRow = document.getElementById("otherDetalsForm");
    let listOFRequired = [...ClickedRow.getElementsByClassName("required")];
    listOFRequired.forEach((element) => {
      if (element.value == "" && !element.nextElementSibling) {
        const spanTag = document.createElement("span");
        spanTag.classList.add("requiredError");
        const message = document.createTextNode("Required !");
        spanTag.appendChild(message);
        spanTag.style.color = "red";
        spanTag.style.fontSize = "11px"
        element.after(spanTag);
      }
    });

    let ErrorList = [...ClickedRow.getElementsByClassName("requiredError")];

    if (ErrorList.length == 0) {
      await axios({
        url: "http://127.0.0.1:8000/invoice/invoice/",
        method: "post",
        data: {
          Seller_data: sellerData.id,
          Buyer_data: buyerData.id,
          ...otherInputData,
        },
      })
        .then((res) => {
          dispatch(addOtherData(res.data));
          setOtherInputData({});
        })
        .catch((err) => console.log(err));
    }
  };

  const handleChange = (newValue) => {
    if (newValue && typeof newValue.value == "number") {
      const filterData = buyerCompanyList.filter(
        (data) => data.id == newValue.value
      )[0];
      const SelectedValue = {
        buyer_company: newValue.value,
        buyer_name: filterData.ledeger_name,
        buyer_phone: filterData.ledeger_phone,
        buyer_email: filterData.ledeger_email,
        buyer_address: filterData.ledeger_address,
        buyer_state: filterData.ledeger_state,
        buyer_gstin: filterData.ledeger_gstin,
        buyer_website: filterData.ledeger_website,
      };
      setBuyerInputData(SelectedValue);
    } else {
      setBuyerInputData({
        buyer_company: "",
        buyer_name: "",
        buyer_phone: "",
        buyer_email: "",
        buyer_address: "",
        buyer_state: "",
        buyer_gstin: "",
        buyer_website: "",
      });
    }
  };

  const HsnHandleChange = (newValue) => {
    if (newValue != null) {
      const filterData = hsnSacList.filter(
        (data) => data.Hsn_code == newValue.value
      )[0];
      const SelectedValue = {
        ...hsnData,
        ...filterData,
        disc: 0,
      };
      setHsnData(SelectedValue);
      setHsnCode(false);
    } else {
      setHsnData(resetHSN);
      setHsnCode(true);
    }
  };

  const HsnHandleChange2 = (newValue) => {
    if (newValue != null) {
      const filterData = hsnSacList.filter(
        (data) => data.Description == newValue.value
      )[0];
      const SelectedValue = {
        ...hsnData,
        ...filterData,
        disc: 0,
      };
      setHsnData(SelectedValue);
      setProductHsn(false);
    } else {
      setHsnData(resetHSN);
      setProductHsn(true);
    }
  };

  const handleInputChange = (inputValue, actionMeta) => {};

  const AddProductDataToList = async () => {
    if (hsnData.quantity != undefined && hsnData.quantity != "" && hsnData.Rate != undefined ) {
      let InvoiceData = {
        Invoice_data: otherData.id,
        Products: hsnData.Description,
        quantity: hsnData.quantity,
        Discount: hsnData.disc,
        HSN_details: hsnData.id,
        Amount: hsnData.amount,
        CGST: igstShow ? null : hsnData.CGst_rate,
        SGST: igstShow ? null : hsnData.SGst_rate,
        IGST: igstShow ? hsnData.IGst_rate : null,
        Hsn_code: hsnData.Hsn_code,
        Rate: hsnData.Rate,
        Per: hsnData.Per,
      };
      await axios({
        url: "http://127.0.0.1:8000/invoice/invoicedata/",
        method: "post",
        data: InvoiceData,
      })
        .then((res) => {
          dispatch(addProductData(res.data));
          hsnData(resetHSN);
        })
        .catch((err) => console.log(err));
    } else {
      toast.warning(<Warning text="All Filed Required !" />)
    }

  };

  useEffect(() => {
    AmountCalculat();
  }, [hsnData.disc, hsnData.quantity]);

  const AmountCalculat = () => {
    let finalAmount;
    let discVale = hsnData.disc == 0 ? 1 : 1 - hsnData.disc / 100;
    finalAmount = (
      parseInt(hsnData.quantity) *
      parseFloat(hsnData.Rate) *
      parseFloat(discVale)
    ).toFixed(2);

    setHsnData({ ...hsnData, amount: finalAmount });
  };

  let totalCGST = 0;
  let totalSGST = 0;
  let totalIGST = 0;
  let totalAmount = 0;

  productData.forEach((data) => {
    if (data.CGST > 0) {
      totalCGST += parseFloat(data.Amount * (data.CGST / 100));
    }
    if (data.SGST > 0) {
      totalSGST += parseFloat(data.Amount * (data.SGST / 100));
    }
    if (data.IGST > 0) {
      totalIGST += parseFloat(data.Amount * (data.IGST / 100));
    }
    if (data.Amount > 0) {
      totalAmount += parseFloat(data.Amount);
    }
  });

  const SubmitInvoceTotal = async () => {
    var data = new FormData();
    data.append("Total", (totalAmount+totalCGST+totalSGST+totalIGST));
    data.append("Seller_data", sellerData.id);
    data.append("Buyer_data", buyerData.id);
    data.append("GST_Total",((totalCGST+totalSGST+totalIGST).toFixed(2)));

    var config = {
      method: "put",
      url: "http://127.0.0.1:8000/invoice/Invoicetotal/",
      headers: {
        "content-type": "multipart/form-data",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const SubmitInvoice = () => {
    SubmitInvoceTotal();
    handleShowVoucherEntry();
  };


  return (
    <>
      <Container fluid className="rowCol">
        <div className="row">
          <Col xl={6}>
            <Title title="Invoice create" />
            <div className="WhiteShadowCard p-30px mb-3">
              <div className="font-size-18px font-weight-700 text-primary pb-2">
                Seller Detail
              </div>
              <Form className="row" id="sellerForm">
                <Col md={6}>
                  <div className="form-group">
                    <select
                      className="form-control required"
                      name="seller_company"
                      onChange={(e) =>
                        OnInputChange(
                          e.target.name,
                          e.target.value,
                          e.target.nextElementSibling
                        )
                      }
                    >
                      {sellerCompanyList.map((data) => (
                        <option value={data.id}>{data.comp_name}</option>
                      ))}
                    </select>
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control required"
                      type="text"
                      placeholder="Seller Name"
                      name="seller_name"
                      onChange={(e) =>
                        OnInputChange(
                          e.target.name,
                          e.target.value,
                          e.target.nextElementSibling
                        )
                      }
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Seller Phone"
                      name="seller_phone"
                      onChange={(e) =>
                        OnInputChange(
                          e.target.name,
                          e.target.value,
                          e.target.nextElementSibling
                        )
                      }
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control required"
                      type="text"
                      placeholder="Seller Email"
                      name="seller_email"
                      onChange={(e) =>
                        OnInputChange(
                          e.target.name,
                          e.target.value,
                          e.target.nextElementSibling
                        )
                      }
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <textarea
                      rows="1"
                      className="form-control required"
                      placeholder="Seller Address"
                      name="seller_address"
                      onChange={(e) =>
                        OnInputChange(
                          e.target.name,
                          e.target.value,
                          e.target.nextElementSibling
                        )
                      }
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Seller Website"
                      name="seller_website"
                      onChange={(e) =>
                        OnInputChange(
                          e.target.name,
                          e.target.value,
                          e.target.nextElementSibling
                        )
                      }
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control required"
                      type="text"
                      placeholder="Seller State"
                      name="seller_state"
                      onChange={(e) =>
                        OnInputChange(
                          e.target.name,
                          e.target.value,
                          e.target.nextElementSibling
                        )
                      }
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control required"
                      type="text"
                      placeholder="Seller GSTIN"
                      name="seller_gstin"
                      onChange={(e) =>
                        OnInputChange(
                          e.target.name,
                          e.target.value,
                          e.target.nextElementSibling
                        )
                      }
                    />
                  </div>
                </Col>
                <Col md={12} className="text-right">
                  <Button variant="primary" onClick={SubmitSellerDataAPI}>
                    OK
                  </Button>
                </Col>
              </Form>
            </div>
            <div className="WhiteShadowCard p-30px mb-3">
              <div className="font-size-18px font-weight-700 text-primary pb-2">
                Buyer Detail
              </div>
              <Form className="row">
                <Col md={6}>
                  <div className="form-group">
                    <Select
                      className="searchBox"
                      isClearable
                      onChange={handleChange}
                      onInputChange={handleInputChange}
                      options={options}
                      placeholder="Buyer Company"
                      components={{
                        DropdownIndicator: () => null,
                        IndicatorSeparator: () => null,
                      }}
                    />

                    {/* <input
                      className="form-control"
                      type="text"
                      placeholder="Buyer Company"
                      name="buyer_company"
                      onChange={(e) => SearchFunction(e.target.value)}
                    /> */}
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Buyer Name"
                      name="buyer_name"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                      value={buyerInputData.buyer_name}
                      disabled
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Buyer Phone"
                      name="buyer_phone"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                      value={buyerInputData.buyer_phone}
                      disabled
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Buyer Email"
                      name="buyer_email"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                      value={buyerInputData.buyer_email}
                      disabled
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <textarea
                      rows="1"
                      className="form-control"
                      placeholder="Buyer Address"
                      name="buyer_address"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                      value={buyerInputData.buyer_address}
                      disabled
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Buyer Website"
                      name="buyer_website"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                      value={buyerInputData.buyer_website}
                      disabled
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Buyer State"
                      name="buyer_state"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                      value={buyerInputData.buyer_state}
                      disabled
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Buyer GSTIN"
                      name="buyer_gstin"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                      value={buyerInputData.buyer_gstin}
                      disabled
                    />
                  </div>
                </Col>
                <Col md={12} className="text-right">
                  <Button variant="primary" onClick={SubmitBuyerDataAPI}>
                    OK
                  </Button>
                </Col>
              </Form>
            </div>
            <div className="WhiteShadowCard p-30px mb-3">
              <div className="font-size-18px font-weight-700 text-primary pb-2">
                Other Details
              </div>
              <Form className="row" id="otherDetalsForm">
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control required"
                      type="text"
                      placeholder="Invoice No"
                      name="Invoice_no"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value, e.target.nextElementSibling)
                      }
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <DatePicker
                      selected={
                        !otherInputData.Invoice_date
                          ? new Date()
                          : new Date(otherInputData.Invoice_date)
                      }
                      className="form-control required"
                      name="Invoice_date"
                      onChange={(date) =>
                        OnInputChange(
                          "Invoice_date",
                          moment(date).format("YYYY-MM-DD")
                        )
                      }
                      dateFormat="dd/MM/yyyy"
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="PO Number"
                      name="P_O_no"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <DatePicker
                      selected={
                        otherInputData.P_O_date != undefined
                          ? new Date(otherInputData.P_O_date)
                          : ""
                      }
                      className="form-control"
                      name="P_O_date"
                      onChange={(date) =>
                        OnInputChange(
                          "P_O_date",
                          moment(date).format("YYYY-MM-DD")
                        )
                      }
                      placeholderText="PO Date"
                      dateFormat="dd/MM/yyyy"
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Terms of Payment"
                      name="Terms_of_payment"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <textarea
                      rows="1"
                      className="form-control"
                      placeholder="Reference No"
                      name="Reference_no"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                    />
                  </div>
                </Col>
                <Col md={12}>
                  <div className="form-group">
                    <textarea
                      className="form-control"
                      type="text"
                      placeholder="Delivery Note"
                      name="Delievry_note"
                      onChange={(e) =>
                        OnInputChange(e.target.name, e.target.value)
                      }
                    />
                  </div>
                </Col>
                <Col md={12} className="text-right">
                  <Button variant="primary" onClick={SubmitOtherDetailsAPI}>
                    OK
                  </Button>
                </Col>
              </Form>
            </div>
            <div className="WhiteShadowCard p-30px mb-3">
              <div className="font-size-18px font-weight-700 text-primary pb-2">
                Other Detail
              </div>
              <Table responsive className="border-0 tdPadding">
                <thead>
                  <tr>
                    <th width="170px">Product</th>
                    <th width="50px">HSN/SAC</th>
                    {!igstShow && <th width="40px">CGst</th>}
                    {!igstShow && <th width="40px">SGST</th>}
                    {igstShow && <th width="40px">IGST</th>}
                    <th width="50px">Quantity</th>
                    <th width="30px">Rate</th>
                    <th width="30px">Per</th>
                    <th width="30px">Disc</th>
                    <th width="30px">Amount</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      {hsnCode && (
                        <Select
                          className="searchBox"
                          isClearable
                          onChange={HsnHandleChange2}
                          onInputChange={handleInputChange}
                          options={hsnProductOption}
                          placeholder="Product"
                          components={{
                            DropdownIndicator: () => null,
                            IndicatorSeparator: () => null,
                          }}
                        />
                      )}
                      {!hsnCode && (
                        <input
                          className="form-control"
                          type="text"
                          placeholder="Product"
                          name="Description"
                          // onChange={(e) =>
                          //   setHsnData({
                          //     ...hsnData,
                          //     [e.target.name]: e.target.value,
                          //   })
                          // }
                          disabled
                          value={hsnData.Description}
                        />
                      )}
                    </td>
                    {productHsn && (
                      <td>
                        <Select
                          className="searchBox"
                          isClearable
                          onChange={HsnHandleChange}
                          onInputChange={handleInputChange}
                          options={hsnOptions}
                          placeholder="HSN"
                          components={{
                            DropdownIndicator: () => null,
                            IndicatorSeparator: () => null,
                          }}
                        />
                      </td>
                    )}
                    {!productHsn && (
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          // placeholder="CGST"
                          disabled
                          value={hsnData.Hsn_code}
                        />
                      </td>
                    )}
                    {!igstShow && (
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          // placeholder="CGST"
                          disabled
                          value={hsnData.CGst_rate}
                        />
                      </td>
                    )}
                    {!igstShow && (
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          // placeholder="SGST"
                          disabled
                          value={hsnData.SGst_rate}
                        />
                      </td>
                    )}
                    {igstShow && (
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          // placeholder="IGST"
                          disabled
                          value={hsnData.IGst_rate}
                        />
                      </td>
                    )}
                    <td>
                      <input
                        className="form-control"
                        type="number"
                        // placeholder="Quantity"
                        value={hsnData.quantity}
                        name="quantity"
                        onChange={(e) => {
                          setHsnData({
                            ...hsnData,
                            [e.target.name]: e.target.value,
                          });
                        }}
                      />
                    </td>
                    <td className="pr-0">
                      <input
                        className="form-control"
                        type="text"
                        // placeholder="Rate"
                        disabled
                        value={hsnData.Rate}
                      />
                    </td>
                    <td className="pr-0">
                      <input
                        className="form-control"
                        type="text"
                        // placeholder="Per"
                        disabled
                        value={hsnData.Per}
                      />
                    </td>
                    <td className="pr-0">
                      <input
                        className="form-control"
                        type="text"
                        // placeholder="Per"
                        name="disc"
                        value={hsnData.disc}
                        onChange={(e) => {
                          setHsnData({
                            ...hsnData,
                            [e.target.name]: e.target.value,
                          });
                        }}
                      />
                    </td>
                    <td className="pr-0">
                      <input
                        className="form-control"
                        type="text"
                        // placeholder="Per"
                        name="amount"
                        value={isNaN(hsnData.amount) ? "" : hsnData.amount}
                        disabled
                      />
                    </td>
                  </tr>
                </tbody>
              </Table>

              <Col md={12} className="text-right">
                <Button variant="primary" onClick={AddProductDataToList}>
                  OK
                </Button>
              </Col>
            </div>
            <div className="WhiteShadowCard p-30px mb-3">
              <div className="font-size-18px font-weight-700 text-primary pb-2">
                Other Detail
              </div>
              <Table responsive className="border-0 tdPadding">
                <thead>
                  <tr>
                    <th width="170px">Product</th>
                    <th width="50px">HSN/SAC</th>
                    {!igstShow && <th width="40px">CGst</th>}
                    {!igstShow && <th width="40px">SGST</th>}
                    {igstShow && <th width="40px">IGST</th>}
                    <th width="50px">Quantity</th>
                    <th width="30px">Rate</th>
                    <th width="30px">Per</th>
                    <th width="30px">Disc</th>
                    <th width="30px">Amount</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      {hsnCode && (
                        <Select
                          className="searchBox"
                          isClearable
                          onChange={HsnHandleChange2}
                          onInputChange={handleInputChange}
                          options={hsnProductOption}
                          placeholder="Product"
                          components={{
                            DropdownIndicator: () => null,
                            IndicatorSeparator: () => null,
                          }}
                        />
                      )}
                      {!hsnCode && (
                        <input
                          className="form-control"
                          type="text"
                          placeholder="Product"
                          name="Description"
                          // onChange={(e) =>
                          //   setHsnData({
                          //     ...hsnData,
                          //     [e.target.name]: e.target.value,
                          //   })
                          // }
                          disabled
                          value={hsnData.Description}
                        />
                      )}
                    </td>
                    {productHsn && (
                      <td>
                        <Select
                          className="searchBox"
                          isClearable
                          onChange={HsnHandleChange}
                          onInputChange={handleInputChange}
                          options={hsnOptions}
                          placeholder="HSN"
                          components={{
                            DropdownIndicator: () => null,
                            IndicatorSeparator: () => null,
                          }}
                        />
                      </td>
                    )}
                    {!productHsn && (
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          // placeholder="CGST"
                          disabled
                          value={hsnData.Hsn_code}
                        />
                      </td>
                    )}
                    {!igstShow && (
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          // placeholder="CGST"
                          disabled
                          value={hsnData.CGst_rate}
                        />
                      </td>
                    )}
                    {!igstShow && (
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          // placeholder="SGST"
                          disabled
                          value={hsnData.SGst_rate}
                        />
                      </td>
                    )}
                    {igstShow && (
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          // placeholder="IGST"
                          disabled
                          value={hsnData.IGst_rate}
                        />
                      </td>
                    )}
                    <td>
                      <input
                        className="form-control"
                        type="number"
                        // placeholder="Quantity"
                        value={hsnData.quantity}
                        name="quantity"
                        onChange={(e) => {
                          setHsnData({
                            ...hsnData,
                            [e.target.name]: e.target.value,
                          });
                        }}
                      />
                    </td>
                    <td className="pr-0">
                      <input
                        className="form-control"
                        type="text"
                        // placeholder="Rate"
                        disabled
                        value={hsnData.Rate}
                      />
                    </td>
                    <td className="pr-0">
                      <input
                        className="form-control"
                        type="text"
                        // placeholder="Per"
                        disabled
                        value={hsnData.Per}
                      />
                    </td>
                    <td className="pr-0">
                      <input
                        className="form-control"
                        type="text"
                        // placeholder="Per"
                        name="disc"
                        value={hsnData.disc}
                        onChange={(e) => {
                          setHsnData({
                            ...hsnData,
                            [e.target.name]: e.target.value,
                          });
                        }}
                      />
                    </td>
                    <td className="pr-0">
                      <input
                        className="form-control"
                        type="text"
                        // placeholder="Per"
                        name="amount"
                        value={isNaN(hsnData.amount) ? "" : hsnData.amount}
                        disabled
                      />
                    </td>
                  </tr>
                </tbody>
              </Table>

              <Col md={12} className="text-right">
                <Button variant="primary" onClick={AddProductDataToList}>
                  OK
                </Button>
              </Col>
            </div>
          </Col>
          <Col xl={6}>
            <div className="d-flex flex-wrap justify-content-between">
              <div>
                <Title title="Invoice create" />
              </div>
              <div>
                <Button
                  type="button"
                  variant="primary"
                  className="mr-2"
                  onClick={handlePrint}
                >
                  Print
                </Button>
                <Button variant="primary" onClick={SubmitInvoice}>
                  Submit Invoice
                </Button>
              </div>
            </div>
            <div className="WhiteShadowCard p-30px mb-3 position-stiky">
              <InvoicePreview ref={componentRef} />
            </div>
          </Col>
        </div>
      </Container>
      <NewVoucherEntry
        show={ShowVoucherEntry}
        handleClose={handleHideVoucherEntry}
        sellerCompanyList={sellerCompanyList}
        TotalAmount={(
          parseFloat(totalIGST) +
          parseFloat(totalCGST) +
          parseFloat(totalSGST) +
          parseFloat(totalAmount)
        ).toFixed(2)}
      />
    </>
  );
};
export default InvoiceCreate;
