import React from "react";
import { Row, Col, Button } from "react-bootstrap";

const InvoiceContainer = (props) => {
  const {
    subtotal,
    companyname,
    invoice_no,
    invoice_date,
    bank_details,
    CGST,
    SGST,
    IGST,
    csvreceipt,
    payment_mode,
  } = props.invoiceResposne;

  // const companyName = bank_details.replace('/n', '').trim().toString()
  // const AcNo = parseInt(companyName.replaceAll('\n',',').split(',')[3].split(':')[1].trim())
  // const ifc_code = companyName.replaceAll('\n',',').split(',')[5].split(':')[1].trim()
  // const bankName = companyName.replaceAll('\n',',').split(',')[2].split(':')[1].trim()

  return (
    <>
      <div className="rowCol">
        <div className="form-group">
          <label className="font-size-12px pl-2">Customer Name</label>
          <input className="form-control" type="text" value={companyname} />
        </div>
        <Row>
          <Col md={3} className="">
            <div className="form-group">
              <label className="font-size-12px pl-2">Invoice Number</label>
              <input className="form-control" type="text" value={invoice_no} />
            </div>
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">Invoice Date</label>
              <input
                className="form-control"
                type="text"
                value={invoice_date}
              />
            </div>
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">Payment Terms</label>
              <input
                className="form-control"
                type="text"
                value={payment_mode}
              />
            </div>
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">Supplier Ref No.</label>
              <input className="form-control" type="text" />
            </div>
          </Col>
        </Row>
        {/*----------------------*/}
        {csvreceipt.map((csv) => (
          <Row>
            <Col md={3}>
              <div className="form-group">
                <label className="font-size-12px pl-2">Goods Detail</label>
                <input
                  className="form-control"
                  type="text"
                  value={csv.Products}
                />
              </div>
            </Col>
            <Col md={2}>
              <Row>
                <Col md={6}>
                  <div className="form-group">
                    <label className="font-size-12px pl-2">HSN</label>
                    <input
                      className="form-control"
                      type="text"
                      value={csv.HSN_SAC}
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <label className="font-size-12px pl-2">Quantity</label>
                    <input
                      className="form-control"
                      type="text"
                      value={csv.quantity}
                    />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col md={3}>
              <Row>
                <Col md={6}>
                  <div className="form-group">
                    <label className="font-size-12px pl-2">Rate</label>
                    <input
                      className="form-control"
                      type="text"
                      value={csv.Rate}
                    />
                  </div>
                </Col>
                <Col md={4}>
                  <div className="form-group">
                    <label className="font-size-12px pl-2">Unit</label>
                    <input
                      className="form-control"
                      type="text"
                      value={csv.Per}
                    />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col md={1}>
              <div className="form-group">
                <label className="font-size-12px pl-2">Discount</label>
                <input
                  className="form-control"
                  type="text"
                  value={csv.Discount}
                />
              </div>
            </Col>
            <Col md={3}>
              <Row>
                <Col md={6}>
                  <div className="form-group">
                    <label className="font-size-12px pl-2">GST Rate</label>
                    <input
                      className="form-control"
                      type="text"
                      value={csv.GST_rate}
                    />
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group">
                    <label className="font-size-12px pl-2">Amount</label>
                    <input
                      className="form-control"
                      type="text"
                      value={csv.Amount}
                    />
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        ))}

        {/*----------------------*/}
        <hr />
        <Row>
          <Col md={6}>
            <Row>
              <Col md={6}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">Total</label>
                  {/* <input className="form-control" type="text" value={parseFloat(props.invoiceResposne.subtotal)}/> */}
                  <input
                    className="form-control"
                    type="text"
                    value={subtotal}
                  />
                </div>
              </Col>
            </Row>
          </Col>
          <Col md={6}>
            <Row>
              <Col md={4}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">CGST</label>
                  <input className="form-control" type="text" value={CGST} />
                </div>
              </Col>
              {/* <Col md={4}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">Rate</label>
                  <input className="form-control" type="text" />
                </div>
              </Col> */}
              <Col md={5}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">Amount</label>
                  <input className="form-control" type="text" value={CGST} />
                </div>
              </Col>
            </Row>
            <Row>
              <Col md={4}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">SGST</label>
                  <input className="form-control" type="text" value={SGST} />
                </div>
              </Col>
              {/* <Col md={4}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">Rate</label>
                  <input className="form-control" type="text" />
                </div>
              </Col> */}
              <Col md={5}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">Amount</label>
                  <input className="form-control" type="text" value={CGST} />
                </div>
              </Col>
            </Row>
            <Row>
              <Col md={4}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">IGST</label>
                  <input className="form-control" type="text" value={IGST} />
                </div>
              </Col>
              {/* <Col md={4}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">Rate</label>
                  <input className="form-control" type="text" />
                </div>
              </Col> */}
              <Col md={5}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">Amount</label>
                  <input className="form-control" type="text" value={CGST} />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        <hr />
        <div className="text-primary pb-3 font-size-md-18px font-size-16px font-weight-700">
          Company Bank Detail
        </div>
        <Row>
          <Col md={5}>
            <div className="form-group">
              <label className="font-size-12px pl-2">Bank Name</label>
              <input
                className="form-control"
                type="text"
                value={bank_details}
              />
            </div>
          </Col>
          <Col md={4}>
            <div className="form-group">
              <label className="font-size-12px pl-2">Account Number</label>
              <input
                className="form-control"
                type="text"
                value={bank_details}
              />
            </div>
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">IFSC Code</label>
              <input
                className="form-control"
                type="text"
                value={bank_details}
              />
            </div>
          </Col>
        </Row>
        {props.import && (
          <div className="d-flex align-items-start justify-content-center pt-2">
            <Button variant="primary" className="px-5">
              OK
            </Button>
          </div>
        )}
        {props.report && (
          <div className="d-flex flex-wrap align-items-start justify-content-center pt-2">
            <Button variant="primary" className="px-5 mr-2 mb-2">
              Edit Data
            </Button>
            <button
              type="button"
              className="font-size-18px delete-btn w-auto py-1 h-auto mb-2 px-5"
            >
              Delete Data
            </button>
          </div>
        )}
      </div>
    </>
  );
};

export default InvoiceContainer;
