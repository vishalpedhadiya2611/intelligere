import React, { useState, useEffect, useRef } from "react";
import { Button, Row, Col, Table } from "react-bootstrap";
import InvoiceContainer from "./InvoiceContainer";
import DatePicker from "react-datepicker";
import axios from "axios";

import "react-datepicker/dist/react-datepicker.css";
import { CompanyListAPI } from "../BankStatements/Api";
function RecieveInvoiceReport(props) {
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [companyList, setCompanyList] = useState([]);
  const [viewId, setViewId] = useState(-1);
  const companyEL = useRef(null);
  const selectRangeFrom = useRef(null);
  const selectRangeTo = useRef(null);
  const [recieveInvoiceData, setRecieveInvoiceData] = useState({
    details: [],
    type: "recieveInvoice",
  });
  useEffect(async () => {
    // let companyListData = [];
    let companyListData = await CompanyListAPI()
    setCompanyList(companyListData);
  }, []);
  const handleRecieveInvoiceOkbutton = () => {
    let companyID = companyEL.current.value;
    let SelectRangeFrom = selectRangeFrom.current.value;
    let SelectRangeTo = selectRangeTo.current.value;
    let url1 = `http://127.0.0.1:8000/invoice/receiptinvoicereport/?Buyer_data=${companyID}&Total_min=${SelectRangeFrom}&Total_max=${SelectRangeTo}&Invoice_date_after=${startDate}&Invoice_date_before=${endDate}`;
    let url2 = "http://127.0.0.1:8000/invoice/receiptinvoicereport/";
    axios({
      method: "get",
      url: url2,
      headers: {
        "content-type": "application/json",
      },
      data: {
        Buyer_data: companyID,
        Total_min: SelectRangeFrom,
        Total_max: SelectRangeTo,
        Invoice_date_after: startDate,
        Invoice_date_before: endDate,
      },
      params:{
        Company: companyID,
        subtotal_min: SelectRangeFrom,
        subtotal_max: SelectRangeTo,
        invoice_date_after: startDate,
        invoice_date_before: endDate

      }
    })
      .then((response) => {
        setRecieveInvoiceData({
          ...recieveInvoiceData,
          details: response.data,
        });
      })
      .catch((error) => {
        setRecieveInvoiceData({
          ...recieveInvoiceData,
          details: [
            {
              id: 54,
              file: "http://www.africau.edu/images/default/sample.pdf",
              companyname: "Raj Electronics (18-19)",
              Company: 1,
              invoice_no: "Invoice No.RA.I/20-21/0617",
              subtotal: 100,
              invoice_date: "2021-10-10",
              payment_mode: null,
              CGST: "SGSTCGST          247.50247.50",
              SGST: "SGSTCGST          247.50247.50",
              IGST: "SGSTCGST          247.50247.50",
              bank_details:
                "Company’s Bank DetailsBank Name : KALUPUR COMMERCIAL CO.OP. BANKA/c No. : 01336100406Branch &IFS Code : PANCHVATI & KCCBOPCV013",
              company_details:
                "Ra Electron ics (18 9AbmedabadGSTIN/UIN: 24alHnPSs2276H1Z3State Name = Gujarat, Gode -24Gontact 079 ~ 26461690 / 2646191 1,9825269099E-Mail = sundip@rajelectronics.com",
              csvreceipt: [
                {
                  id: 1,
                  Products: "Machinewry",
                  HSN_SAC: "402",
                  GST_rate: 4,
                  quantity: 4,
                  Rate: "100",
                  Per: null,
                  Discount: null,
                  Amount: "1000",
                  Invoice_data: 54,
                },
              ],
            },
          ],
        });
      });
  };
  return (
    <>
      <div className="WhiteShadowCard p-30px mb-3">
        <div className="form-group">
          <label className="font-size-12px pl-2">Report Type</label>
          <select
            value={props.ReportType}
            className="form-control"
            onChange={props.handleReportType}
          >
            <option value="create">Create Invoice</option>
            <option value="recieve">Recieve Invoice</option>
          </select>
        </div>
        <div className="form-group">
          <label className="font-size-12px pl-2">Select a Company</label>

          <select className="form-control" ref={companyEL}>
            <option value="none" selected disabled>
              Select a Company
            </option>
            {companyList.map((res) => (
              <option key={res.id} value={res.id}>
                {res.comp_name}
              </option>
            ))}
          </select>
        </div>
        <Row>
          <Col lg={6}>
            <label className="font-size-12px pl-2">Select Date</label>
            <div className="row">
              <Col className="col-md-6">
                <div className="form-group">
                  <DatePicker
                    selected={startDate}
                    onChange={(date) => setStartDate(date)}
                    selectsStart
                    className="form-control"
                    startDate={startDate}
                    endDate={endDate}
                    placeholderText="From Date"
                  />
                </div>
              </Col>
              <Col className="col-md-6">
                <div className="form-group">
                  <DatePicker
                    selected={endDate}
                    onChange={(date) => setEndDate(date)}
                    selectsEnd
                    className="form-control"
                    startDate={startDate}
                    endDate={endDate}
                    minDate={startDate}
                    placeholderText="To Date"
                  />
                </div>
              </Col>
            </div>
          </Col>
          <Col lg={6}>
            <label className="font-size-12px pl-2">Select Range</label>
            <div className="row">
              <Col className="col-md-6">
                <div className="form-group">
                  <input
                    ref={selectRangeFrom}
                    type="text"
                    className="form-control"
                    placeholder="From Range"
                  />
                </div>
              </Col>
              <Col className="col-md-6">
                <div className="form-group">
                  <input
                    ref={selectRangeTo}
                    type="text"
                    className="form-control"
                    placeholder="To Range"
                  />
                </div>
              </Col>
            </div>
          </Col>
        </Row>
        <Button
          variant="primary"
          onClick={() => {
            handleRecieveInvoiceOkbutton();
          }}
        >
          OK
        </Button>
      </div>
      {recieveInvoiceData.details.length > 0 && (
        <div className="WhiteShadowCard p-30px mb-3">
          <Table responsive hover>
            <thead>
              <tr>
                <th>SR No.</th>
                <th>Customer Name</th>
                <th>Invoice Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>             
              {recieveInvoiceData.details.map((data, index) => {
                return (
                  <tr>
                    <td>{(index+1)}</td>
                    <td>{data.companyname}</td>
                    <td>{data.invoice_date}</td>
                    <td className="pl-0">
                      <button className="ok-btn ml-2" onClick={()=>{setViewId(index); props.handelFile(recieveInvoiceData.details.filter((item) => item.id == data.id)[0].file)}}>View</button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </div>
      )}
      {viewId != -1 && (
        <div className="WhiteShadowCard p-30px mb-3 mb-xl-0">
          <InvoiceContainer report={true} invoiceResposne={recieveInvoiceData.details[viewId]} />
        </div>
      )}
    </>
  );
}

export default RecieveInvoiceReport;
