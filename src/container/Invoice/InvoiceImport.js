import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import PdfContainer from "./PdfContainer";
import InvoiceContainer from "./InvoiceContainer";
import axios from "axios";
import { Title } from "../../components/Texonomy";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import { NewVoucherEntry } from "../../components/Modal/NewVoucherEntry";
import { NewVoucherForInvoiceImport } from "../../components/Modal/NewVoucherForInvoiceImport";

const InvoiceImport = () => {
  const defaultInvoiceResponse = {
    id: 54,
    companyname: "Raj Electronics (18-19)",
    file: "http://www.africau.edu/images/default/sample.pdf",
    invoice_no: "Invoice No.RA.I/20-21/0617",
    subtotal: 100,
    invoice_date: "2021-10-10",
    payment_mode: null,
    CGST: "SGSTCGST          247.50247.50",
    SGST: "SGSTCGST          247.50247.50",
    IGST: "SGSTCGST          247.50247.50",
    bank_details: "Company’s Bank DetailsBank Name : KALUPUR COMMERCIAL CO.OP. BANKA/c No. : 01336100406Branch &IFS Code : PANCHVATI & KCCBOPCV013",
    company_details: "Ra Electron ics (18 9AbmedabadGSTIN/UIN: 24alHnPSs2276H1Z3State Name = Gujarat, Gode -24Gontact 079 ~ 26461690 / 2646191 1,9825269099E-Mail = sundip@rajelectronics.com",
    legdername: null,
    legderdata: "PURCHASE",
    GSTlegderdata: null,
    GSTlegderamount: null,
    Narration: "INVOICEInvoice No.RA.I/20-21/0617\fto your company.",
    csvreceipt: [
        {
            id: 1,
            Products: "Machinewry",
            HSN_SAC: "402",
            GST_rate: 4,
            quantity: 4,
            Rate: "100",
            Amount: "1000",
            Total: "10000",
            CGST: 2,
            SGST: 2,
            IGST: 2,
            Invoice_data: 54
        },
        {
          id: 2,
          Products: "Machinewry",
          HSN_SAC: "402",
          GST_rate: 4,
          quantity: 4,
          Rate: "100",
          Amount: "1000",
          Total: "10000",
          CGST: 2,
          SGST: 2,
          IGST: 2,
          Invoice_data: 54
      },
      {
        id: 3,
        Products: "Machinewry",
        HSN_SAC: "402",
        GST_rate: 4,
        quantity: 4,
        Rate: "100",
        Amount: "1000",
        Total: "10000",
        CGST: 2,
        SGST: 2,
        IGST: 2,
        Invoice_data: 54
    }
    ]
}

  const [bnkCsv, setbnkCsv] = useState(null);
  const [companyNameList, setCompanyNameList] = useState([{
    id: "1",
    comp_name: "DDS"
  }]);
  const [showVoucherEntry, setShowVoucherEntry] = useState(false);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [invoiceResposne, setInvoiceResposne] = useState(
    {}
  );

  const [selectedCompany, setSelectedCompany] = useState(0)
  var bodyFormData = new FormData();
  bodyFormData.append("file", bnkCsv);
  //   for (var pair of bodyFormData.entries()) {
  //     pair[0] + ", " + pair[1]);
  //   }

  useEffect(() => {
    axios({
      method: "get",
      // url: "https://bank-statement.herokuapp.com/e_payment/CompanyViews",
      url: "http://127.0.0.1:8000/invoice/companyshow/",
      headers: {
        "content-type": "application/json",
      },
    })
      .then(function (response) {
        //handele success
        setCompanyNameList(response.data);
      })
      .catch(function (error) {
        //handel error
        console.log("post api---", error);
      });
  }, []);

  

  const handelSubmit = async () => {
    let formdata = new FormData();
    formdata.append("file", bnkCsv);
    formdata.append("Company", selectedCompany);


    await axios(
      {
        method: "post",
        url: "http://127.0.0.1:8000/invoice/csvinvoice/",
        headers: {
          "content-type": "multipart/form-data",
        },
        data: formdata,
      })
        .then(async(res) => {
          axios({
            method: "get",
            url: "http://127.0.0.1:8000/invoice/csvinvoice/"
          })
          .then(res2 => 
            {
              setInvoiceResposne(res2.data)})
          .catch(err => console.log(err))
        })
        .catch((err) => {
          console.log(err);
        })
    
  };

  return (
    <>
      <Container fluid className="rowCol">
        <Title title="Invoice" />
        <Row>
          <Col xl={6} className="w-1366">
            <div className="WhiteShadowCard p-30px mb-3">
              <Row>
                <Col lg={12}>
                  <div className="form-group">
                    <select className="form-control"  onChange={e => setSelectedCompany(e.target.value)}>
                      <option value="none" selected disabled>
                        Select a Company
                      </option>
                      {companyNameList.map((companyList) => (
                        <option key={companyList.id} value={companyList.id}>
                          {companyList.comp_name}
                        </option>
                      ))}
                    </select>
                  </div>
                </Col>
                {/* <Col lg={6}>
                  <div className="row">
                    <Col className="col-md-6">
                      <div className="form-group">
                        <DatePicker
                          selected={startDate}
                          onChange={(date) => setStartDate(date)}
                          selectsStart
                          className="form-control"
                          startDate={startDate}
                          endDate={endDate}
                          placeholderText="From Date"
                        />
                      </div>
                    </Col>
                    <Col className="col-md-6">
                      <div className="form-group">
                        <DatePicker
                          selected={endDate}
                          onChange={(date) => setEndDate(date)}
                          selectsEnd
                          className="form-control"
                          startDate={startDate}
                          endDate={endDate}
                          minDate={startDate}
                          placeholderText="To Date"
                        />
                      </div>
                    </Col>
                  </div>
                </Col> */}

                {/* <Col lg={2}>
                  <Button
                    variant="primary"
                    onClick={() => setShowVoucherEntry(!showVoucherEntry)}
                  >
                    OK
                  </Button>
                </Col> */}
              </Row>
            </div>
            <div className="WhiteShadowCard p-30px mb-3 mb-xl-0">
              <InvoiceContainer
                import={true}
                invoiceResposne={invoiceResposne}
                okayBTN={true} okBTn={true}
              />
            </div>
          </Col>
          <Col xl={6} className="w-1366">
            <div className="WhiteShadowCard h-100 mb-3">
              <div className="border-bottom p-30px pb-0">
                <Row>
                  <Col
                    md={12}
                    className="d-flex flex-wrap align-items-center justify-content-md-end"
                  >
                   { !!selectedCompany && <div className="pb-3 ">
                      <div className="fileInput font-size-14px">
                        Select File
                        <input
                          type="file"
                          onChange={(e) => setbnkCsv(e.target.files[0])}
                        />
                      </div>
                    </div>}
                    {bnkCsv && <div className="pb-3 pl-md-3">
                      <Button variant="primary" onClick={handelSubmit}>
                        Submit File
                      </Button>
                    </div>}
                  </Col>
                </Row>
              </div>
              {/* <PdfContainer SrcFilePath={invoiceResposne.file}/> */}
              {(invoiceResposne.file != undefined && bnkCsv != null)? <PdfContainer  SrcFilePath={invoiceResposne.file}/> : <div className="pdfView">
              <div className="opacity-50 font-size-20px text-primary">Image Preview</div>
              </div>}
            </div>
          </Col>
        </Row>
      </Container>
      {/* <NewVoucherEntry
        show={showVoucherEntry}
        data={invoiceResposne}
        handleClose={() => setShowVoucherEntry(!showVoucherEntry)}
      />
      <NewVoucherForInvoiceImport /> */}
    </>
  );
};

export default InvoiceImport;
