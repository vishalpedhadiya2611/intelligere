import React, { useEffect, useState } from "react";
import { Container, Row, Col } from 'react-bootstrap';
import PdfContainer from './PdfContainer';
import { Title } from "../../components/Texonomy";
import CreateInvoiceReport from "./CreateInvoiceReport";

import "react-datepicker/dist/react-datepicker.css";
import RecieveInvoiceReport from "./RecieveInvoiceReport";
function InvoiceReport() {
    const [pdfFile, setPdfFile] = useState("")
    const [ReportType, setReportTypee] = useState("create");
    const handleReportType = (e) => {
        setReportTypee(e.target.value)
    }


return (<>

        <Container fluid className="rowCol">
            <Title title="Invoice Report" />
            <Row>
                <Col xl={6} className="w-1366">
                    {ReportType === "create" ? <CreateInvoiceReport ReportType={ReportType} handleReportType={handleReportType} handelFile={setPdfFile}/> :
                        <RecieveInvoiceReport ReportType={ReportType} handleReportType={handleReportType}  handelFile={setPdfFile}/>}
                </Col>
                <Col xl={6} className="w-1366">
                    <div className="WhiteShadowCard h-100">
                    {(pdfFile != "")? <PdfContainer SrcFilePath={pdfFile}/> : <div className="pdfView">
              <div className="opacity-50 font-size-20px text-primary">Image Preview</div>
              </div>}
                    </div>
                </Col>
            </Row>
        </Container>
    </>

    );
}

export default InvoiceReport;
