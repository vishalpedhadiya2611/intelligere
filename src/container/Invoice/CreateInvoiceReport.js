import React, { useState, useEffect, useRef } from "react";
import { Button, Row, Col, Table } from "react-bootstrap";
import InvoiceContainer from "./InvoiceContainer";
import axios from "axios";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { BuyerCompanyListAPI } from "../BankStatements/Api";
const CreateInvoiceReport = (props) => {
  //const compnayEL = useRef(null);
  const buyerEL = useRef(null);
  const selectRangeFrom = useRef(null);
  const selectRangeTo = useRef(null);

  const [startDate, setStartDate] = useState("");
  const [viewId, setViewId] = useState(0);
  const [endDate, setEndDate] = useState("");
  const [companyList, setCompanyList] = useState([]);

  useEffect(async () => {
    // let companyListData = [];
    const companyListData = await BuyerCompanyListAPI()
    setCompanyList(companyListData);
  }, []);

  const [createInvoiceData, setCreateInvoiceData] = useState({
    details: [],
    type: "createInvoice",
  });


  const handleCreateInvoiceOkbutton = () => {
    let buyerID = buyerEL.current.value;
    let SelectRangeFrom = selectRangeFrom.current.value;
    let SelectRangeTo = selectRangeTo.current.value;
    let url1 = `http://127.0.0.1:8000/invoice/createinvoicereport/?Buyer_data=${buyerID}&Total_min=${SelectRangeFrom}&Total_max=${SelectRangeTo}&Invoice_date_after=${startDate}&Invoice_date_before=${endDate}`;
    // let url2 = "http://127.0.0.1:8000/invoice/createinvoicereport/";
    axios({
      method: "get",
      url: url1,
      headers: {
        "content-type": "application/json",
      },
      data: {
        Buyer_data: buyerID,
        Total_min: SelectRangeFrom,
        Total_max: SelectRangeTo,
        Invoice_date_after: startDate,
        Invoice_date_before: endDate,
      },
    })
      .then((response) => {
          setCreateInvoiceData({
          ...createInvoiceData,
          details: response.data,
        });
      })
      .catch((error) => {
        var response={};
        response.data= [
            {
              id: 1,
              Buyer_data: "DDS",
              Seller_data: 1,
              Invoice_no: "001",
              Invoice_date: "2021-09-21",
              P_O_no: "1",
              P_O_date: "2020-02-02",
              Total: 200,
              Terms_of_payment: "CASH",
              Reference_no: "213453221",
              Delievry_note: "fdsfdfd",
              file: "http://www.africau.edu/images/default/sample.pdf",
              invoicedatas: [
                {
                  id: 1,
                  Products: "Machinery",
                  quantity: 1,
                  Discount: "10.00",
                  Amount: null,
                  CGST: null,
                  SGST: null,
                  IGST: null,
                  Hsn_code: null,
                  Rate: null,
                  Per: null,
                  created_by: "AnonymousUser",
                  Invoice_data: "001",
                  HSN_details: 2,
                },
                {
                  id: 2,
                  Products: "Machinery",
                  quantity: 1,
                  Discount: "10.00",
                  Amount: null,
                  CGST: null,
                  SGST: null,
                  IGST: null,
                  Hsn_code: null,
                  Rate: null,
                  Per: null,
                  created_by: "AnonymousUser",
                  Invoice_data: "001",
                  HSN_details: 2,
                },
                {
                  id: 3,
                  Products: "Machinery",
                  quantity: 12,
                  Discount: "10.00",
                  Amount: null,
                  CGST: null,
                  SGST: null,
                  IGST: null,
                  Hsn_code: null,
                  Rate: null,
                  Per: null,
                  created_by: "AnonymousUser",
                  Invoice_data: "001",
                  HSN_details: 2,
                },
                {
                  id: 4,
                  Products: "ok",
                  quantity: 12,
                  Discount: "10.00",
                  Amount: "50.00",
                  CGST: null,
                  SGST: null,
                  IGST: null,
                  Hsn_code: null,
                  Rate: null,
                  Per: null,
                  created_by: "AnonymousUser",
                  Invoice_data: "001",
                  HSN_details: 2,
                },
              ],
            },
          ]
               
        setCreateInvoiceData({
          ...createInvoiceData,                  
          details:response.data
        });
      });
  };

  const [invoiceState, setInvoiceState] = useState({})


  const ChangeData = (id) => {
    let data = createInvoiceData.details.filter((data) => data.id == id)[0]
    let customResp={};          
        customResp.id=data.id;
        customResp.companyname=data.Buyer_data;
        customResp.file=data.file;
        customResp.invoice_no=data.Invoice_no;
        customResp.subtotal=data.Total;
        customResp.invoice_date=data.Invoice_date;
        customResp.payment_mode=data.Terms_of_payment;
        customResp.CGST=""
        customResp.SGST=""
        customResp.IGST=""
        customResp.bank_details="";
        customResp.company_details="";
        customResp.legdername="";
        customResp.GSTlegderdata="";
        customResp.GSTlegderamount="";
        customResp.Narration="";
        customResp.Reference_no=data.Reference_no;
        customResp.csvreceipt=[];
        let data2=data.invoicedatas;
        for (let index = 0; index < data2.length; index++) {   
          let customObject={
              id: data2[index].id,
              Products: data2[index].Products,
              HSN_SAC: data2[index].Hsn_code,
              GST_rate:"",
              quantity: data2[index].quantity,
              Rate: data2[index].Rate,
              Amount: data2[index].Amount,
              Total:"",
              CGST:data2[index].CGST,
              SGST: data2[index].SGST,
              IGST: data2[index].IGST,
              Invoice_data: data2[index].Invoice_data,
          }
          customResp.csvreceipt.push(customObject)               
        } 
        setInvoiceState(customResp)
  }

  return (
    <>
      <div className="WhiteShadowCard p-30px mb-3">
        <div className="form-group">
          <label className="font-size-12px pl-2">Report Type</label>
          <select
            value={props.ReportType}
            className="form-control"
            onChange={props.handleReportType}
          >
            <option value="create">Create Invoice</option>
            <option value="recieve">Recieve Invoice</option>
          </select>
        </div>
        <div className="form-group">
          <label className="font-size-12px pl-2">Select a Buyer Company</label>
          <select ref={buyerEL} className="form-control">
            <option value="none" selected disabled>
              Select a Company
            </option>
            {companyList.map((res) => (
              <option key={res.id} value={res.id}>
                {res.buyer_company}
              </option>
            ))}
          </select>
        </div>
        <Row>
          <Col lg={6}>
            <label className="font-size-12px pl-2">Select Date</label>
            <div className="row">
              <Col className="col-md-6">
                <div className="form-group">
                  <DatePicker
                    selected={startDate}
                    onChange={(date) => setStartDate(date)}
                    selectsStart
                    className="form-control"
                    startDate={startDate}
                    endDate={endDate}
                    placeholderText="From Date"
                  />
                </div>
              </Col>
              <Col className="col-md-6">
                <div className="form-group">
                  <DatePicker
                    selected={endDate}
                    onChange={(date) => setEndDate(date)}
                    selectsEnd
                    className="form-control"
                    startDate={startDate}
                    endDate={endDate}
                    minDate={startDate}
                    placeholderText="To Date"
                  />
                </div>
              </Col>
            </div>
          </Col>
          <Col lg={6}>
            <label className="font-size-12px pl-2">Select Range</label>
            <div className="row">
              <Col className="col-md-6">
                <div className="form-group">
                  <input
                    ref={selectRangeFrom}
                    type="text"
                    className="form-control"
                    placeholder="From Range"
                  />
                </div>
              </Col>
              <Col className="col-md-6">
                <div className="form-group">
                  <input
                    ref={selectRangeTo}
                    type="text"
                    className="form-control"
                    placeholder="To Range"
                  />
                </div>
              </Col>
            </div>
          </Col>
        </Row>
        <Button
          variant="primary"
          onClick={() => {
            handleCreateInvoiceOkbutton();
          }}
        >
          OK
        </Button>
      </div>
      {createInvoiceData.details.length > 0 && (
        <div className="WhiteShadowCard p-30px mb-3">
          <Table responsive hover>
            <thead>
              <tr>
                <th>SR No.</th>
                <th>Buyer Name</th>
                <th>Invoice Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {createInvoiceData.details.map((data, index) => {
                return (
                  <tr>
                    <td>{(index+1)}</td>
                    <td>{data.Buyer_data}</td>
                    <td>{data.Invoice_date}</td>
                    <td className="pl-0">
                      <button className="ok-btn ml-2" onClick={()=>{setViewId(data.id); ChangeData(data.id); props.handelFile(createInvoiceData.details.filter((item) => item.id == data.id)[0].file)}}>View</button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </div>
      )}
      {viewId != 0 && (
        <div className="WhiteShadowCard p-30px mb-3 mb-xl-0">
          <InvoiceContainer report={true} invoiceResposne={invoiceState} />
        </div>
      )}
    </>
  );
};

export default CreateInvoiceReport;
