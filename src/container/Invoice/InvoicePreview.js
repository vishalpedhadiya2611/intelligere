import React from "react";
import { Row, Col, Table } from "react-bootstrap";
import { useSelector } from "react-redux";
import { ToWords } from "to-words";

const InvoicePreview = React.forwardRef((props, ref) => {
  const { sellerData, buyerData, otherData, productData } = useSelector(
    (state) => state.InvoiceCreateReducer
  );

  let totalCGST = 0.0;
  let totalSGST = 0;
  let totalIGST = 0;
  let totalAmount = 0;

  productData.forEach((data) => {
    if (data.CGST > 0) {
      totalCGST += parseFloat(data.Amount * (data.CGST / 100));
    }
    if (data.SGST > 0) {
      totalSGST += parseFloat(data.Amount * (data.SGST / 100));
    }
    if (data.IGST > 0) {
      totalIGST += parseFloat(data.Amount * (data.IGST / 100));
    }
    if (data.Amount > 0) {
      totalAmount += parseFloat(data.Amount);
    }
  });
  const toWords = new ToWords()



  return (
    <div ref={ref}>
      <Row className="align-items-end pt-4">
        <Col xs={12}>
          <div className="text-center font-weight-600 font-size-20px">
            e-invoice
          </div>
        </Col>
      </Row>
      <Row className="align-items-end pb-3">
        <Col xs={2}>
          <div className="font-size-14px">IRN :</div>
          <div className="font-size-14px">Ack No. :</div>
          <div className="font-size-14px">Ack Date :</div>
        </Col>
        <Col xs={6}>
          <div className="font-weight-500 font-size-14px">123456456 </div>
          <div className="font-weight-500 font-size-14px">4565465</div>
          <div className="font-weight-500 font-size-14px">20/08/2021 </div>
        </Col>
        <Col xs={4} className="text-center">
          <img
            src="logo192.png"
            alt="logo"
            className="w-100 max-w-120px max-h-120px"
          />
        </Col>
      </Row>
      <table className="printtable vtop">
        <tbody>
          <tr>
            <td colSpan="2" rowSpan="3">
              <div className="font-weight-600 font-size-14px">
                Seller Detail
              </div>
              <div>Company Name: {sellerData.seller_company}</div>
              <div>Name: {sellerData.seller_name}</div>
              <div className="d-flex">
                <div className="pr-3">Phone: {sellerData.seller_phone}</div>
                <div>Email: {sellerData.seller_email}</div>
              </div>
              <div>Address: {sellerData.seller_address}</div>
              <div>Website: {sellerData.seller_website}</div>
              <div className="d-flex">
                <div className="pr-3">
                  State Name: {sellerData.seller_state}
                </div>
                <div>GSTIN/UIN: {sellerData.seller_gstin}</div>
              </div>
            </td>
            <td>
              <div>Invoice No.</div>
              <div>{otherData.Invoice_no}</div>
            </td>
            <td>
              <div>Invoice Date</div>
              <div>{otherData.Invoice_date}</div>
            </td>
          </tr>
          <tr>
            <td>
              <div>Terms of Payment</div>
              <div>{otherData.Terms_of_payment}</div>
            </td>
            <td>
              <div>Reference No</div>
              <div>{otherData.Reference_no}</div>
            </td>
          </tr>
          <tr>
            <td>
              <div>PO Number</div>
              <div>{otherData.P_O_no}</div>
            </td>
            <td>
              <div>PO Date</div>
              <div>{otherData.P_O_date}</div>
            </td>
          </tr>
          <tr>
            <td colSpan="2" rowSpan="2">
              <div className="font-weight-600 font-size-14px">Buyer Detail</div>
              <div>Company Name: {buyerData.buyer_company}</div>
              <div>Name: {buyerData.buyer_name}</div>
              <div className="d-flex">
                <div className="pr-3">Phone: {buyerData.buyer_phone}</div>
                <div>Email: {buyerData.buyer_email}</div>
              </div>
              <div>Address: {buyerData.buyer_address}</div>
              <div>Website: {buyerData.buyer_website}</div>
              <div className="d-flex">
                <div className="pr-3">State Name: {buyerData.buyer_state}</div>
                <div>GSTIN/UIN: {buyerData.buyer_gstin}</div>
              </div>
            </td>
            <td colSpan="2" rowSpan="4">
              <div>Delivery Details</div>
              <div>{otherData.Delievry_note}</div>
            </td>
          </tr>
        </tbody>
      </table>
      <div className="table-responsive">
        <table className="printtable">
          <thead>
            <tr>
              <th width="50px">Sl No.</th>
              <th width="auto">Description of Goods</th>
              <th width="30px">HSN/SAC</th>
              <th width="30px">Quantity</th>
              <th width="30px">Rate</th>
              <th width="30px">per</th>
              <th width="50px">Disc. %</th>
              <th width="80px">Amount</th>
            </tr>
          </thead>
          <tbody>
            {productData.map((data, ind) => (
              <tr key={ind}>
                <td>{ind + 1}</td>
                <td>{data.Products}</td>
                <td>{data.Hsn_code}</td>
                <td>{data.quantity}</td>
                <td>{data.Rate}</td>
                <td>{data.Per}</td>
                <td>{data.Discount + " %"}</td>
                <td>{data.Amount}</td>
              </tr>
            ))}

            <tr>
              <th colSpan="2" className="text-right">
                Central Tax
              </th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <th>Rs. {totalCGST.toFixed(2)}</th>
            </tr>
            <tr>
              <th colSpan="2" className="text-right">
                State Tax
              </th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <th>Rs. {totalSGST.toFixed(2)}</th>
            </tr>
            <tr>
              <th colSpan="2" className="text-right">
                Integrated Tax
              </th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <th>Rs. {totalIGST.toFixed(2)}</th>
            </tr>
            <tr>
              <th colSpan="2" className="text-right">
                Total
              </th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <th colSpan={2}>
                Rs.{" "}
                {(
                  parseFloat(totalIGST) +
                  parseFloat(totalCGST) +
                  parseFloat(totalSGST) +
                  parseFloat(totalAmount)
                ).toFixed(2)}
              </th>
            </tr>
          </tbody>
        </table>
      </div>
      <div className="table-responsive">
        <table className="printtable">
          <thead>
            <tr>
              <th width="50px">Sl No.</th>
              <th width="auto">Description of Goods</th>
              <th width="30px">HSN/SAC</th>
              <th width="30px">Quantity</th>
              <th width="30px">Rate</th>
              <th width="30px">per</th>
              <th width="50px">Disc. %</th>
              <th width="80px">Amount</th>
            </tr>
          </thead>
          <tbody>
            {productData.map((data, ind) => (
              <tr key={ind}>
                <td>{ind + 1}</td>
                <td>{data.Products}</td>
                <td>{data.Hsn_code}</td>
                <td>{data.quantity}</td>
                <td>{data.Rate}</td>
                <td>{data.Per}</td>
                <td>{data.Discount + " %"}</td>
                <td>{data.Amount}</td>
              </tr>
            ))}

            <tr>
              <th colSpan="2" className="text-right">
                Central Tax
              </th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <th>Rs. {totalCGST.toFixed(2)}</th>
            </tr>
            <tr>
              <th colSpan="2" className="text-right">
                State Tax
              </th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <th>Rs. {totalSGST.toFixed(2)}</th>
            </tr>
            <tr>
              <th colSpan="2" className="text-right">
                Integrated Tax
              </th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <th>Rs. {totalIGST.toFixed(2)}</th>
            </tr>
            <tr>
              <th colSpan="2" className="text-right">
                Total
              </th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <th colSpan={2}>
                Rs.{" "}
                {(
                  parseFloat(totalIGST) +
                  parseFloat(totalCGST) +
                  parseFloat(totalSGST) +
                  parseFloat(totalAmount)
                ).toFixed(2)}
              </th>
            </tr>
          </tbody>
        </table>
      </div>
      <table className="printtable vtop">
        <tbody>
          <tr>
            <td>
              <div className="d-flex w-100 justify-content-between">
                <div>
                  <div>Amount Chargeable(in words)</div>
                  <div className="font-size-14px font-weight-600">
                    {/* Indian Rupee One Thousand Only */}
                    {toWords.convert((
                  parseFloat(totalIGST) +
                  parseFloat(totalCGST) +
                  parseFloat(totalSGST) +
                  parseFloat(totalAmount)
                ).toFixed(2), { currency: true })}
                  </div>
                </div>
                <div>E. & O.E</div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
      <div className="table-responsive ">
        <table className="printtable vtop text-center">
          <thead>
            <tr>
              <th rowSpan="2" className="w-200px">
                HSN/SAC
              </th>
              <th rowSpan="2">
                Taxable
                <br />
                Value
              </th>
              <th colSpan="2">Central Tax</th>
              <th colSpan="2">State Tax</th>
              <th colSpan="2">Integrated Tax</th>
              <th rowSpan="2">
                Total
                <br />
                Tax Amount
              </th>
            </tr>
            <tr>
              <th>Rate</th>
              <th>Amount</th>
              <th>Rate</th>
              <th>Amount</th>
              <th>Rate</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            {productData.map((data, index2) => (
              <tr key={index2}>
                <td></td>
                <td></td>
                <td>{parseFloat(data.CGST).toFixed(2) > 0 ? parseFloat(data.CGST).toFixed(2) : ""}%</td>
                <td>{(data.Amount * (data.CGST / 100)).toFixed(2) > 0 ? (data.Amount * (data.CGST / 100)).toFixed(2) : ""}</td>
                <td>{parseFloat(data.SGST).toFixed(2) > 0 ? parseFloat(data.SGST).toFixed(2) : ""}%</td>
                <td>{(data.Amount * (data.SGST / 100)).toFixed(2) > 0 ? (data.Amount * (data.SGST / 100)).toFixed(2) : ""}</td>
                <td>{parseFloat(data.IGST).toFixed(2) > 0 ? parseFloat(data.IGST).toFixed(2) : ""}%</td>
                <td>{(data.Amount * (data.IGST / 100)).toFixed(2) > 0 ? (data.Amount * (data.IGST / 100)).toFixed(2):  ""}</td>
                {/* <td>{(parseFloat(totalIGST) + parseFloat(totalCGST) + parseFloat(totalSGST)).toFixed(2)}</td> */}
                <td>
                  {(
                    data.Amount * (data.CGST / 100) +
                    data.Amount * (data.SGST / 100) +
                    data.Amount * (data.IGST / 100)
                  ).toFixed(2)}
                </td>
              </tr>
            ))}

            <tr>
              <th className="text-right">Total</th>
              <td></td>
              <td></td>
              <td>{totalCGST.toFixed(2)}</td>
              <td></td>
              <td>{totalSGST.toFixed(2)}</td>
              <td></td>
              <td>{totalIGST.toFixed(2)}</td>
              <td>{(totalIGST + totalSGST + totalCGST).toFixed(2)}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <table className="printtable vtop">
        <tbody>
          <tr>
            <td className="pr-0 pb-0">
              Tax Amount (in words) :{" "}
              <span className="font-size-14px font-weight-600">
                {/* Indian Rupee One Thousand Only */}
                {toWords.convert((totalIGST + totalSGST + totalCGST).toFixed(2), { currency: true })}
              </span>
              <div className="d-flex mt-2">
                <Col xs={6} className="pl-0">
                  <div>
                    GST NO :{" "}
                    <span className="font-size-14px font-weight-600">
                    {sellerData.seller_gstno}
                    </span>
                  </div>
                  <div>
                    PAN NO :{" "}
                    <span className="font-size-14px font-weight-600">
                      {sellerData.seller_panno}
                    </span>
                  </div>
                  <div>
                    TAN NO :{" "}
                    <span className="font-size-14px font-weight-600">
                    {sellerData.seller_tanno}
                    </span>
                  </div>
                </Col>
                <Col xs={6} className="pr-0">
                  <div>
                    Bank Name :{" "}
                    <span className="font-size-14px font-weight-600">{sellerData.seller_bankname}</span>
                  </div>
                  <div>
                    Account No :{" "}
                    <span className="font-size-14px font-weight-600">
                    {sellerData.seller_accountno}
                    </span>
                  </div>
                  <div>
                    IFSC Code :{" "}
                    <span className="font-size-14px font-weight-600">
                    {sellerData.seller_ifccode}
                    </span>
                  </div>
                </Col>
              </div>
              <div className="d-flex mt-2">
                <Col xs={6} className="pl-0">
                  <div className="text-decoration pb-1">Declaration</div>
                  We declare that this invoice shows the actual price of the
                  goods described and that all particulars are true and correct.
                </Col>
                <Col xs={6} className="pr-0">
                  <div className="border text-right p-1">
                    <div className="h-50px">
                      <span className="font-size-14px font-weight-600">
                        for
                      </span>
                      <span className="h-10px d-inline-block w-225px"></span>
                    </div>
                    <div>Authorised Signatory</div>
                  </div>
                </Col>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
      <div className="font-size-14px text-center py-2">
        This is a Computer Generated Invoice
      </div>
    </div>
  );
});

export default InvoicePreview;
