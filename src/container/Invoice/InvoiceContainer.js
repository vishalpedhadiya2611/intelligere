import axios from "axios";
import React, { useEffect, useState } from "react";
import { Row, Col, Button } from "react-bootstrap";
import { NewVoucherForInvoiceImport } from "../../components/Modal/NewVoucherForInvoiceImport";

function InvoiceContainer(props) {
  const { invoiceResposne, okBTn } = props;
  const [invoiceData, setInvoiceData] = useState({});
  const [ShowVoucherEntry, setShowVoucherEntry] = useState(false);

  useEffect(() => {
    setInvoiceData(invoiceResposne);
  }, [invoiceResposne]);

  const onChangeInput = (fname, fvalue) => {
    setInvoiceData({ ...invoiceData, [fname]: fvalue });
  };
  const handleHideVoucherEntry = () => setShowVoucherEntry(false);
  const handleShowVoucherEntry = () => setShowVoucherEntry(true);

  const onSubmitBuyerDetalis = async () => {
    await axios({
      method: "put",
      url: `http://127.0.0.1:8000/invoice/UpdatecsvDataAPI/${invoiceData.id}/`,
      data: invoiceData,
      headers: {
        "content-type": "application/json",
      },
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onChangeInputCSV = (fname, fvalue, fid, index) => {
    const changeRow = { ...invoiceData.csvreceipt[index], [fname]: fvalue };
    setInvoiceData({
      ...invoiceData,
      csvreceipt: [
        ...invoiceData.csvreceipt.slice(0, index),
        changeRow,
        ...invoiceData.csvreceipt.slice(index + 1),
      ],
    });
  };

  const onSubmitRowDetalis = async (rowId) => {
    const data = { ...invoiceData.csvreceipt }[rowId];
    await axios({
      method: "put",
      url: `http://127.0.0.1:8000/invoice/UpdatecsvDatadetailsAPI/${rowId}/`,
      data,
    })
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  return (
    <>
      <div className="rowCol">
        <div className="form-group">
          <label className="font-size-12px pl-2">Buyer Name</label>
          <input
            className="form-control"
            type="text"
            name="companyname"
            value={invoiceData.companyname}
            onChange={(e) => onChangeInput(e.target.name, e.target.value)}
            disabled={!okBTn}
          />
        </div>
        <Row>
          <Col md={3} className="">
            <div className="form-group">
              <label className="font-size-12px pl-2">Invoice Number</label>
              <input
                className="form-control"
                type="text"
                name="invoice_no"
                value={invoiceData.invoice_no}
                onChange={(e) => onChangeInput(e.target.name, e.target.value)}
                disabled={!okBTn}
              />
            </div>
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">Invoice Date</label>
              <input
                className="form-control"
                type="text"
                name="invoice_date"
                value={invoiceData.invoice_date}
                onChange={(e) => onChangeInput(e.target.name, e.target.value)}
                disabled={!okBTn}
              />
            </div>
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">Payment Terms</label>
              <input
                className="form-control"
                type="text"
                name="payment_mode"
                value={invoiceData.payment_mode}
                onChange={(e) => onChangeInput(e.target.name, e.target.value)}
                disabled={!okBTn}
              />
            </div>
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">Supplier Ref No.</label>
              <input
                className="form-control"
                type="text"
                name="payment_mode"
                value={invoiceData.payment_mode}
                onChange={(e) => onChangeInput(e.target.name, e.target.value)}
                disabled={!okBTn}
              />
            </div>
          </Col>
        </Row>
        {/*----------------------*/}
        <Row>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">CGST</label>
              <input
                className="form-control"
                type="text"
                name="CGST"
                value={invoiceData.CGST}
                onChange={(e) => onChangeInput(e.target.name, e.target.value)}
                disabled={!okBTn}
              />
            </div>
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">SGST</label>
              <input
                className="form-control"
                type="text"
                name="SGST"
                value={invoiceData.SGST}
                onChange={(e) => onChangeInput(e.target.name, e.target.value)}
                disabled={!okBTn}
              />
            </div>
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">IGST</label>
              <input
                className="form-control"
                type="text"
                name="IGST"
                value={invoiceData.IGST}
                onChange={(e) => onChangeInput(e.target.name, e.target.value)}
                disabled={!okBTn}
              />
            </div>
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label className="font-size-12px pl-2">Total</label>
              <input
                className="form-control"
                type="text"
                name="subtotal"
                value={invoiceData.subtotal}
                onChange={(e) => onChangeInput(e.target.name, e.target.value)}
                disabled={!okBTn}
              />
            </div>
          </Col>
        </Row>
        {props.okayBTN && (
          <div className="text-right">
            <Button variant="primary" onClick={onSubmitBuyerDetalis}>
              OK
            </Button>
          </div>
        )}
        {/*----------------------*/}
        <hr />
        {typeof invoiceData.csvreceipt == "object" &&
          invoiceData.csvreceipt.map((invoice, index) => (
            <Row>
              <Col md={3}>
                <div className="form-group">
                  <label className="font-size-12px pl-2">Goods Detail</label>
                  <input
                    className="form-control"
                    type="text"
                    name="Products"
                    value={invoice.Products}
                    onChange={(e) =>
                      onChangeInputCSV(
                        e.target.name,
                        e.target.value,
                        invoice.id,
                        index
                      )
                    }
                    disabled={!okBTn}
                  />
                </div>
              </Col>
              <Col md={3}>
                <Row>
                  <Col md={6}>
                    <div className="form-group">
                      <label className="font-size-12px pl-2">HSN</label>
                      <input
                        className="form-control"
                        type="text"
                        name="HSN_SAC"
                        value={invoice.HSN_SAC}
                        onChange={(e) =>
                          onChangeInputCSV(
                            e.target.name,
                            e.target.value,
                            invoice.id,
                            index
                          )
                        }
                        disabled={!okBTn}
                      />
                    </div>
                  </Col>
                  <Col md={6}>
                    <div className="form-group">
                      <label className="font-size-12px pl-2">Quantity</label>
                      <input
                        className="form-control"
                        type="text"
                        name="quantity"
                        value={invoice.quantity}
                        onChange={(e) =>
                          onChangeInputCSV(
                            e.target.name,
                            e.target.value,
                            invoice.id,
                            index
                          )
                        }
                        disabled={!okBTn}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col md={3}>
                <Row>
                  <Col md={4}>
                    <div className="form-group">
                      <label className="font-size-12px pl-2">GST_rate</label>
                      <input
                        className="form-control"
                        type="text"
                        name="GST_rate"
                        value={invoice.GST_rate}
                        onChange={(e) =>
                          onChangeInputCSV(
                            e.target.name,
                            e.target.value,
                            invoice.id,
                            index
                          )
                        }
                        disabled={!okBTn}
                      />
                    </div>
                  </Col>
                  <Col md={4}>
                    <div className="form-group">
                      <label className="font-size-12px pl-2">Rate</label>
                      <input
                        className="form-control"
                        type="text"
                        name="Rate"
                        value={invoice.Rate}
                        onChange={(e) =>
                          onChangeInputCSV(
                            e.target.name,
                            e.target.value,
                            invoice.id,
                            index
                          )
                        }
                        disabled={!okBTn}
                      />
                    </div>
                  </Col>
                  <Col md={4}>
                    <div className="form-group">
                      <label className="font-size-12px pl-2">Unit</label>
                      <input
                        className="form-control"
                        type="text"
                        name="Unit"
                        //   value={invoice.Unit}
                        onChange={(e) =>
                          onChangeInputCSV(
                            e.target.name,
                            e.target.value,
                            invoice.id,
                            index
                          )
                        }
                        disabled={!okBTn}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col md={3}>
                <Row>
                  <Col md={4}>
                    <div className="form-group">
                      <label className="font-size-12px pl-2">Amount</label>
                      <input
                        className="form-control"
                        type="text"
                        name="Amount"
                        value={invoice.Amount}
                        onChange={(e) =>
                          onChangeInputCSV(
                            e.target.name,
                            e.target.value,
                            invoice.id,
                            index
                          )
                        }
                        disabled={!okBTn}
                      />
                    </div>
                  </Col>
                  <Col md={8}>
                    <div className="form-group">
                      <label className="font-size-12px pl-2">Total</label>
                      <div className="d-flex">
                        <input
                          className="form-control"
                          type="text"
                          name="Total"
                          value={invoice.Total}
                          onChange={(e) =>
                            onChangeInputCSV(
                              e.target.name,
                              e.target.value,
                              invoice.id,
                              index
                            )
                          }
                          disabled={!okBTn}
                        />
                        {props.okBTn && (
                          <Button
                            variant="primary"
                            className="ml-2"
                            onClick={() => onSubmitRowDetalis(index)}
                          >
                            OK
                          </Button>
                        )}
                      </div>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          ))}

        {props.okayBTN && (
          <div className="text-center">
            <Button variant="primary" onClick={handleShowVoucherEntry}>
              OK
            </Button>
          </div>
        )}
      </div>
      <NewVoucherForInvoiceImport
        show={ShowVoucherEntry}
        handleClose={handleHideVoucherEntry}
        TotalAmount={invoiceData.subtotal}
        InvoiceNo={invoiceData.invoice_no}
        companyname={invoiceData.companyname}
        invoice_date={invoiceData.invoice_date}
      />
    </>
  );
}

export default InvoiceContainer;
