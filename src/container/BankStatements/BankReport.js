import React, { useEffect, useState } from "react";
import { Button, Container, Row, Col, Table } from "react-bootstrap";
import PdfContainer from "./PdfContainer";
import BankContainer from "./BankContainer";
import { Title } from "../../components/Texonomy";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import { useSelector } from "react-redux";
import axios from "axios";
import Response from "./reportResponse.json";
import moment from "moment";
import { UpdateResponse } from "./CommonFunction";
import { useDispatch } from "react-redux";
import {
  bankListAction,
  removeUpdatedBankStmtList,
} from "../../redux/action/BankStatementAction";
import { BankListAPI } from "./Api";

const BankReport = () => {
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [bankList, setBankList] = useState([]);
  const [filter, setFilter] = useState({
    startDate: "",
    endDate: "",
    bankId: "",
  });

  const [filterResponse, setFilterResponse] = useState([]);
  const [bnkFileBas64, setBnkFileBas64] = useState("");
  const [apiCalled, setApiCalled] = useState(false)
  const [disableOK, setDisableOK] = useState(false)
  const dispatch = useDispatch();

  // reducer
  const bankListData = useSelector((state) => state.CommonReducer.bankList);

  useEffect(() => {
    setBankList(bankListData != undefined ? bankListData : []);
  }, [bankListData]);

  useEffect(async () => {
    const bankListData = await BankListAPI();
    dispatch(bankListAction(bankListData));
    setBankList(bankListData);
  }, []);

  useEffect(() => {
    return () => {
      dispatch(removeUpdatedBankStmtList([]));
    };
  }, []);


  useEffect(() => {
    setDisableOK(false)
    setApiCalled(false)
    setFilterResponse([])
  } ,[filter])

  const GetFilterData = async () => {
    axios({
      method: "get",
      url: `http://127.0.0.1:8001/e_payment/BankStatementfilter/?created_on_after=${filter.startDate}&created_on_before=${filter.endDate}&bankname=${filter.bankId}`,
    })
      .then((response) => {
        setFilterResponse(response.data);
        setApiCalled(true)
        setDisableOK(true)
      })
      .catch((error) => {
        console.log("post api---", error);
        setApiCalled(true) //remove this 
        setDisableOK(true) //remove this 
      });
  };

  const onClickViewButton = (source, data) => {
    setBnkFileBas64(source);
    const result = UpdateResponse(data);
    dispatch(removeUpdatedBankStmtList(result));
  };

  return (
    <>
      <Container fluid className="rowCol">
        <Title title="Bank Statement Report" />
        <Row>
          <Col xl={6} className="w-1366">
            <div className="WhiteShadowCard p-30px mb-3">
              <div className="form-group">
                <select className="form-control">
                  <option value="none" selected disabled>
                    Select a Company
                  </option>
                  <option>Comapny 1</option>
                  <option>Comapny 2</option>
                  <option>Comapny 3</option>
                </select>
              </div>
              <Row>
                <Col lg={6}>
                  <select
                    className="form-control mt-4"
                    name="particular"
                    onChange={(e) =>
                      setFilter({ ...filter, bankId: e.target.value })
                    }
                  >
                    {bankList.map((resb, index) => {
                      return (
                        <option value={index == 0 ? "" : resb.id} key={index}>
                          {resb.bankname}
                        </option>
                      );
                    })}
                  </select>
                </Col>
                <Col lg={6}>
                  <label className="font-size-12px pl-2">Select Date</label>
                  <div className="row">
                    <Col className="col-md-6">
                      <div className="form-group">
                        <DatePicker
                          selected={
                            filter.startDate != ""
                              ? new Date(filter.startDate)
                              : ""
                          }
                          onChange={(date) => {
                            setFilter({
                              ...filter,
                              startDate: moment(date).format("YYYY-MM-DD"),
                            });
                          }}
                          selectsStart
                          className="form-control"
                          startDate={
                            filter.startDate != ""
                              ? new Date(filter.startDate)
                              : ""
                          }
                          endDate={
                            filter.endDate != "" ? new Date(filter.endDate) : ""
                          }
                          placeholderText="From Date"
                          // dateFormat="dd-mm-yyyy"
                          dateFormat="dd/MM/yyyy"
                        />
                      </div>
                    </Col>
                    <Col className="col-md-6">
                      <div className="form-group">
                        <DatePicker
                          selected={
                            filter.endDate != "" ? new Date(filter.endDate) : ""
                          }
                          onChange={(date) => {
                            setFilter({
                              ...filter,
                              endDate: moment(date).format("YYYY-MM-DD"),
                            });
                          }}
                          selectsEnd
                          className="form-control"
                          startDate={
                            filter.startDate != ""
                              ? new Date(filter.startDate)
                              : ""
                          }
                          endDate={
                            filter.endDate != "" ? new Date(filter.endDate) : ""
                          }
                          minDate={
                            filter.startDate != ""
                              ? new Date(filter.startDate)
                              : ""
                          }
                          placeholderText="To Date"
                          dateFormat="dd/MM/yyyy"
                        />
                      </div>
                    </Col>
                  </div>
                </Col>
              </Row>
              <Button
                variant="primary"
                onClick={GetFilterData}
                disabled={disableOK}
              >
                OK
              </Button>
            </div>
            <div className="WhiteShadowCard p-30px mb-3">
              {filterResponse.length == 0 && apiCalled ? (
                <>NO Data</>
              ) : (
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th>SR No.</th>
                      <th>Import Date</th>
                      <th>From Date</th>
                      <th>To Date</th>
                      <th>Total Entires</th>
                    </tr>
                  </thead>
                  <tbody>
                    {filterResponse.map((data, index) => {
                      return (
                        <tr>
                          <td>{index + 1}</td>
                          <td>{data.created_on}</td>
                          <td>{data.s_date}</td>
                          <td>{data.e_date}</td>
                          <td>
                            {data.entry}
                            <button
                              className="ok-btn ml-2"
                              onClick={() =>
                                onClickViewButton(data.file, data.showdataentry)
                              }
                            >
                              View
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              )}
            </div>
            {apiCalled && (filterResponse.length > 0) && <div className="WhiteShadowCard p-30px mb-3 mb-xl-0">
              <div className="tableHDR">
                <Row>
                  <Col md={3}>
                    <div className="font-size-12px text-primary opacity-40">
                      Date
                    </div>
                  </Col>
                  <Col md={7}>
                    <div className="font-size-12px text-primary opacity-40">
                      Particulars
                    </div>
                  </Col>
                  <Col md={2}>
                    <div className="font-size-12px text-primary opacity-40">
                      Amount
                    </div>
                  </Col>
                </Row>
              </div>
              <BankContainer />
            </div>}
          </Col>
          <Col xl={6} className="w-1366">
            {/* <div className="WhiteShadowCard h-100">
              <PdfContainer bnkFileBas64={bnkFileBas64}/>
            </div> */}

            <div className="WhiteShadowCard position-stiky">
              {bnkFileBas64 != "" ? (
                <PdfContainer bnkFileBas64={bnkFileBas64} />
              ) : (
                <div className="pdfView">
                  <div className="opacity-50 font-size-20px text-primary">
                    Image Preview
                  </div>
                </div>
              )}
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default BankReport;
