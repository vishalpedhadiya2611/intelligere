import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import PdfContainer from "./PdfContainer";
import BankContainer from "./BankContainer";
import axios from "axios";
import { Link } from "react-router-dom";
import { BackIcon } from "../../components/Icons/Icons";
import { Title } from "../../components/Texonomy";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import csvDemoFile from "../../doc/tabula-jalpa.csv";
import { func } from "prop-types";
import resData from "../../doc/response.js";
// import resData from './response.json';
import { useDispatch, useSelector } from "react-redux";
import {
  bankListAction,
  BankStatementData,
  companyListAction,
  ledgerListAction,
  removeUpdatedBankStmtList,
} from "../../redux/action/BankStatementAction";
import { BankListAPI, CompanyListAPI, LedgerListAPI } from "./Api";
import { UpdateResponse } from "./CommonFunction";

const BankStatement = () => {
  const updatedBankData = useSelector(
    (state) => state.bankStatement.updatedBankStmtList
  );
  const [bnkCsv, setbnkCsv] = useState(null);
  const [bnkFileBas64, setbnkFileBas64] = useState(null);
  const [CSVRes, setCSVRes] = useState(null);

  const [bankname, setBankName] = useState(null);
  const [showTable, setShowTable] = useState(true);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [bank, setBank] = useState(false);
  const [filetype, setFiletype] = useState(".pdf");
  var bodyFormData = new FormData();

  const [companyList, setCompanyList] = useState([]);
  const [bankList, setBankList] = useState([]);
  const [bankNameList, setBankNameList] = useState([]);
  const [selectedBankName, setSelectedBankName] = useState("");

  useEffect(() => {
    if (updatedBankData.length == 0) {
      setbnkCsv(null);
      setbnkFileBas64(null);
    }
  }, [updatedBankData]);

  const dispatch = useDispatch();
  useEffect(async () => {
    // dispatch(BankStatementData());
    // updateDebit();

    dispatch(BankStatementData([]));
    // updateDebit(resData);
    const result = UpdateResponse(resData);
    dispatch(removeUpdatedBankStmtList(result));
    // dispatch(companyList(resData))

    // companyList
    const companyListData = await CompanyListAPI();
    dispatch(companyListAction(companyListData));
    setCompanyList(companyListData);

    // bankList
    const bankListData = await BankListAPI();
    dispatch(bankListAction(bankListData));
    setBankList(bankListData);

    // ledgerList
    const ledgerListData = await LedgerListAPI();
    dispatch(ledgerListAction(ledgerListData));
  }, []);

  useEffect(() => {
    return () => {
      dispatch(removeUpdatedBankStmtList([]));
    };
  }, []);

  const onSelectFileHandler = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();

    reader.addEventListener(
      "load",
      function () {
        setbnkCsv(file);
        setbnkFileBas64(reader.result);
      },
      false
    );

    if (file) {
      reader.readAsDataURL(file);
    }
  };

  const onChangeBankName = (id) => {
    const list = bankNameList.filter((res, index) => {
      return res.id == id;
    });
    if (list.length > 0) {
      setSelectedBankName(list[0].bankname);
    }
  };

  //bank list
  const onComapanyChangeHandle = (companyId) => {
    const bankListA = bankList.filter(
      (res) => res.companyname === parseInt(companyId)
    );
    setBankNameList(bankListA);
    if (bankListA.length == 1) {
      setBank(true);
      setBankName(bankListA[0].id);
      bodyFormData.append("bankname", parseInt(bankname));
    }
  };

  //on submit file
  const onSubmitClickHandler = async () => {
    // const BankID = bankList.filter((data) => data.bankname == bankname)

    // setBankName(BankID[0].companyname)

    bodyFormData.append("file", bnkCsv);
    bodyFormData.append("bankname", parseInt(bankname));

    axios({
      method: "post",
      url: "http://127.0.0.1:8000/e_payment/bankstmnt/",
      headers: {
        "content-type": "multipart/form-data",
      },
      data: bodyFormData,
    })
      .then((response) => {
        axios({
          method: "get",
          url: "http://127.0.0.1:8000/e_payment/NLPDataViews/",
          headers: {
            "content-type": "application/json",
          },
        })
          .then((response) => {
            axios({
              method: "post",
              url: "http://127.0.0.1:8000/e_payment/PostViews/",
              headers: {
                "content-type": "multipart/form-data",
              },
              data: {
                bank: "",
                Date: null,
                Transaction: "",
                Legder: "",
                Cheque_no: "",
                Credit: null,
                Debit: null,
              },
            })
              .then((response) => {
                axios({
                  method: "get",
                  url: "http://127.0.0.1:8000/e_payment/PostViews/",
                  headers: {
                    "content-type": "application/json",
                  },
                })
                  .then((response) => {
                    setCSVRes(response.data);
                    // updateDebit(response.data);
                    const result = UpdateResponse(response.data);
                    dispatch(removeUpdatedBankStmtList(result));
                  })
                  .catch((error) => {
                    console.log(error);
                  });
              })
              .catch((error) => {
                console.log(error);
              });
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log("post api---", error);
      });
  };

  return (
    <>
      <Container fluid className="rowCol">
        <Title title="Bank Statement" />
        <Row>
          <Col xl={6} className="w-1366">
            <div className="WhiteShadowCard overflow-scroll p-30px mb-3">
              <Row>
                <Col lg={6}>
                  <div className="form-group">
                    <select
                      className="form-control"
                      onChange={(e) => onComapanyChangeHandle(e.target.value)}
                      disabled={updatedBankData.length > 0 ? true : false}
                    >
                      <option value="none" selected disabled>
                        Select a Company
                      </option>
                      {companyList.map((res) => (
                        <option key={res.id} value={res.id}>
                          {res.companyname}
                        </option>
                      ))}
                    </select>
                  </div>
                </Col>
                {bankNameList.length > 1 ? (
                  <Col lg={6}>
                    <div className="form-group">
                      <select
                        className="form-control"
                        onChange={(e) => {
                          setBank(true);
                          setBankName(e.target.value);
                          onChangeBankName(e.target.value);
                        }}
                        disabled={updatedBankData.length > 0 ? true : false}
                      >
                        <option value="none" selected disabled>
                          Select a Bank
                        </option>
                        {bankNameList.map((bankListItem, index) => (
                          <option key={index} value={bankListItem.id}>
                            {bankListItem.bankname}
                          </option>
                        ))}
                      </select>
                    </div>
                  </Col>
                ) : (
                  <Col lg={6}>
                    <div className="form-group">
                      <input
                        onChange={(e) => onComapanyChangeHandle(e.target.value)}
                        value={
                          bankNameList.length !== 0
                            ? bankNameList[0].bankname
                            : ""
                        }
                        type="text"
                        className="form-control"
                        placeholder="Bank Name"
                        disabled
                      />
                    </div>
                  </Col>
                )}
              </Row>
            </div>
            <div className="WhiteShadowCard p-30px mb-3 mb-xl-0">
              <div className="tableHDR">
                <Row>
                  <Col md={3}>
                    <div className="font-size-12px text-primary opacity-40">
                      Date
                    </div>
                  </Col>
                  <Col md={7}>
                    <div className="font-size-12px text-primary opacity-40">
                      Particulars
                    </div>
                  </Col>
                  <Col md={2}>
                    <div className="font-size-12px text-primary opacity-40">
                      Amount
                    </div>
                  </Col>
                </Row>
              </div>
              {showTable && <BankContainer bankname={bankname}/>}
            </div>
          </Col>
          <Col xl={6} className="w-1366">
            <div className="WhiteShadowCard position-stiky">
              <div className="border-bottom p-30px pb-0">
                <Row>
                  <Col
                    md={12}
                    className="d-flex flex-wrap align-items-center justify-content-md-end"
                  >
                    <div className="pb-3 ">
                      {bank && (
                        <select
                          className="form-control"
                          onChange={(e) => setFiletype(e.target.value)}
                          disabled={updatedBankData.length > 0 ? true : false}
                        >
                          <option value=".pdf">PDF</option>
                          <option value=".csv">CSV</option>
                          {/* {bankNameList.map((bankListItem, index) => (
                            <option key={index} value={bankListItem.id}>
                              {bankListItem.bankname}
                            </option>
                          ))} */}
                        </select>
                      )}
                    </div>
                    <div className="pb-3 ">
                      <Button
                        variant="primary"
                        // onClick={() => {
                        //   onSubmitClickHandler();
                        // }}
                        // disabled={updatedBankData.length > 0 ? true : false}
                      >
                        Tabula
                      </Button>
                    </div>
                    <div className="pb-3 ">
                      {bank && (
                        <div className="fileInput font-size-14px">
                          Select File
                          <input
                            type="file"
                            onChange={(e) => {
                              onSelectFileHandler(e);
                            }}
                            disabled={updatedBankData.length > 0 ? true : false}
                            accept={filetype}
                          />
                        </div>
                      )}
                    </div>
                    {bnkCsv ? (
                      <div className="pb-3 pl-md-3">
                        <Button
                          variant="primary"
                          onClick={() => {
                            onSubmitClickHandler();
                          }}
                          disabled={updatedBankData.length > 0 ? true : false}
                        >
                          Submit File
                        </Button>
                      </div>
                    ) : null}
                  </Col>
                </Row>
              </div>
              {updatedBankData.length != 0 && bnkCsv != null ? (
                <PdfContainer
                  pdfFile={bnkCsv !== null ? bnkCsv : null}
                  bnkFileBas64={bnkFileBas64}
                />
              ) : (
                <div className="pdfView">
                  <div className="opacity-50 font-size-20px text-primary">
                    Image Preview
                  </div>
                </div>
              )}
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default BankStatement;
