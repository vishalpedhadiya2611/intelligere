import React from "react";

function PdfContainer({bnkFileBas64}) {

  return (
    <div className="pdfView">
        <iframe
         style={{border: "1px solid #666CCC"}}
         src={bnkFileBas64}
         frameBorder="1"
         scrolling="auto"
         height="100%"
         width="100%"
         >
        </iframe>
    </div>
  );
}

export default PdfContainer;
