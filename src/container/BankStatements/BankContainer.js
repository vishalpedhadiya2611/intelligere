import React, { useEffect, useState } from "react";
import { Row, Col, Form, InputGroup } from "react-bootstrap";
import axios from "axios";
import {
  addNewRow,
  bankListAction,
  BankStatementData,
  deleteBankStmt,
  ledgerListAction,
  removeUpdatedBankStmtList,
  updatedBankStmtList,
  updateOnEnter,
} from "../../redux/action/BankStatementAction";
import { useDispatch, useSelector } from "react-redux";
import { BankListAPI, LedgerListAPI } from "./Api";
import { Popconfirm } from "antd";

const BankContainer = ({bankname}) => {
  console.log(bankname);
  const dispatch = useDispatch();
  const bankData = useSelector((state) => state.bankStatement.bankStmtData);
  const updatedBankData = useSelector(
    (state) => state.bankStatement.updatedBankStmtList
  );
  const bankListData = useSelector((state) => state.CommonReducer.bankList);

  const [responseData, setResponseData] = useState(bankData);
  const [updatedBankItem, setUpdatedBankItem] = useState([]);

  const [ledger, setLedger] = useState([]);
  const [bank, setBank] = useState([]);
  const [addRowItem, setAddRowItem] = useState([]);

  const VoucherList = [
    "PURCHASE VOUCHER",
    "SALES VOUCHER",
    "DEBIT NOTE VOUCHER",
    "PAYMENT VOUCHER",
    "RECEIPT VOUCHER",
    "JOURNAL VOUCHER",
    "CREDIT NOTE VOUCHER",
    "CONTRA VOUCHER",
  ];

  useEffect(() => {
    setBank(bankListData);
  }, [bankListData]);

  useEffect(() => {
    setResponseData(bankData);
  }, [bankData]);

  useEffect(() => {
    setUpdatedBankItem(updatedBankData);
  }, [updatedBankData]);

  useEffect(async () => {
    const data = await LedgerListAPI();

    dispatch(ledgerListAction(data));
    setLedger(data);
  }, []);

  useEffect(async () => {
    const bankListData = await BankListAPI();
    dispatch(bankListAction(bankListData));
  }, []);

  const RemoveBlock = (parentIndex) => {
    const data = [...updatedBankItem];
    const UpdatedData = data.filter((data, index) => index != parentIndex);
    const deletedDataObject = data.filter(
      (data, index) => index == parentIndex
    );
    dispatch(removeUpdatedBankStmtList(UpdatedData != 0 ? UpdatedData : []));
    return deletedDataObject[0][0].id;
  };

  const deleteData = (parentIndex) => {
    let id = RemoveBlock(parentIndex);
    axios({
      method: "delete",
      url: "http://127.0.0.1:8000/e_payment/UpdateDeleteData/" + id + "/",
    })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log("post api---", error);
      });
  };

  const onClickAddButton = (parentIndex, childIndex) => {
    const check = updatedBankItem[parentIndex].findIndex(
      (data) => data.rowType == "added"
    );

    if (updatedBankItem[parentIndex].length < 3) {
      if (check < 0) {
        const data = [...updatedBankItem];
        let newrowDr = [...data].filter(
          (data, index) => index == parentIndex
        )[0][0];
        let newrowCr = [...data].filter(
          (data, index) => index == parentIndex
        )[0][1];

        let newValueDr = {};
        newValueDr.type = newrowDr.type;
        if (newrowCr.valueType == "bank") {
          newValueDr.particular = "";
          newValueDr.valueType = "ledger";
        } else {
          newValueDr.particular = newrowCr.particular;
          newValueDr.valueType = "ledger";
        }
        newValueDr.date = newrowDr.date;
        newValueDr.rowType = "added";
        newValueDr.values = parseFloat(newrowDr.originalValue);
        newValueDr.originalValue = parseFloat(newrowDr.originalValue);
        newValueDr.reference = "";
        data[parentIndex].push(newValueDr);

        // ---------------------------------------
        let newValueCr = {};
        newValueCr.type = newrowCr.type;
        if (newrowDr.valueType == "bank") {
          newValueCr.particular = "";
          newValueCr.valueType = "ledger";
        } else {
          newValueCr.particular = newrowDr.particular;
          newValueCr.valueType = "ledger";
        }
        newValueCr.date = newrowCr.date;
        newValueCr.rowType = "added";
        newValueCr.values = parseFloat(newrowCr.originalValue);
        newValueCr.originalValue = parseFloat(newrowCr.originalValue);
        newValueCr.reference = "";
        data[parentIndex].push(newValueCr);

        dispatch(removeUpdatedBankStmtList(data));
      }
    }
  };

  const onSubmit = async (parentIndex) => {
    const ClickedRow = document.getElementById("row" + parentIndex);
    let listOFRequired = [...ClickedRow.getElementsByClassName("required")];
    listOFRequired.forEach((element) => {
      if (element.value == "" && !element.nextElementSibling) {
        const spanTag = document.createElement("span");
        spanTag.classList.add("requiredError");
        const message = document.createTextNode("Required !");
        spanTag.appendChild(message);
        spanTag.style.color = "red";
        spanTag.style.fontSize = "11px";
        element.after(spanTag);
      }
    });

    let ErrorList = [...ClickedRow.getElementsByClassName("requiredError")];

    if (ErrorList.length == 0) {
      const oldData = [...updatedBankItem];
      const UpdatedData = oldData.filter((data, index) => index == parentIndex);
      // const Rdata = responseData.filter((data) => data.id == parentIndex);
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

      let ListAmount1Value = [];
      let ListLegder1Value = [];
      let ListAmount2Value = [];
      let ListLegder2Value = [];
      let EditLegder = "";
      let EditLegder2 = "";
      let CreditValue = 0;
      let DebitValue = 0;
      UpdatedData[0].map((item, index) => {
        if (index == 0) {
          CreditValue = parseFloat(item.values);
        }
        if (index == 1) {
          DebitValue = parseFloat(item.values);
        }

        if (index > 1 && item.rowType == "default") {
          ListAmount1Value.push(parseFloat(item.values));
          ListLegder1Value.push(item.particular);
        }
        if (index > 3 && item.rowType == "added") {
          ListAmount2Value.push(parseFloat(item.values));
          ListLegder2Value.push(item.particular);
        }

        if (index == 2 && item.rowType == "added") {
          EditLegder = item.particular;
        }
        if (index == 3 && item.rowType == "added") {
          EditLegder2 = item.particular;
        }
      });

      var urlencoded = new URLSearchParams();
      urlencoded.append("ListAmount1", ListAmount1Value);
      urlencoded.append("ListLegder1", ListLegder1Value);
      urlencoded.append("ListAmount2", ListAmount2Value);
      urlencoded.append("ListLegder2", ListLegder2Value);

      urlencoded.append("EditLegder", EditLegder);
      urlencoded.append("EditLegder2", EditLegder2);
      urlencoded.append("Vouchetype", UpdatedData[0][0].vouchetype);
      urlencoded.append("Vouchenumber", UpdatedData[0][0].vouchenumber);
      urlencoded.append(
        "AccountantNarration",
        UpdatedData[0][0].AccountantNarration
      );
      urlencoded.append("Credit", CreditValue);
      urlencoded.append("Debit", DebitValue);

      if (UpdatedData[0][0].id != "") {
        let requestOptions = {
          method: "PUT",
          headers: myHeaders,
          body: urlencoded,
        };
        fetch(
          `http://127.0.0.1:8000/e_payment/UpdateDeleteData/${UpdatedData[0][0].id}/`,
          requestOptions
        )
          .then((res) => {
            RemoveBlock(parentIndex);
            console.log(res);
          })
          .catch((err) => console.log(err));
      } else {
        let requestOptions = {
          method: "POST",
          headers: myHeaders,
          body: urlencoded,
        };

        fetch("http://127.0.0.1:8000/e_payment/newvoucher/", requestOptions)
          .then((res) => {
            RemoveBlock(parentIndex);
            console.log(res);
          })
          .catch((err) => console.log(err));
      }
    }
  };

  const onUpdatedChange = (updatedItemIndex, index, value, action) => {
    setUpdatedBankItem(
      updatedBankItem.map((d, i) => {
        if (d.index == updatedItemIndex) {
          if (action == "dr") {
            let ad = [...d.dr.values];
            ad[index] = value;
            return {
              ...d,
              dr: { ...d.dr, ["values"]: ad },
            };
          }
          if (action == "cr") {
            let ad = [...d.cr.values];
            ad[index] = value;
            return {
              ...d,
              cr: { ...d.cr, ["values"]: ad },
            };
          } else {
            return d;
          }
        } else {
          return d;
        }
      })
    );
  };

  const onChange = (parentIndex, childIndex, e) => {
    if (
      e.target.nextElementSibling &&
      e.target.nextElementSibling.className == "requiredError"
    ) {
      debugger;
      e.target.nextElementSibling.remove();
    }
    if (e.target.name != "date") {
      const caret = e.target.selectionStart;
      const element = e.target;
      window.requestAnimationFrame(() => {
        element.selectionStart = caret;
        element.selectionEnd = caret;
      });
    }
    const updatedData = updatedBankData.map((parentData, index) => {
      if (index == parentIndex) {
        let childData = parentData.map((chilData, index) => {
          if (index == childIndex) {
            return {
              ...chilData,
              [e.target.name]: e.target.value,
            };
          } else {
            return chilData;
          }
        });
        return childData;
      } else {
        return parentData;
      }
    });
    dispatch(removeUpdatedBankStmtList(updatedData));
  };

  const deleteRow = (parentIndex, childIndex) => {
    const oldData = [...updatedBankItem];
    const deletedObject = [...updatedBankItem[parentIndex]];
    let deleteAllRows = deletedObject.filter(
      (data, index) => index != childIndex
    );
    if (
      deletedObject[childIndex].rowType == "default" ||
      (deletedObject[childIndex].rowType == "added" && childIndex > 3)
    ) {
      if (
        (deletedObject[childIndex].type == "dr" && deleteAllRows.length == 2) ||
        (deletedObject[childIndex].rowType == "added" &&
          deletedObject[childIndex].type == "dr")
      ) {
        deleteAllRows[childIndex - 2].values =
          parseFloat(deletedObject[childIndex].originalValue) +
          parseFloat(deletedObject[childIndex - 2].originalValue);
        deleteAllRows[childIndex - 2].originalValue = parseFloat(
          deletedObject[childIndex - 2].values
        );
      } else {
        deleteAllRows[childIndex - 1].values =
          parseFloat(deletedObject[childIndex].originalValue) +
          parseFloat(deletedObject[childIndex - 1].originalValue);
        deleteAllRows[childIndex - 1].originalValue = parseFloat(
          deletedObject[childIndex - 1].values
        );
      }
    }
    const t = [...oldData[parentIndex], deleteAllRows];
    oldData[parentIndex] = t[t.length - 1];
    dispatch(removeUpdatedBankStmtList(oldData));
  };

  useEffect(() => {
    if (updatedBankItem.length > 0) {
      window.history.pushState(null, null, window.location.href);
      window.onpopstate = function (event) {
        window.history.go(1);
      };
      window.addEventListener("beforeunload", function (e) {
        e.preventDefault();
        e.returnValue = "";
      });
    }
  }, []);

  return (
    <>
      {updatedBankData.length > 0 ? (
        updatedBankItem.map((item, parentIndex) => {
          return (
            <div
              key={parentIndex}
              className="borderBox rowCol"
              id={"row" + parentIndex}
            >
              <div className="px-3 pt-3">
                {item.map((resb, childIndex) => {
                  return (
                    <Row>
                      <Col md={3}>
                        <div className="form-group">
                          <input
                            name="date"
                            onChange={(e) =>
                              onChange(parentIndex, childIndex, e)
                            }
                            value={resb.date}
                            type="date"
                            className="form-control"
                            placeholder="Transaction Date"
                          />
                        </div>
                      </Col>
                      <Col md={6}>
                        {resb.particular == "" || resb.particular == "nan" ? (
                          resb.valueType == "ledger" ? (
                            <select
                              className="form-control required"
                              name="particular"
                              onChange={(e) =>
                                onChange(parentIndex, childIndex, e)
                              }
                            >
                              {ledger.map((resb, index) => {
                                return (
                                  <option value={resb.legdername} key={index}>
                                    {resb.legdername}
                                  </option>
                                );
                              })}
                            </select>
                          ) : (
                            <select
                              className="form-control required"
                              name="particular"
                              onChange={(e) =>
                                onChange(parentIndex, childIndex, e)
                              }
                            >
                              {bank.map((bankRes, index) => {
                                return (
                                  <option
                                    value={index == 0 ? "" : bankRes.id}
                                    key={index}
                                  >
                                    {bankRes.bankname}
                                  </option>
                                );
                              })}
                            </select>
                          )
                        ) : (
                          <input
                            value={resb.particular}
                            name="particular"
                            onChange={(e) =>
                              onChange(parentIndex, childIndex, e)
                            }
                            type="text"
                            className="form-control required"
                            placeholder="Particulars"
                          />
                        )}
                      </Col>
                      <Col md={3}>
                        <div className="form-group position-relative">
                          <input
                            value={resb.values}
                            name="values"
                            onKeyDown={(e) => {
                              if (
                                e.keyCode == "13" &&
                                resb.values != resb.originalValue &&
                                e.target.value != 0 &&
                                e.target.value != "" &&
                                !isNaN(e.target.value)
                              ) {
                                dispatch(
                                  updateOnEnter(parentIndex, childIndex, e)
                                );
                              }
                            }}
                            onChange={(e) =>
                              onChange(parentIndex, childIndex, e)
                            }
                            type="text"
                            className="form-control required"
                            placeholder="Amount"
                            disabled={resb.particular ? true : false}
                          />

                          {
                            <span className={"status " + resb.type}>
                              {resb.type == "cr" ? "Cr" : "Dr"}
                            </span>
                          }
                          {childIndex < 2 ? (
                            ""
                          ) : (
                            <div className="close-icon">
                              <Popconfirm
                                okText="Yes"
                                cancelText="No"
                                title="Are you sure to delete row?"
                                onConfirm={() =>
                                  deleteRow(parentIndex, childIndex)
                                }
                              >
                                <span>x</span>
                              </Popconfirm>
                            </div>
                          )}
                        </div>
                      </Col>
                    </Row>
                  );
                })}
                <div className="px-3 pb-3">
                  <Row>
                    <Col md={6}>
                      <div className="form-group">
                        <label className="font-size-14px pl-2">Narration</label>
                        <input
                          name="transaction"
                          onChange={(e) => onChange(parentIndex, 0, e)}
                          value={item[0].transaction}
                          className="form-control required"
                          type="text"
                          placeholder="Narration"
                          disabled={item[0].id == "" ? false : true}
                        />
                      </div>
                    </Col>
                    <Col md={6}>
                      <div className="form-group">
                        <label className="font-size-14px pl-2">
                          Accountant Narration
                        </label>
                        <input
                          name="AccountantNarration"
                          onChange={(e) => onChange(parentIndex, 0, e)}
                          value={item[0].AccountantNarration}
                          className="form-control required"
                          type="text"
                          placeholder="Accountant Narration"
                        />
                      </div>
                    </Col>
                    <Col md={6}>
                      <div className="form-group">
                        <label className="font-size-12px pl-2">
                          Voucher Type:
                        </label>
                        <select
                          className="form-control required"
                          name="vouchetype"
                          onChange={(e) => onChange(parentIndex, 0, e)}
                        >
                          <option value="">SELECT VOUCHER</option>
                          {VoucherList.map((item, index) => {
                            return (
                              <option value={item} key={index}>
                                {item}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                    </Col>
                    <Col md={6}>
                      <div className="form-group">
                        <label className="font-size-12px pl-2">
                          Voucher No:
                        </label>
                        <input
                          className="form-control required"
                          name="vouchenumber"
                          type="text"
                          value={item[0].vouchenumber}
                          placeholder="Voucher No"
                          onChange={(e) => onChange(parentIndex, 0, e)}
                        />
                      </div>
                    </Col>
                    <Col
                      md={12}
                      className="d-flex flex-wrap align-items-center justify-content-md-end"
                    >
                      <button
                        className="ok-btn"
                        onClick={() => onSubmit(parentIndex)}
                      >
                        Ok
                      </button>
                      <button
                        className="add-btn ml-md-2"
                        onClick={() => {
                          onClickAddButton(parentIndex, 0);
                        }}
                        // onClick={() => {dispatch(addNewRow(parentIndex)); setAddRowItem([...addRowItem,parentIndex])}}
                      >
                        Add
                      </button>
                      <Popconfirm
                        placement="bottomRight"
                        okText="Ok"
                        cancelText="Cancel"
                        title="Are you sure to delete entry?"
                        onConfirm={() => {
                          deleteData(parentIndex);
                        }}
                      >
                        <button className="delete-btn ml-md-2">Delete</button>
                      </Popconfirm>
                    </Col>
                  </Row>
                </div>
              </div>
            </div>
          );
        })
      ) : (
        <></>
      )}
    </>
  );
};

export default BankContainer;
