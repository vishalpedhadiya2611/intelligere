export const UpdateResponse = (data) => {
 

  let updatedItemList = [];
  const updateData = data.map((res, inx) => {
   
    let childArray = [];
    let newDRData = {
      id: res.id,
      type: "dr",
      particular: res.Debit == 0 ? res.Legder : res.bank,
      date: res.Date,
      values: parseFloat(res.Debit == 0 ? res.Credit : res.Debit),
      valueType: res.Debit == 0 ? "ledger" : "bank",
      rowType: "default",
      vouchetype: res.Vouchetype,
      vouchenumber: res.Vouchenumber,
      transaction: res.Transaction,
      originalValue: parseFloat(res.Debit == 0 ? res.Credit : res.Debit),
      reference: "",
      AccountantNarration: res.AccountantNarration,
    };
    let newCRData = {
      id: res.id,
      type: "cr",
      rowType: "default",
      particular: res.Credit == 0 ? res.Legder : res.bank,
      date: res.Date,
      valueType: res.Credit == 0 ? "ledger" : "bank",
      values: parseFloat(res.Credit == 0 ? res.Debit : res.Credit),
      vouchetype: res.Vouchetype,
      vouchenumber: res.Vouchenumber,
      transaction: res.Transaction,
      originalValue: parseFloat(res.Credit == 0 ? res.Debit : res.Credit),
      reference: "",
      AccountantNarration: res.AccountantNarration,
    };

    childArray.push(newDRData);
    childArray.push(newCRData);
// for default
    if (res.ListAmount1 != "" && res.ListAmount1 != null) {
      const ListAmount1 = res.ListAmount1.split(",");
      const ListLegder1 = res.ListLegder1.split(",");
      ListAmount1.map((result, ind) => {
        let Amount1 = {
          id: res.id,
          type: res.Debit > res.Credit ? "cr" : "dr",
          particular: ListLegder1[ind],
          date: res.Date,
          values: result,
          valueType: "ledger",
          rowType: "default",
          vouchetype: res.Vouchetype,
          vouchenumber: res.Vouchenumber,
          transaction: res.Transaction,
          originalValue: parseFloat(
            result
            // res.Debit > res.Credit ? res.Credit : res.Debit
          ),
          reference: res.Debit > res.Credit ? "2" : "1",
          AccountantNarration: res.AccountantNarration,
        };
        childArray.push(Amount1);
      });
    }


    // for added
    if (res.ListAmount2 != "" && res.ListAmount2 != null) {
      const ListAmount2 = res.ListAmount2.split(",");
      const ListLegder2 = res.ListLegder2.split(",");
      let EditLegder = {
        id: res.id,
        type: res.Credit >= res.Debit ? "dr" : "cr",
        particular: res.EditLegder,
        date: res.Date,
        values: res.Debit1,
        valueType: "ledger",
        rowType: "added",
        vouchetype: res.Vouchetype,
        vouchenumber: res.Vouchenumber,
        transaction: res.Transaction,
        originalValue: parseFloat(res.Debit1),
        reference: res.Debit > res.Credit ? "2" : "1",
        AccountantNarration: res.AccountantNarration,
      };
      childArray.push(EditLegder);

      let EditLegder2 = {
        id: res.id,
        type: res.Credit >= res.Debit ? "cr" : "dr",
        particular: res.EditLegder2,
        date: res.Date,
        values: res.Credit1,
        valueType: "ledger",
        rowType: "added",
        vouchetype: res.Vouchetype,
        vouchenumber: res.Vouchenumber,
        transaction: res.Transaction,
        originalValue: parseFloat(res.Credit1),
        reference: res.Debit > res.Credit ? "2" : "1",
        AccountantNarration: res.AccountantNarration,
      };

      childArray.push(EditLegder2);

      ListAmount2.map((result, ind) => {
        let Amount1 = {
          id: res.id,
          type: res.Credit >= res.Debit ? "cr" : "dr",
          particular: ListLegder2[ind],
          date: res.Date,
          values: result,
          valueType: "ledger",
          rowType: "added",
          vouchetype: res.Vouchetype,
          vouchenumber: res.Vouchenumber,
          transaction: res.Transaction,
          originalValue: parseFloat(
            result
            // res.Debit > res.Credit ? res.Credit : res.Debit
          ),
          reference: res.Debit > res.Credit ? "2" : "1",
          AccountantNarration: res.AccountantNarration,
        };
        childArray.push(Amount1);
      });
    }

    updatedItemList.push(childArray);
  });
  return updatedItemList;
};
