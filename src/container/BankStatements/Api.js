import axios from 'axios';


export const CompanyListAPI = async () => {
    // const {data} = await axios.get('http://127.0.0.1:8000/e_payment/CompanyViews/')
    const {data} = await axios.get('https://bank-statement.herokuapp.com/e_payment/CompanyViews')
    return data
}


export const BuyerCompanyListAPI = async () => {
    const {data} = await axios.get('http://127.0.0.1:8000/invoice/buyerdata/')
    // const {data} = await axios.get('https://bank-statement.herokuapp.com/e_payment/CompanyViews')
    return data
}


export const BankListAPI = async () => {
    // const {data} = await axios.get('http://127.0.0.1:8000/e_payment/BankViews/')
    // const {data} = await axios.get('https://bank-statement.herokuapp.com/e_payment/BankViews')    
    // data.unshift(
    //         {
    //             "id": 0,
    //             "bankname": "Select Bank",
    //             "companyname": ""
    //         }
    // )
    // return data


    const data = [{
        "id": 0,
        "bankname": "Select Bank",
        "companyname": "",
        "index": 1
    },
    {
        "companyname": 1,
        "bankname": "Bank Of Baroda",
        "index": 1
    },
    {
        "companyname": 1,
        "bankname": "City Bank",
        "index": 2
    },
    {
        "companyname": 1,
        "bankname": "AMCO bank",
        "index": 3
    }]
    return data
}

export const LedgerListAPI = async () => {
//     let d=[{
//         "id": 0,
//         "legdername": "Select Legder",
//         "companyname": ""
//     },{
//                 "id": 1,
//                 "legdername": "TEST",
//                 "companyname": ""
//             },{
//                         "id": 2,
//                         "legdername": "PAYTM",
//                         "companyname": ""
//                     }];
// return d;   
    const {data} = await axios.get('http://127.0.0.1:8000/e_payment/Legderlist/')
    data.unshift(
        {
            "id": 0,
            "legdername": "Select Legder",
            "companyname": ""
        }
    )
    return data
}
