import React, { useState } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import swal from "sweetalert";
import {
  ContactIcon,
  EyeIcon,
  EyeSlashIcon,
  LockIcon,
  LogoIcon,
  UserIcon,
} from "../../components/Icons/Icons";

import logoDDS from "./../../assets/logoDDS.svg";
import axios from 'axios'

const Login = () => {
  const [showPassword, setShaowpassword] = useState(false);

  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // eslint-disable-next-line
  const [error, setError] = useState(false);

  
  const handleLogin = () => {
    if (!email) {
      swal("Please Enter Email", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    if (!password || password.length < 6) {
      swal("Please Enter Password", "Incorrect Input", "warning");
      setError(true);
      return;
    }
    const loginData = {
      email: email,
      password: password,
    };

    const api = axios.create({
      baseURL: `https://api.digitaldocsys.in/api`,
    });
    api
      .post("/login/", loginData)
      .then((res) => {
        console.log(res.data.tokens);
        const token = res.data.tokens.access;
        localStorage.setItem("user", token);
        history.push("/organization");
      })
      .catch((error) => {
        swal("Unauthorized Access", "", "error");
        setError(true);
      });
  };
  const register = () => {
    history.push("/Signup");
  };
  return (
    <>
      <div>
        <Container fluid>
          <Row className="justify-content-center align-items-center">
            <Col
              md={10}
              className="py-md-5 py-3 d-flex justify-content-between"
            >
              <Link to="/" className="logodds">
                <img src={logoDDS} alt="logo" />
              </Link>
              <Link to="/">
                <ContactIcon />
              </Link>
            </Col>
            <Col lg={5}>
              <div className="text-center py-md-4 pb-3">
                <div className="logoicon mb-4">
                  <LogoIcon />
                </div>
                <div className="font-size-md-16px font-size-14px text-uppercase opacity-60 text-primary pt-3">
                  Welcome to
                </div>
                <div className="font-size-md-32px font-size-18px font-weight-700 text-primary ">
                  smart.cheque
                </div>
              </div>
              <Card className="loginCard">
                <Card.Body>
                  <div className="form-group">
                    <label className="font-size-14px pl-2 opacity-60">
                      Username & Email
                    </label>
                    <div className="position-relative mb-3">
                      <input
                        type="email"
                        className="form-control w-100"
                        placeholder="Enter Username & Email"
                        autoComplete="off"
                        onChange={e => setEmail(e.target.value)}
                      />
                      <UserIcon />
                    </div>
                  </div>
                  <div className="form-group">
                    <label className="font-size-14px pl-2 opacity-60">
                      Password
                    </label>
                    <div className="position-relative mb-3">
                      <input
                        type={showPassword ? "text" : "password"}
                        className="form-control w-100"
                        placeholder="Enter Password"
                        autoComplete="off"
                        onChange={e => setPassword(e.target.value)}
                      />
                      <LockIcon />
                      <button
                        type="button"
                        className="showPassword btn-link btn p-0"
                        onClick={() => setShaowpassword(!showPassword)}
                      >
                        {!showPassword ? <EyeIcon /> : <EyeSlashIcon />}
                      </button>
                    </div>
                  </div>
                  <button type="submit" className="btn btn-primary w-100 mt-3" onClick={handleLogin}>
                    Login
                  </button>
                  <div className="text-center pt-3">
                    <Link to="/" className="text-primary btn btn-link p-0">
                      Forgot Password
                    </Link>
                  </div>
                </Card.Body>
              </Card>
              <div className="text-center pt-md-4">
                <div className="font-size-md-20px font-size-16px pt-3">
                  New to smart.cheque?
                </div>
                <Link
                  to="/register"
                  className="mt-3 font-size-md-16px font-size-14px ok-btn btn py-1 px-4 h-auto w-auto"
                >
                  Register Here
                </Link>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};
export default Login;
