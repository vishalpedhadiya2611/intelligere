import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import login from "../container/BankCheque/login/Login";
import ForgetPassword from "../container/ForgetPassword/ForgetPassword";
import Login from "../container/Login/Login";
import Register from "../container/Register/Register";
import { ActiveRouts } from "./ActiveRouts";

export function Routes() {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/forgetpassword" component={ForgetPassword} />
                <Route exact path="/login" component={Login} />
                <ActiveRouts />
            </Switch>
            
        </Router>
    )
}