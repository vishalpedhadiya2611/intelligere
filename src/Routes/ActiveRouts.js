import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import { ToastContainer } from "react-toastify";
import Footer from "../components/Layout/Footer";
import Header from "../components/Layout/Header";
import Sidebar from "../components/Layout/Sidebar";
import BankReport from "../container/BankStatements/BankReport";
import BankStatement from "../container/BankStatements/BankStatement";
import Dashboard from "../container/Dashboard/Dashboard";
import Extra from "../container/Extra";
import InvoiceCreate from "../container/Invoice/InvoiceCreate";
import InvoiceImport from "../container/Invoice/InvoiceImport";
import InvoiceReport from "../container/Invoice/InvoiceReport";
import 'react-toastify/dist/ReactToastify.css';
import 'antd/dist/antd.css'
import ProtectedRoute from "../container/BankCheque/GuardedRoute";
import Report from "../container/BankCheque/Report/report";
import BankDetail from "../container/BankCheque/BankDetail/BankDetail";
import Payment from "../container/BankCheque/Payment/Payment";
import camera from "../container/BankCheque/camera";
import PrintExample from "../container/BankCheque/PrintExample";
import PaymentVerification from "../container/BankCheque/paymenVerification/PaymentVerification";
import Print from "../container/BankCheque/Print/Print";
import organization from "../container/BankCheque/organization/organization";
import ExternalSoftware from "../container/BankCheque/ExternalSoftware/ExternalSoftware";
import termconditions from "../container/BankCheque/termconditions";
import ChangePassword from "../container/ChangePassword/ChangePassword";
import Login from "../container/Login/Login";

export function ActiveRouts() {
    return (
        <Router>
            <main className="d-flex TM_main">
                <Sidebar />
                <div className="TM_mainContentWidthHeader">
                    <Header />
                    <div className="TM_mainContent">
                        <Switch>
                            <ProtectedRoute exact path="/extra" component={Extra} />
                            <ProtectedRoute exact path="/dashboard" component={Dashboard} />
                            <ProtectedRoute exact path="/bank-statement-emport" component={BankStatement} />
                            <ProtectedRoute exact path="/bank-statement-report" component={BankReport} />
                            <ProtectedRoute exact path="/invoice-create" component={InvoiceCreate} />
                            <ProtectedRoute exact path="/invoice-import" component={InvoiceImport} />
                            <ProtectedRoute exact path="/invoice-report" component={InvoiceReport} />

                            {/* bank cheque route*/}
                            <ProtectedRoute path="/bankcheque/dashboard" component={Report} />
                            <ProtectedRoute path="/bank" component={BankDetail} />
                            <ProtectedRoute path='/payment' component={Payment} />
                            
                            <ProtectedRoute path='/camera' component={camera} />
                            <ProtectedRoute path='/printers' component={PrintExample} />
                            <ProtectedRoute path="/verification" component={PaymentVerification} />
                            <ProtectedRoute path='/report' component={Report} />
                            <ProtectedRoute path='/print' component={Print} />
                            <Route exact path="/login" component={Login} />
                            <ProtectedRoute path="/organization" component={organization} />
                            <ProtectedRoute path="/external" component={ExternalSoftware} />
                            {/* <Route path="/Signup" component={Signup} /> */}
                            <Route path="/termconditions" component={termconditions} />
                            <ProtectedRoute path='/changepassword' component={ChangePassword} />
                            <Redirect to="/login" />
                        </Switch>
                    </div>
                    <ToastContainer />
                    <Footer />
                </div>
            </main>
        </Router>
    )
}