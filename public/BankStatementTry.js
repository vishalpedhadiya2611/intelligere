import React, { useState, useRef } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import PdfContainer from "./PdfContainer";
import BankContainer from "./BankContainer";
import axios from "axios";
import { Title } from "../../components/Texonomy";
import DatePicker from "react-datepicker";
import PSPDFKit from 'pspdfkit';

import "react-datepicker/dist/react-datepicker.css";

function BankStatement() {
  const containerRef = useRef(null);
  const [bnkCsv, setbnkCsv] = useState(null);
  const [bankname, setBankName] = useState(null);
  const [showTable, setShowTable] = useState(false);
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [pdfUrl, setPdfUrl] = useState('https://cors-anywhere.herokuapp.com/http://www.africau.edu/images/default/sample.pdf');
  var bodyFormData = new FormData();
  bodyFormData.append("file", bnkCsv);
  bodyFormData.append("bankname", bankname);

  var bodyFormData2 = new FormData();
  bodyFormData2.append("file", bnkCsv);
  

  const funct = (e) => {
    const container = containerRef.current;
    e.preventDefault();
    
    PSPDFKit.load({ 
      maxPasswordRetries: 0,
      container,
      document: pdfUrl,
      baseUrl: `${window.location.protocol}//${window.location.host}/${process.env.PUBLIC_URL}`
      })
      .then(instance => {
        
        return instance;
      })
      .catch(err => {
        if (err.message === "INVALID_PASSWORD") {
          
        }
      });
    
  };

  return (
    <>
      <Container fluid className="rowCol">
        <Title title="Bank Statement" />
        <Row>
          <Col xl={6} className="w-1366">
            <div className="WhiteShadowCard p-30px mb-3">
              <Row>
                <Col lg={6}>
                  <div className="form-group">
                    <select className="form-control">
                      <option value="none" selected disabled>
                        Select a Company
                      </option>
                      <option>Comapny 1</option>
                      <option>Comapny 2</option>
                      <option>Comapny 3</option>
                    </select>
                  </div>
                </Col>
                <Col lg={6}>
                  <div className="form-group">
                    <input value={bankname} onChange={(e) => setBankName(e.target.value)} type="text" className="form-control" placeholder="Bank Name" />
                  </div>
                </Col>
              </Row>
            </div>
            <div className="WhiteShadowCard p-30px mb-3 mb-xl-0">
              <div className="tableHDR">
                <Row>
                  <Col md={3}>
                    <div className="font-size-12px text-primary opacity-40">Date</div>
                  </Col>
                  <Col md={7}>
                    <div className="font-size-12px text-primary opacity-40">Particulars</div>
                  </Col>
                  <Col md={2}>
                    <div className="font-size-12px text-primary opacity-40">Amount</div>
                  </Col>
                </Row>
              </div>
              {
                showTable === true && <BankContainer />
              }
            </div>
          </Col>
          <Col xl={6} className="w-1366">
            <div className="WhiteShadowCard h-100">
              <div className="border-bottom p-30px pb-0">
                <Row>
                  <Col md={12} className="d-flex flex-wrap align-items-center justify-content-md-end">
                    <div className="pb-3 ">
                      <div className="fileInput font-size-14px">
                        Select File
                        <input
                          type="file"
                          onChange={(e) => setbnkCsv(e.target.files[0])}
                        />
                      </div>
                    </div>
                    <div className="pb-3 pl-md-3">
                      <Button variant="primary" onClick={funct}>
                        Submit File
                      </Button>
                    </div>
                  </Col>
                </Row>
              </div>
              <div ref={containerRef} style={{ width: "100%", height: "100vh"}}/>
              {/* <div className="d-flex h-xl-100 align-items-center justify-content-center">
                <div className="opacity-50 font-size-20px text-primary">Image Preview</div>
              </div> */}
              {/* <PdfContainer /> */}
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default BankStatement;
